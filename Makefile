#
# NOSOUND driver for sega mega drive
# Copyright(c) 2023 Emmanuel Rousseau;
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all; copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

MAKE=make
DRIVER=./src/asm/nosound.bin
NOSDK_LIBFILE=libnosound-m68k.a
NOSDK_LIBFILEPATH=./src/c/$(NOSDK_LIBFILE)
DEMO_ROM=out.bin
TARGET=release

MARSDEV_INSTALL=$(HOME)/mars
MARSDEV_DRIVER_INSTALL=$(MARSDEV_INSTALL)/driver
MARSDEV_LIBS=$(MARSDEV_INSTALL)/m68k-elf/lib
MARSDEV_HEADERS=$(MARSDEV_INSTALL)/m68k-elf/include
MARSDEV_HEADERS_INSTALL=$(MARSDEV_HEADERS)/nosound

all: out.bin

.PHONY: $(DRIVER) $(NOSDK_LIBFILEPATH) clean install_linux

./res/nosound.bin: $(DRIVER)
	cp ./src/asm/nosound.bin ./res

driver: $(DRIVER)
	$(MAKE) -C ./src/asm

sdk: $(NOSDK_LIBFILEPATH)
	$(MAKE) -C ./src/c $(TARGET)

$(DEMO_ROM): driver sdk ./res/nosound.bin
	python3 ./src/python/nos-tools-fur.py --path ./data --output ./data/vgml
	python3 ./src/python/nos-tools-sgdk.py --vgml_path ./data/vgml --output_res ./res  --output_src ./demo
	$(MAKE) -f ./demo/Makefile $(TARGET)

install_linux:
	mkdir -p $(MARSDEV_HEADERS_INSTALL)
	mkdir -p $(MARSDEV_DRIVER_INSTALL)
	cp $(NOSDK_LIBFILEPATH) $(MARSDEV_LIBS)
	cp ./src/c/*.h $(MARSDEV_HEADERS_INSTALL)
	cp ./res/nosound.bin $(MARSDEV_DRIVER_INSTALL)

clean:
	rm ./res/nosound.bin -f
	rm ./res/nosound ./data/vgml -rf
	rm ./res/nosound_chunk*.bin nosound.bin -f
	rm ./res/res_nosound.h ./res/res_nosound.res -f
	rm ./demo/res_nosound_index.* -f
	$(MAKE) -C ./src/asm clean
	$(MAKE) -C ./src/c clean
	$(MAKE) -f ./demo/Makefile clean
