# NoSound Audio Driver for Sega Genesis (beta)

This is a humble audio driver for the Sega Genesis that uses a tracker like format. It's written in Z80 assembly and runs on that co-processor.

The driver is in development and may evolve in the future. I developped it for my Genesis project *Planet-B*. I wanted a music engine that supports sound effects, can be controlled by the game logic and that has a very small ROM footprint. Could be as low as less than 3k per songs and less than 100 bytes per SFX. I'm trying to minimize my environmental footprint. Less is more.

## Features

 - Command based sequencer
 - Full YM2612 and SN76489 synthesis
 - Works with SGDK, using the exporter, out of the box
 - C API to install and interract with the driver
 - Furnace module import (through Python import scripts see below)
 - Sound effects playback over busy channel
 - Very decent size for the ROM data.
 - Custom FM instruments
 - Custom PSG instruments through macro tables
 - BPM Precise tempos, adjustable at runtime
 - Fully open source and royalty free, MIT lisencing. It can be used freely in commercial projects.

## How to install

### Building from sources

For now, the only way to get the driver, is through the use of the awesome approach *build it yourself (tm)*, I don't have much time now to setup a CI/CD pipeline and a build image that embeds marsdev to make this process painless. I apologize.

Building everything from the sources, though, is best way to ensure that you haven't any linker errors between SGDK and the NoSound SDK static library. The downside of this approach being, I didn't made it compatible with Windows, contributing sadly to that trend in FOSS to make Windows a second class citizen. I apologize even more.

To build it from sources, see the section below.

### Project Setup

Once you built it, you'll have the following files:

    # The SDK static library file
    ${NOSOUND}/src/c/libnosound-m68k.a

    # the z80 driver
    ${NOSOUND}/res/nosound.bin

For simplicity, I suggest you to just vendor the static library, the driver file, the python scripts and the headers somewhere in your project. You may also use the flackier approach to keep them external. But this has a lot of problems, especially in the long run.

So, in order to build your ROM with gcc, you'll need to add these options to the compiler command:

    $ gcc (...) -I${NOSOUND}/src/c -L${NOSOUND}/src/c -lnosound-m68k

If you are using a `Makefile` you'll need somehow to modify it but it's out of the scope of this documentation.

Once the ROM builds and the linking is correct, you still won't have the driver embedded into the ROM. You need to add a new resource for rescomp so the driver gets embedded into it. First, copy the driver into your local `./res` directory. Then, add this line to one of your `.res` file.

    BIN nosound_z80 ./nosound.bin 2 2 0 NONE

Now the driver will be accessible by the variable `nosound_z80`. From there the SDK will be able to help you.

Reading the code of the demo is a good way to get you started. You may also take the time to read the comments in `nosound.h`. Stay with `nosound.h`, only include this file and refrain from using anything else. It might change in the future.

## How to Import music

### Overview

#### Preparation

What is required:
 - python 3.8+ installed. Both Windows and *nix OSes are supported.
 - Furnace 0.6pre5

The tools have some dependencies. To install them, it is required to run :

    $ pip install --upgrade pip
    $ pip install -r requirements.txt

`pip` package manager comes with python so there's no need to install it.

> Tip: If you want to go cleaner, using a virtual environment is a good idea, but not required.

#### Module authoring

At the moment, the tools only support Furnace. Thus, you need to write your module with [Furnace Tracker](https://github.com/tildearrow/furnace), as plain `*.fur` files. The tracker is used to either write Music files or SFX files. The tools are written to work with **Furnace 0.6pre5**, later version may work, but it is not garanteed.

You can also produce a VGML from another source, but this is advanded topic.

#### Translation to VGML

VGML is an intermediate music format of my own. Instead of having a direct translation from Furnace Modules to NoSound binary format, there's a YAML transitionnal file in between. It seems to be an unnecessary step, but, in fact, it's quite useful.

  1. rather than storing only binaries in the souce control software (aka Git), it allows to persist them as text versions instead. This has tendencies to rot less over the years and make your source data independent from Furnace.
  2. it allows inspection of what is extracted from the module file for debug purposes and even open possibilities to write tools to analyse/optimize the music files.
  3. allow extraction from other sources. ie, MML, Deflemask etc.

The script `nos-tools-fur` will process a directory containing all the modules files into a YAML (VGML) representation. An example is given below.

#### SGDK support

Once the VGMLs are generated. The tool `nos-tools-sgdk` will convert these YAML into a bunch of binary files. Here's what is generated:

 - `*.nos` file:  which are the binaries the driver need as data for the playback.
 - `res_nosound.res`: SGDK Rescomp resource file.
 - `res_nosound_index.h|c`: data structure for the index.
 - `nosound_chunk0.bin`: binary data block of maximum size of 32768 bytes. These files contains several entries. This is what `res_nosound_index.h` indexes.

As a sidenote, you may notice that this converter generates a file named `_orders.txt`. This file persists the ordering as your project evolves and gets new songs/sfx. This way, the position in the index won't change, this allows you to easily define constants in the game code and stay with them as long as you don't delete it.

#### Command line example

This example uses the current repository demo and show how to convert the `data/*.fur` files to consolidated binaries for your project. These commands must be run at the root of the project. If you're a Windows user, invert the `/` to `\` just in case your experiencing path errors.

This command converts all the files from the directory `data` and export them to VGML in the directory `data/vgml`.

    $ python3 ./src/python/nos-tools-fur.py --path ./data --output ./data/vgml

Next, export for SGDK in the `res` directory and generates a header and a source file containing the indexes:

    $ python3 ./src/python/nos-tools-sgdk.py --vgml_path ./data/vgml --output_res ./res  --output_src ./demo

Next, you may build your ROM.

If you want to use these scripts in your project, just refer to them using a full path to your installation of Nosound or vendor them.

### Naming convention

In order for `nos-tools-sgdk` to import correctly your files, they must follow a naming convention and they must be regrouped into a single directory.

Here's the convention, files not following this convention are ignored:

| Filename pattern  | Parameters  | Comment |
|---|---|---|
| `mod_<song_name>.fur`  |   | A music file must be prefixed by `mod_`.<br><br> `mod_level_0.fur` is a valid name for a music module. |
| `sfx_ch<C>_<sfx_name>.fur` | `C` : channel playback | A SFX file must be prefixed by `sfx_`. Furthermore, a channel must be provided for the Driver to play it. See below for more detail about how to create SFX modules. The SFX is played as FM when C is between [0, 5], or on the SMS sound chip when C is between [6, 9]  <br><br> - `sfx_ch3_boom.fur`: a FM SFX which is played on the FM 4th channel by the driver.<br> - `sfx_ch7_slash.fur`: a PSG SFX which is played on the 2nd channel of the SN76489.|

### SFX

The channel in the filename for the SFX isn't required to match the channel that has been used in the Furnace Module. The tools will detect which channel has been used and remap it accordingly.

A SFX file must contain no more than only one channel containing music
events (notes, volume, fx  etc.).

If a SFX is intended to be played on the YM2612, you must use one of the  FM channels. The same applies for the SN76489. In the case of the noise channel, you must use the noise channel in your module to get the proper result in NoSound format.

The driver isn't very robust, it's meant to be fast. The tools are built to prevalidate for the driver. But, be diligent with your input data, keep it clean and straight. I wrote the import tooling in a hurry, so, although it's rebust and reliable, the error reporting is not very good for the moment and I might have overlooked some edge cases. By keeping your data simple and clean, it will prevent you from dealing with it.

## Building

### Requirements

To build only the driver:
 * [zasm](https://k1.spdns.de/Develop/Projects/zasm/Distributions/) 4.4.10 by Günter Woigk

To run the tooling:
 * python 3.8+

The tools have some dependencies. To install them, it is required to run :

    $ pip install --upgrade pip
    $ pip install -r requirements.txt

To build the demo you'll need
 * Marsdev

Once everything is all setup, just type

    make

At the moment, the demo can only be built out of the box in *Nix OS. Building from Windows is perfectly doable, but it requires a bit of elbow grease.

## Roadmap

What is planned on the roadmap (no schedule provided):
 - Tracker FX (tremolo, panning, vibrato, portamento etc...)
 - 50hz PAL support (almost already supported)
 - Interractions with the driver, game code would be able to :
   - Request a specific pattern to be played after the current one is finished
   - Being able to save and restore a state to resume playing music where
     is was before playing another song.
   - Being able to request playback information to the driver (current tempo,
     notes being played, current pattern and position in song).
 - Project file for the import, to get rid of the stupid naming convention.
 - Debugging mode that displays what's going on in the driver.
 - PCM playback (I didn't need it, I'll do it later)
 - Automatic compression side chaining
 - ROMs larger than 4MB, the workaround for now it to put the music data into
   a non switchable area.

What I will not implement:
 - PCM streaming
 - Multichannel PCM mixing
 - Complicated effects encountered in trackers. Would make the driver too
   complicated without much of plus value.

## Project's directory structure

    /boot   assemble boot code (SGDK)
    /data   furnace test files
    /demo   demo using SGDK
    /doc    various dev related files
    /res    SGDK resources
    /src    sources of the driver, sdk and tools

## Special Thanks To

 * My wife and my daugther, for supporting me in what I love the most.
 * Stéphane Dallongeville, his help with the hardware, he saved me a lot of time, thank you dude.
 * Kannagi, for sharing experience and advising me about how to design in ASM languages. Very valuable life tips! :)


