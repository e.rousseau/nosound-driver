# Various links

## z80
 * [Simulator](https://www.asm80.com/#)
 * [Z80 in 28 days](https://tutorials.eeems.ca/ASMin28Days/lesson/toc.html)
 * [zasm](https://k1.spdns.de/Develop/Projects/zasm/Documentation/)
## YM2612
 * [Timings](http://gendev.spritesmind.net/forum/viewtopic.php?f=24&t=386&start=855)
 * [Technical manual](https://www.smspower.org/maxim/Documents/YM2612)

## Furnace 
 * [Format Documentation](https://github.com/tildearrow/furnace/blob/master/papers/format.md)
