from __future__ import annotations
from enum import Enum
import argparse
import zlib
import logging
import struct

logger = logging.getLogger(__name__)


from typing import List, Dict
from collections import defaultdict

CHANNEL_COUNT = 10


class FurError(RuntimeError):
    pass


class Reader:
    def __init__(self, data: bytes):
        self.data: bytes = data
        self.ptr = 0

    def read(self, size: int) -> bytes:
        data = self.data[self.ptr : self.ptr + size]
        self.ptr += size
        return data

    def read_string(self, size: int) -> str:
        data = self.data[self.ptr : self.ptr + size]
        self.ptr += size
        return data.decode("utf-8")

    def readu8(self) -> int:
        data = self.data[self.ptr]
        self.ptr += 1
        return data

    def readu16(self) -> int:
        data = self.data[self.ptr : self.ptr + 2]
        self.ptr += 2
        return int.from_bytes(data, byteorder="little")

    def readu32(self) -> int:
        data = self.data[self.ptr : self.ptr + 4]
        self.ptr += 4
        return int.from_bytes(data, byteorder="little")

    def read_array_of_int(self, elem_count, elem_size) -> list:
        data = self.data[self.ptr : self.ptr + elem_count * elem_size]
        self.ptr += elem_count * elem_size
        return [
            int.from_bytes(data[i : i + elem_size], byteorder="little")
            for i in range(0, elem_count * elem_size, elem_size)
        ]

    def read_bytes(self, size: int) -> bytes:
        data = self.data[self.ptr : self.ptr + size]
        self.ptr += size
        return data

    def skip(self, size: int) -> None:
        self.ptr += size

    def new_reader_from(self, offset: int) -> "Reader":
        return Reader(self.data[offset:])

    def new_reader(self) -> "Reader":
        return Reader(self.data[self.ptr :])

    def eof(self) -> bool:
        return self.ptr >= len(self.data)


def validate_chips(chips: list):
    if len(chips) != 32:
        raise ValueError(f"Expected 32 chips, got {len(chips)}")
    # Validate the chips is YM2612 6 channels
    if chips[0] != 131 or chips[1] != 3:
        raise FurError(f"supported system is only Sega Mega Drive YM2612 6 channels")

    return chips


def load_orders(fursonginfo: FurSongInfo, reader: Reader):
    orders_length = fursonginfo.orders_length
    data = reader.read_array_of_int(orders_length * CHANNEL_COUNT, 1)
    rowscount = int(len(data) / CHANNEL_COUNT)

    columns: List[List[int]] = []
    for i in range(0, len(data), rowscount):
        col = data[i : i + rowscount]
        columns.append(col)

    rows: List[List[int]] = []

    for i in range(rowscount):
        row = []
        for c in columns:
            row.append(c[i])
        rows.append(row)
    fursonginfo.orders = rows


def load_instrument_features(fur_instrument: FurInstrument, reader: Reader):
    read_bytes = 12
    features: List[FurFeature] = []

    while read_bytes < fur_instrument.block_size:
        subreader = reader.new_reader()
        feature = deserialize(FurFeature, subreader, FurInstrumentFeatureStructure)
        read_bytes += (
            feature.block_size + 4
        )  # 4 bytes for the size of the block's header
        features.append(feature)
        reader.skip(feature.block_size + 4)

    fur_instrument.features = features


def load_instrument_feature(fur_feature: FurFeature, reader: Reader):
    data = reader.read_bytes(fur_feature.block_size)
    fur_feature.data = data


def load_pattern_data(fur_pattern: FurPattern, r: Reader):
    subreader = r.new_reader()

    pattern_data: List[FurPatternCell] = []
    fur_pattern.pattern_data = pattern_data
    while subreader.ptr < fur_pattern.block_size:
        cell = FurPatternCell()
        row_flags = subreader.readu8()

        if row_flags == 0xFF:
            # end of pattern
            break

        if row_flags & 0x80:
            skip = row_flags & 0x7F
            pattern_data.append(cell)
            for i in range(0, skip + 1):
                pattern_data.append(cell)
            continue

        pattern_data.append(cell)

        has_note = (row_flags & 0x01) != 0
        has_instrument = (row_flags & 0x02) != 0
        has_volume = (row_flags & 0x04) != 0
        has_effect = (row_flags & 0x08) != 0
        has_effect_param = (row_flags & 0x10) != 0
        has_more_effects = (row_flags & 0x20) != 0
        has_even_more_effects = (row_flags & 0x40) != 0

        fx = [None] * 8
        fx_val = [None] * 8

        fx_col = [None] * 8

        if has_effect:
            fx[0] = True
        if has_effect_param:
            fx_val[0] = True

        if has_more_effects:
            fx_0_3 = f"{subreader.readu8():08b}"[::-1]
            for fx_id in range(0, 4, 1):
                bit = fx_0_3[fx_id * 2]
                if bit == "1":
                    fx[fx_id] = True
                bit = fx_0_3[fx_id * 2 + 1]
                if bit == "1":
                    fx_val[fx_id] = True

        if has_even_more_effects:
            fx4_7 = f"{subreader.readu8():08b}"[::-1]
            for fx_id in range(0, 4, 1):
                bit = fx4_7[fx_id * 2]
                if bit == "1":
                    fx[fx_id + 4] = True
                bit = fx4_7[fx_id * 2 + 1]
                if bit == "1":
                    fx_val[fx_id + 4] = True

        if has_note:
            cell.note = subreader.readu8()

        if has_instrument:
            cell.instrument = subreader.readu8()

        if has_volume:
            cell.volume = subreader.readu8()

        had_fx = False
        for i in range(0, 8):
            f = [None, None]
            if fx[i] is not None:
                had_fx = True
                f[0] = fx[i]
                f[0] = subreader.readu8()
            if fx_val[i] is not None:
                had_fx = True
                f[1] = subreader.readu8()
            fx_col[i] = f
        if had_fx:
            cell.fx = fx_col
        else:
            cell.fx = []

    r.skip(subreader.ptr)


class FurSongInfoStructure(Enum):
    block_id = (4, "block_id", str)
    block_size = (4, "block_size", int)
    time_base = (1, "time_base", int)
    speed1 = (1, "speed1", int)
    speed2 = (1, "speed2", int)
    init_arpegio_time = (1, "init_arpegio_time", int)
    tick_per_second = (4, "tick_per_second", float)
    pattern_length = (2, "pattern_length", int)
    orders_length = (2, "orders_length", int)
    highlight_a = (1, "highlight_a", int)
    highlight_b = (1, "highlight_b", int)
    instrument_count = (2, "instrument_count", int)
    wave_table_count = (2, "wave_table_count", int)
    sample_count = (2, "sample_count", int)
    pattern_count = (4, "pattern_count", int)
    list_of_sound_chips = (32, "list_of_sound_chips", (list, 1), validate_chips)
    sound_chip_volumes = (32, "sound_chip_volumes", (list, 1))
    sound_chip_pannings = (32, "sound_chip_pannings", (list, 1))
    sound_chip_flags = (128, "sound_chip_flags", bytes)
    song_name = (0, "song_name", str)
    song_author = (0, "song_author", str)
    a4_tuning = (4, "a4_tuning", float)
    _Skip0 = (20, "", None)
    instrument_addresses = ("instrument_count", "instrument_addresses", (list, 4))
    wave_table_addresses = ("wave_table_count", "wave_table_addresses", (list, 4))
    sample_addresses = ("sample_count", "sample_addresses", (list, 4))
    pattern_addresses = ("pattern_count", "pattern_addresses", (list, 4))
    orders = (load_orders,)
    column_effects = (CHANNEL_COUNT, "column_effects", (list, 1))


class FurInstrumentStructure(Enum):
    block_id = (4, "block_id", str)
    block_size = (4, "block_size", int)
    version = (2, "version", int)
    type_ = (2, "type", int)
    features = (load_instrument_features,)


class FurInstrumentFeatureStructure(Enum):
    feature_code = (2, "feature_code", str)
    block_size = (2, "block_size", int)
    data = (load_instrument_feature,)


class FurPatternStructure(Enum):
    block_id = (4, "block_id", str)
    block_size = (4, "block_size", int)
    subsong = (1, "subsong", int)
    channel = (1, "channel", int)
    pattern_index = (2, "pattern_index", int)
    pattern_name = (0, "pattern_name", str)
    pattern_data = (load_pattern_data,)


def deserialize(type, src: Reader, structure: Enum):
    o = type()
    for field in structure:
        if len(field.value) == 1:
            if callable(field.value[0]):
                field.value[0](o, src)
                continue
            else:
                raise ValueError(f"Unknown data type {field.value}")
        elif len(field.value) == 3:
            size, fieldname, datatype = field.value
            validate = lambda x: True
        elif len(field.value) == 4:
            size, fieldname, datatype, validate = field.value
        else:
            raise ValueError(f"Unknown data type {field.value}")

        if isinstance(size, str):
            if not isinstance(datatype, tuple):
                raise ValueError(f"Unknown data type {datatype}")
            size = getattr(o, size) * datatype[1]

        if size == 0 and datatype == str:
            data = bytearray()
            while True:
                b = src.readu8()
                if b == 0:
                    break
                data.append(b)
        else:
            data = src.read(size)

        if datatype is None:
            continue

        if datatype == str:
            setattr(o, fieldname, data.decode("utf-8"))
        elif datatype == int:
            setattr(o, fieldname, int.from_bytes(data, byteorder="little"))
        elif datatype == float:
            setattr(o, fieldname, struct.unpack("<f", data)[0])
        elif datatype == bytes:
            setattr(o, fieldname, data)
        else:
            datatype, subsize = datatype
            if size == 0:
                size = len(data)

            if datatype == list:
                arr = [
                    int.from_bytes(data[i : i + subsize], byteorder="little")
                    for i in range(0, size, subsize)
                ]
                setattr(o, fieldname, arr)
            else:
                raise ValueError(f"Unknown data type {datatype}")

        validate(getattr(o, fieldname))

    return o


class FurSongInfo:
    def __init__(self):
        self.block_id: str = None
        self.block_size: int = None
        self.time_base: int = None
        self.speed1: int = None
        self.speed2: int = None
        self.init_arpegio_time: int = None
        self.tick_per_second: float = None
        self.pattern_length: int = None
        self.orders_length: int = None
        self.highlight_a: int = None
        self.highlight_b: int = None
        self.instrument_count: int = None
        self.wave_table_count: int = None
        self.sample_count: int = None
        self.pattern_count: int = None
        self.list_of_sound_chips: list = None
        self.sound_chip_volumes: list = None
        self.sound_chip_pannings: list = None
        self.sound_chip_flags: bytes = None
        self.song_name: str = None
        self.song_author: str = None
        self.a4_tuning: float = None
        self.instrument_addresses: List[int] = None
        self.wave_table_addresses: List[int] = None
        self.sample_addresses: List[int] = None
        self.pattern_addresses: List[int] = None
        self.orders: List[List[int]] = None
        self.column_effects: list = None


class FurInstrument:
    def __init__(self):
        self.block_id: str = None
        self.block_size: int = None
        self.version: int = None
        self.type: int = None
        self.features: List[FurFeature] = []

    def get_features_parameters(self, feature_code: str) -> Dict[str, int]:
        for f in self.features:
            if f.feature_code == feature_code:
                return f.parameters()
        return {}

    def get_macros(self) -> List[FurMacro]:
        for f in self.features:
            if f.feature_code == "MA":
                return f.get_macros()
        return []

    def get_name(self) -> str:
        for f in self.features:
            if f.feature_code == "NA":
                return f.get_as_nz_string()


class FurFeature:
    def __init__(self):
        self.feature_code: str = None
        self.block_size: int = None
        self.data: bytes = None

    def parameters(self) -> Dict[str, int]:
        if self.feature_code == "FM":
            return self.fm_parameters()
        return {}

    def get_macros(self) -> List[FurMacro]:
        result: List[FurMacro] = []
        r = Reader(self.data)
        size = r.readu16()
        while not r.eof():
            macro = FurMacro()

            macro.code = r.readu8()
            if macro.code == 0xFF:
                break
            macro.length = r.readu8()
            macro.loop = r.readu8()
            macro.release = r.readu8()
            macro.mode = r.readu8()
            t = r.readu8()
            macro.type_ = (t >> 1) & 0x03
            if macro.type_ != 0x00:
                raise FurError(f"unsupported macro type {macro.type_}")

            t &= (0xC0) >> 6
            macro.wordsize = [1, 1, 2, 4][t]
            macro.signed = [False, True, True, True][t]
            macro.delay = r.readu8()
            macro.speed = r.readu8()
            macro.data = r.read_array_of_int(macro.length, macro.wordsize)

            result.append(macro)
        return result

    def get_as_nz_string(self) -> str:
        r = Reader(self.data)
        return r.read_string(len(self.data) - 1)

    def fm_parameters(self) -> Dict[str, int]:
        params = defaultdict(lambda: [0, 0, 0, 0])
        r = Reader(self.data)

        flags = r.readu8()
        params["furnace_flags"] = flags

        feedback_algo = bits(r.readu8())
        fms2asmfms = bits(r.readu8())
        am2llpatch = bits(r.readu8())

        # on YM2612 we have 4 operators, Furnace store them in this order...
        for i in [0, 2, 1, 3]:
            ksr_dt_ml = bits(r.readu8())
            s_tl = bits(r.readu8())
            rs_v_ar = bits(r.readu8())
            am_ksl_dr = bits(r.readu8())
            egt_kvs_d2r = bits(r.readu8())
            sl_rr = bits(r.readu8())
            dvb_ssg = bits(r.readu8())
            dam_dt2_ws = bits(r.readu8())

            params["ksr"][i] = ksr_dt_ml(7, 8)
            params["dt"][i] = ksr_dt_ml(4, 7)
            params["ml"][i] = ksr_dt_ml(0, 4)
            params["sus"][i] = s_tl(7, 8)
            params["tl"][i] = s_tl(0, 7)
            params["rs"][i] = rs_v_ar(6, 8)
            params["v"][i] = rs_v_ar(5, 6)
            params["ar"][i] = rs_v_ar(0, 5)
            params["am"][i] = am_ksl_dr(7, 8)
            params["ksl"][i] = am_ksl_dr(5, 7)
            params["dr"][i] = am_ksl_dr(0, 5)
            params["egt"][i] = egt_kvs_d2r(7, 8)
            params["kvs"][i] = egt_kvs_d2r(5, 7)
            params["d2r"][i] = egt_kvs_d2r(0, 5)
            params["s"][i] = sl_rr(4, 7)
            params["rr"][i] = sl_rr(0, 4)
            params["dvb"][i] = dvb_ssg(4, 7)
            params["ssg"][i] = dvb_ssg(0, 4)
            params["dam"][i] = dam_dt2_ws(5, 7)
            params["dt2"][i] = dam_dt2_ws(3, 5)
            params["ws"][i] = dam_dt2_ws(0, 3)

        params["feedback"] = feedback_algo(0, 3)
        params["algorithm"] = feedback_algo(4, 7)
        params["fms2"] = fms2asmfms(5, 8)
        params["ams"] = fms2asmfms(3, 5)
        params["fms"] = fms2asmfms(0, 3)
        params["am2"] = am2llpatch(6, 8)
        params["llpatch"] = am2llpatch(0, 5)

        return params

    def __repr__(self):
        return f"{self.__class__.__name__}({self.feature_code}) data={self.data}"


def bits(v: int):
    def eval(bit0, bit1) -> int:
        return (v >> bit0) & ((1 << (bit1 - bit0)) - 1)

    return eval


class FurPattern:
    def __init__(self):
        self.block_id: str = None
        self.block_size: int = None
        self.subsong: int = None
        self.channel: int = None
        self.pattern_index: int = None
        self.pattern_name: str = None
        self.pattern_data: List[FurPatternCell] = []

    def __repr__(self):
        return f"FurPattern(block_id={self.block_id}, block_size ={self.block_size}, subsong={self.subsong}, channel={self.channel }, pattern_index={self.pattern_index}, pattern_name={self.pattern_name }, pattern_data={self.pattern_data})"


class FurPatternCell:
    def __init__(self):
        self.note = None
        self.instrument = None
        self.volume = None
        self.fx = []

    def __repr__(self):
        return f"FurPatternCell(note={self.note}, instrument={self.instrument}, volume={self.volume}, fx={self.fx})"


class FurMacro:
    def __init__(self):
        self.code = None
        self.length = None
        self.loop = None
        self.release = None
        self.mode = None
        self.type_ = None
        self.wordsize = None
        self.signed = None
        self.delay = None
        self.speed = None
        self.data = None

    def __repr__(self):
        return f"FurMacro(code={self.code}, length={self.length}, loop={self.loop}, release={self.release}, mode={self.mode}, type_={self.type_}, wordsize={self.wordsize}, signed={self.signed}, delay={self.delay}, speed={self.speed}, data={self.data})"


class Fur:
    def __init__(self, r: Reader):
        self.songinfo: FurSongInfo = deserialize(FurSongInfo, r, FurSongInfoStructure)
        self.instruments: List[FurInstrument] = []
        self.patterns: List[FurPattern] = []

    def get_pattern(self, channelid, patternid) -> FurPattern:
        for p in self.patterns:
            if p.channel == channelid and p.pattern_index == patternid:
                return p
        return None

    def load_instruments(self, r: Reader):
        for addr in self.songinfo.instrument_addresses:
            insread = r.new_reader_from(addr)
            instrument: FurInstrument = deserialize(
                FurInstrument, insread, FurInstrumentStructure
            )
            self.instruments.append(instrument)
            for f in instrument.features:
                if f.feature_code == "MA":
                    f.get_macros()

    def load_patterns(self, r: Reader):
        for addr in self.songinfo.pattern_addresses:
            patread = r.new_reader_from(addr)
            pattern = deserialize(FurPattern, patread, FurPatternStructure)
            self.patterns.append(pattern)

    def __repr__(self):
        return f"Fur(songinfo={self.songinfo}, instruments={self.instruments}, patterns={self.patterns})"


class PeltError(RuntimeError):
    pass


def decompress(in_file) -> bytes:
    with open(in_file, "rb") as f:
        return zlib.decompress(f.read())


def load_song_info(r: Reader) -> Fur:
    furSong = Fur(r)
    return furSong


def load_fur(in_file):
    logger.info(f"Loading {in_file}")
    fur_data = decompress(in_file)

    logger.info(f"Loaded {len(fur_data)} bytes")

    r = Reader(fur_data)
    magic = r.read_string(16)
    if magic != "-Furnace module-":
        raise PeltError(f"not a furnace module")

    version = r.readu16()
    logger.debug(f" Version: {version}")
    if version < 157:
        raise PeltError(f"unsupported version lesser than 0.6pre5")
    r.skip(2)

    song_info_reader = r.new_reader_from(r.readu32())
    furSong: Fur = load_song_info(song_info_reader)
    furSong.load_instruments(r)
    furSong.load_patterns(r)

    print(furSong)


if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument("in_file", type=str, help=".fur file to read")

    logging.basicConfig(level=logging.INFO)
    logger.setLevel(logging.DEBUG)

    args = argparser.parse_args()
    load_fur(args.in_file)
