# No Sound Protocol Messages

Le protocole audio du driver no sound fonctionne sous la base de commandes
audios encapsulée dans des trames. Une trame correspond à l'unité fonctionnelle
de temps la plus petite.

Le messages sont une suite d'octets permettant de communiquer avec divers
éléments du driver.

## Structure d'une commande

Une commande NOS est structurée en suite d'octets.

   HH B0 B1 .. Bn

Le byte HH indique le nom du message et les octets subséquents, le contenu
du message.

## Catégorie de commande

Il existe deux catégories de messages:

 1. Les commandes de status concernent le status général du composant. Les  status sont des valeurs générales. Un status ne change pas tant qu'il n'a pas été sujet d'une demande de changement par une nouvelle commande.
 2. Les commandes de voix affectent les différents cannaux audio de No Sound.

Le bit le plus significatif du header indique la catégorie de la commande.

    Txxxxxxx

Donc,
 - les commande $00 - $7F sont des status.
 - les commandes $80 - $FF sont des commandes de voix.

> <!> Certaines commandes ou groupe de commande affichent le symbole de section (§). Cela indique que la commande ne doit pas être executée via le bus entre le 68k et le Z80 puisqu'elle dépend d'un contexte plus complexe propre à l'état du driver lorsque la commande est exécutée. La commande n'a aucun sens hors contexte.

## Source de commande

Comme la MD est un système ayant des ressources limités, le système
de messagerie des commandes n'est pas complètement centralisé. Les
commandes peuvent provenir de plusieurs de l'une des deux sources
suivantes.

 1. Séquenceur : le séquençage est un stream de commandes immutable lu par NOS grâce à un pointeur de playback. (PP). Les commandes de séquence permettent l'execution de partition musicale préenregistrée. Elle sont structurés sous la forme de commandes de voix pour des channels ainsi que de commandes de temporalité.
 2. Queue : la queue est un stream non constant de commandes provenant d'execution externe. Dans le cas de la MD, c'est le 68k qui va émettre ces commandes. Sur d'autre plateforme, ce serait seulement le programme principal. L'objectif est de permettre un agent externe de piloter le comportement du driver.

## Commands de voix

### Contrôle global

Les commandes de contrôle global

| header | nom | taille | description |
|--------|-----|--------|-------------|
| $01 | Song Load | 5 | Demande au driver de charger une partition. Le driver ne peut pas lire dans la RAM du 68k. Seulement dans la ROM. <br>`$01 pppp oooo`<br>pppp: page dans la mémoire du 68k. <br>oooo: offset dans la page additionné de 0x8000 (c'est l'offset interne du z80). |
| $02 | Test | 1 | Sert uniquement au développement |
| $08 | Play Sound FX (SFX) | 5 | The SFX isn't buffered but played immediately. The host CPU should buffer and manage the priorities of the SFX so only one Play command is emited at a given frame. Otherwise, the driver might get overloaded with SFX requests and even produce audio glitches. <br><br>`$68 pppp oooo`<br> pppp: page bank<br>oooo: offset in the page. The address must be offset by `$8000` to accomodate the driver |
| $0A | Play | 1 | Lance le playback au pattern 0. (Combinaison de Stop-Resume) |
| $0B | Resume | 1 | Lance le playback à la position du curseur d'exécution. |
| $0C | Stop   | 1      | arrête le playback ramène le pointeur de chanson au pattern 0. La trame en cours se termine avant l'a mise sur pause. Silence total. |
| $0F | Reset driver | 1 | Réinnialise tous les états du driver. |

### Rythmique et temporalité

Les commandes de temporalité servent à gérer le tempo, la vitesse
d'execution etc.

| header | nom | taille | description |
|--------|-----|--------|-------------|
| $10    | Set clock divider | 3 | Cette commande est la façon de paramétrer le tempo. La méthode de calcul est expliquée ci-bas. <br>`$11 nn dd`<br>nn : numérateur<br>dd: dénominateur.<br>
| $18    | Pattern Clock | 2 | indique le début d'un nouveau pattern. <br>`$18 nn`<br>nn : id du pattern |
| $19    | Frame Clock | 1 | indique une nouvelle trame, indique au séquenceur d'attendre une clock avant de continuer |
| $1A    | Repeated Sequencer Frame Clock | 2 | indique une série de nouvelles trame. Le séquenceur doit attendre `nn` clocks avant de continuer.<br>`$1A nn`<br>nn: nombre de clocks. |

#### Set Clock divider

Définissions un peu de vocabulaire, puisque ce concept peu devenir
très rapidement aride.

* La *Clock* correspond à l'unité temporelle la plus petite. Sur
  un système NTSC, il y a 60 clocks par secondes, sur PAL, 50.
* Une *Trame* correspond à la plus petite subdivision musicale.
  Normalement, dans un tracker, c'est une rangée.
* Un *Beat* correspond à une note noire. La note noire est
  l'unité de référence pour calculer le tempo.

Une trame est généralement émise à une fraction de la clock. Pour créer un tempo donné, sur le Z80, nous utilisons deux nombres qui représentent une fraction de sorte à éviter des calculs décimaux complexes. Cette fraction se nomme diviseur de clock.

Un timer T est utilisé pour garder une valeur de temps. Lorsqu'une clock est émise, T est réduit par le numérateur `nn`. Lorsque T <= 0,
un signal une trame est émise et T est augmenté par le dénominateur `dd`.

Par exemple, supposons que nous voulons 130 BPM, et qu'un beat est subdivisé en 8 trames (8 rangées dans un tracker, des triples croches). Pour calculer `nn / dd`:

      nn/dd = BPM/(Trames*VSyncFreq)

For example, let's assume that we want 130 BPM, and that a beat is subdivided into 8 steps (8 rows in a tracker, like triplets). To calculate nn / dd :

   * NTSC : 60 clocks per seconds
   * 130 BPM = 130/60 (2.16) beats per seconds
   * 60/60 clock*s/s * 4 row/beat * 130/60 beats/s = 26/3 row * clock/ s
   * effectively `nn/dd = 4*130/60`
   * `nn` = 26, `dd` = 3

| Clock | T | # Trame |
|------|------|------|
| 1 | 48 | 1 |
| 2 | 35 | |
| 3 | 22 | |
| 4 | 9 | 2 |
| 5 | 44 | |
| 6 | 31 | |
| 7 | 18 | |
| 8 | 5 | 3 |
| 9 | 40 |  |
| 10 | 27 | |
| 11 | 14 | |
| 12 | 1 | 4 |
| 13 | 36 | |
| 14 | 23 | |
| 15 | 10 | 5 |
| 16 | 45 | |
| 17 | 32 | |
| 18 | 19 | 6 |
| 19 | 6 | |
| ... |  | |

En pal:
   * PAL : 50 clocks par secondes
   * 130 BPM = 130/60 (2.16) beats par secondes
   * 60/50 s/clock*s * 1/8 trames/beat * 130/60 beats/s ~= 13/40 trames/clock * s
   * PAL: `nn/dd = 130/(8*50)`
   * `nn` = 13, `dd` = 40

Nous avons donc parfois 3 clocks entre chaque trames, ou parfois 4. Une clock ayant une durée 13ms, cet oscillation n'est pas perceptible à l'oreille pour la plupart d'entre-nous.

### Séquenceur

Le commande de séquences sont des commandes impératives.

| header | nom | taille | description |
|--------|-----|--------|-------------|
| $20    | Set FM Instrument on current channel | 2 | `$20 pp`<br>pp: #patch |
| $21    | Set PSG Instrument on current channel | 2 | `$21 pp`<br>pp: #patch |
| $2C    | Mute FM channels | 2 | `$2C mm`<br>mm: each bits indicates if the channel is muted during song playback. |
| $2D    | Mute PSG channels | 2 | `$2D mm`<br>mm: each bits indicates if the channel is muted during song playback. |

### Key on/off
| header | nom | taille | description |
|--------|-----|--------|-------------|
| $30    | FM Channel Key off | 1 | |
| $31    | SN Channel Key off | 1 | |

### FX commands
| header | nom | taille | description |
|--------|-----|--------|-------------|
| $60    | FM Set Instrument Attenuation (§) | 2 | `$60 aa`<br> aa: attenuation (00-7f) |
| $61    | SN Set Instrument Attenuation (§) | 2 | `$61 aa`<br> aa: attenuation (00-0f) |

### Key-on commands

Commands from `$80` to `$FF` are key in the current channel. The value
of the command is the note to play.
