# Quelques notes d'optimisations

## Incrémenter une valeur en RAM

|   |   |   |
|---|---|---|
| Code  |`ld      HL, VAR`<br>`inc     (HL)`  | `ld      A, (VAR)`<br>`inc     A`<br>`ld      (VAR), A`  |
| Cycles  | 21  | 30  |

## Étendre 8bit signé à 16bit

    ld      L, A        ; lets extend A to 16 bits 
    add     A, A        ; push sign bit into carry
    sbc     A, A        ; A = 0 if carry == 0 else $FF
    ld      H, A        ; now LH contains the extended version of A

## Addition 8bit à 16bit

    add     A, L        ; perform HL + A
    ld      L, A
    adc     A, H
    sub     A, L
    ld      H, A        ; answer in HL

