#!/bin/bash

ROM=./out.bin
BLASTEM=blastem

# run the emulator in half full_screen mode
#$BLASTEM "$ROM" 1440 1080
$BLASTEM "$ROM" 800 600
