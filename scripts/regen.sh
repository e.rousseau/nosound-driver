#!/bin/bash

PYTHON="python"

set -e
python3 ./src/python/nos-tools-gen.py nos_decode_table

# generate song
$PYTHON ./src/python/nos-tools-fur.py laraignee.fur -o laraignee.yaml
$PYTHON ./src/python/nos-tools-gen.py module -i laraignee.yaml -t clang -o ./demo/song.c
$PYTHON ./src/python/nos-tools-gen.py module -i laraignee.yaml -t bin -o ./song.bin

# generate sfx
$PYTHON ./src/python/nos-tools-fur.py ./sfx/sfx_spell_0.fur -o ./sfx/sfx_spell_0.yaml
$PYTHON ./src/python/nos-tools-gen.py sfx -i ./sfx/sfx_spell_0.yaml -t clang -o ./demo/sfx_spell_0.c -channel 0

$PYTHON ./src/python/nos-tools-fur.py ./sfx/sfx_gingle_0.fur -o ./sfx/sfx_gingle_0.yaml
$PYTHON ./src/python/nos-tools-gen.py sfx -i ./sfx/sfx_gingle_0.yaml -t clang -o ./demo/sfx_gingle_0.c -channel 7
$PYTHON ./src/python/nos-tools-gen.py sfx -i ./sfx/sfx_gingle_0.yaml -t bin -o ./sfx.bin -channel 7

