#!/bin/bash

ROM=./out.bin
MEGALINK=$(which megalink)
# changer ceci pour /dev/ttyACM0 ou /dev/ttyACM1 si l'autre ne 
# fonctionne pas. Ne pas oublier aussi d'inscrire l'utilisateur
# en cours comme membre du groupe "dialout".
DEV="/dev/ttyACM0"

# upload the ROM to the Mega Everdrive
$MEGALINK -s $DEV run "$ROM"
