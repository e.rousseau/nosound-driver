#
# NOSOUND driver for sega mega drive
# Copyright(c) 2023 Emmanuel Rousseau;
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all; copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

import io
from typing import List, NamedTuple, Tuple

col_size = 80
tab_size = 4


class ByteArrayBuilder:
    def __init__(self):
        self.data = []

    def append(self, i: int, w: int):
        r = [0] * w
        p = 0
        while i > 0:
            v = i & 0xFF
            r[p] = v
            i >>= 8
            p += 1
        self.data.extend(r)

    def concat(self, b: "ByteArrayBuilder"):
        self.data.extend(b.data)

    def __len__(self) -> int:
        return len(self.data)

    def fill(self, v: int, n: int):
        self.data.extend([v] * n)


class Segment:
    def __init__(self, offset: int, size: int, last: bool):
        self.offset: int = offset
        self.size: int = size
        self.last: bool = last


class SegmentedBinaryFile:
    def __init__(self, max_chunk_size: int = 0x7FFF):
        self._bytes: List[bytes] = []
        self._size: int = 0
        self._max_chunk_size: int = max_chunk_size

    def append(self, data: bytes):
        self._bytes.append(data)
        self._size += len(data)

    def __len__(self) -> int:
        return self._size

    def get_data(self) -> Tuple[List[bytes], List[Segment]]:
        cur_chunk: bytes = b""
        chunks: List[bytes] = []
        size = 0
        segments: List[Segment] = []

        for seg in self._bytes:
            if len(cur_chunk) > 0x7FFF:
                raise ValueError(f"one of the data is too large to fit {0x7fff} bytes")

            s = Segment(size, len(seg), False)

            if len(cur_chunk) + len(seg) <= self._max_chunk_size:
                cur_chunk += seg
                size += len(seg)
            else:
                segments[-1].last = True
                chunks.append(cur_chunk)
                cur_chunk = seg
                size = 0

            segments.append(s)

        segments[-1].last = True
        chunks.append(cur_chunk)

        return chunks, segments


class Reader:
    def __init__(self, data: bytes):
        self.data: bytes = data
        self.ptr = 0

    def read(self, size: int) -> bytes:
        data = self.data[self.ptr : self.ptr + size]
        self.ptr += size
        return data

    def read_string(self, size: int) -> str:
        data = self.data[self.ptr : self.ptr + size]
        self.ptr += size
        return data.decode("utf-8")

    def readu8(self) -> int:
        data = self.data[self.ptr]
        self.ptr += 1
        return data

    def readu16(self) -> int:
        data = self.data[self.ptr : self.ptr + 2]
        self.ptr += 2
        return int.from_bytes(data, byteorder="little")

    def readu32(self) -> int:
        data = self.data[self.ptr : self.ptr + 4]
        self.ptr += 4
        return int.from_bytes(data, byteorder="little")

    def read_array_of_int(self, elem_count, elem_size) -> list:
        data = self.data[self.ptr : self.ptr + elem_count * elem_size]
        self.ptr += elem_count * elem_size
        return [
            int.from_bytes(data[i : i + elem_size], byteorder="little")
            for i in range(0, elem_count * elem_size, elem_size)
        ]

    def read_bytes(self, size: int) -> bytes:
        data = self.data[self.ptr : self.ptr + size]
        self.ptr += size
        return data

    def skip(self, size: int) -> None:
        self.ptr += size

    def new_reader_from(self, offset: int) -> "Reader":
        return Reader(self.data[offset:])

    def new_reader(self) -> "Reader":
        return Reader(self.data[self.ptr :])

    def eof(self) -> bool:
        return self.ptr >= len(self.data)


def as_z80_db(array, max_per_line=255) -> str:
    buf = io.StringIO()
    tab = " " * tab_size
    newline = f"{tab}db "

    line = newline
    elems = 0

    for d in array:
        extend = f"${d:02x},"

        if len(line) + len(extend) >= col_size or elems >= max_per_line:
            line = line[:-1]
            buf.write(line + "\n")
            line = newline
            elems = 0

        line += extend
        elems += 1

    if line != newline:
        line = line[:-1]
        buf.write(line + "\n")

    return buf.getvalue()


def as_z80_db_variable(byte_data):
    if byte_data < 0 or byte_data > 255:
        raise ValueError(f"Invalid byte_data {byte_data}")
    return as_z80_db([byte_data])


def as_z80_dw_variable(word_data):
    if word_data < 0 or word_data > 0xFFFF:
        raise ValueError(f"Invalid word_data {word_data}")
    return as_z80_dw([word_data])


def as_z80_dw_str(array, max_per_line=255):
    buf = io.StringIO()
    tab = " " * tab_size
    newline = f"{tab}dw "

    line = newline
    elems = 0

    for d in array:
        extend = f"{d}, "

        if len(line) + len(extend) >= col_size or elems >= max_per_line:
            line = line[:-2]
            buf.write(line + "\n")
            line = newline
            elems = 0

        line += extend
        elems += 1

    if line != newline:
        line = line[:-2]
        buf.write(line + "\n")

    return buf.getvalue()


def as_z80_dw(array, max_per_line=255):
    buf = io.StringIO()
    tab = " " * tab_size
    newline = f"{tab}dw "

    line = newline
    elems = 0

    for d in array:
        extend = f"${d:04x},"

        if len(line) + len(extend) >= col_size or elems >= max_per_line:
            line = line[:-1]
            buf.write(line + "\n")
            line = newline
            elems = 0

        line += extend
        elems += 1

    if line != newline:
        line = line[:-1]
        buf.write(line + "\n")

    return buf.getvalue()
