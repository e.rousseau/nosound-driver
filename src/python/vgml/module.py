#
# NOSOUND driver for sega mega drive
# Copyright(c) 2023 Emmanuel Rousseau;
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all; copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

from __future__ import annotations
import yaml
from typing import Dict, List, Tuple
import logging
import fractions

logger = logging.getLogger(__name__)

KEY_OFF_CMD = "k_off"
INSTRUMENT_CMD = "instr"
ATTENUATION_CMD = "attenuation"


KEY_ON_GENERIC_CMD = 0x80


class ExportError(RuntimeError):
    pass


class Sheet:
    def __init__(self, filepath: str):
        """
        load a YAML file containing a pattern based module for the SEGA
        Genesis.
        """
        self._filepath: str = filepath
        self._patterns: List[int] = []
        self.pattern_length: int = 0
        self.fm_instruments: List[FmNormalInstrument] = []
        self.sn_instruments: List[SN76489Instrument] = []

        # maps global instrument id to typed instrument id (fm or PSG)
        self._instrument_global_to_typed: Dict[int, int] = {}
        self._cur_columns: List[Column] = []
        self._all_cols: List[Column] = []

        self._pattern_started: bool = False

        self._tempo: int = 120
        self._rows_per_beat: int = 4
        self._virtual_tempo_num: int = 1
        self._virtual_tempo_den: int = 1
        self._ticks_per_seconds: int = 60
        self._speed: int = 1

        self._channels: List[FmChannel] = [
            FmChannel(),
            FmChannel(),
            FmChannel(),
            FmChannel(),
            FmChannel(),
            FmChannel(),
            PsgChannel(),
            PsgChannel(),
            PsgChannel(),
            PsgChannel(),
        ]
        self._channel_count: int = len(self._channels)
        self._used_channels: List[bool] = [False] * self._channel_count

        self.comment: str = ""

    def load(self):
        logger.debug("loading sheet %s", self._filepath)
        with open(self._filepath, "r") as f:
            data = yaml.load(f, Loader=yaml.FullLoader)

        if "metadata" in data:
            if "comment" in data["metadata"]:
                self.comment = data["metadata"]["comment"]

        patterns = data["patterns"]
        self.pattern_length = data["pattern_size"]
        pattern_count = len(patterns)

        if pattern_count == 0:
            return

        tempo = data["tempo"]
        self._tempo = int(tempo["tempo_bpm"])
        self._rows_per_beat = int(tempo["rows_per_beat"])
        self._ticks_per_seconds = int(tempo["ticks_per_second"])
        self._speed = int(tempo["speed"])
        virtual_tempo = tempo["virtual_tempo"]
        self._virtual_tempo_num = virtual_tempo[0]
        self._virtual_tempo_den = virtual_tempo[1]

        self.load_instruments(data)

        for pattern in patterns:
            self._cur_columns = []
            for i in range(self._channel_count):
                self._cur_columns.append(Column(self.pattern_length, i < 6))
                self._patterns.append(len(self._all_cols))
                self._all_cols.append(self._cur_columns[i])

            self.start_pattern()
            for row in pattern:
                self.start_frame()
                for c, event in enumerate(row):
                    self.push_event(c, event)

                self.end_frame()
            self.end_pattern()
        self.compress()

    def load_instruments(self, data):
        instruments = data["instruments"]
        for i, instrument in enumerate(instruments):
            id = self.load_instrument(instrument)
            self._instrument_global_to_typed[i] = id

    def load_instrument(self, instrument_data) -> int:
        if len(instrument_data) == 0:
            return
        if instrument_data["type"] == "sn76489":
            instrument = SN76489Instrument(instrument_data["parameters"])
            self.sn_instruments.append(instrument)
            return len(self.sn_instruments) - 1
        if instrument_data["type"] == "fm_ym2612_normal":
            instrument = FmNormalInstrument(instrument_data["parameters"])
            self.fm_instruments.append(instrument)
            return len(self.fm_instruments) - 1

    def compress(self):
        compressed_cols: List[Column] = []
        compressed_patterns: List[int] = []
        for c_id in self._patterns:
            if self._all_cols[c_id].empty():
                compressed_patterns.append(0xFFFF)
                continue

            for j, cc in enumerate(compressed_cols):
                if cc.equals(self._all_cols[c_id]):
                    compressed_patterns.append(j)
                    break
            else:
                compressed_patterns.append(len(compressed_cols))
                compressed_cols.append(self._all_cols[c_id])

        self._all_cols = compressed_cols
        self._patterns = compressed_patterns

    def get_tempo_fraction(
        self,
        base_freq: int,
        max_denom: int = 255,
    ) -> fractions.Fraction:
        """
        returns the tempo in discrete rational number. base_freq is frequency
        of the smallest increment for the target system. On NTSC systems it's 60
        on PAL systems its 50... nothing prevents a system to have an even
        faster base frequency.
        """
        rows_per_seconds = (
            self._rows_per_beat * self._tempo / (self._ticks_per_seconds / self._speed)
        )

        if rows_per_seconds == 0:
            raise ExportError(
                f"Can't get tempo fraction for {self._filepath}, tempo values "
                + "leads to rows_per_seconds={rows_per_seconds}"
            )
        update_period = base_freq / rows_per_seconds
        return fractions.Fraction(update_period).limit_denominator(max_denom)

    def get_sfx_speed(self) -> int:
        return self._speed

    def get_sfx_stream(self) -> Tuple[List[int], bool]:
        """sfx uses only one column, lets return the stream for that column"""
        if self.get_used_channel_count() != 1:
            raise ExportError(
                f"Sfx only support one channel and only one, has {self.get_used_channel_count()}"
            )

        fm = False
        column: Column = None
        for c in self._all_cols:
            if c.contains_events:
                if c.fm:
                    fm = True
                column = c
                break

        if c is None:
            raise ExportError(f"Can't find any column with events")

        return column.get_data(), fm

    def get_stream_and_pattern_offsets(self) -> Song:
        stream: List[int] = []
        patterns_offset: List[int] = []

        pattern_to_offset: Dict[int, int] = {}

        for i, col in enumerate(self._all_cols):
            pattern_to_offset[i] = len(stream)
            stream.extend(col.get_data())

        for p in self._patterns:
            if p != 0xFFFF:
                patterns_offset.append(pattern_to_offset[p])
            else:
                patterns_offset.append(0xFFFF)
        return Song(stream, patterns_offset)

    def get_pattern_count(self) -> int:
        return int(len(self._patterns) / self._channel_count)

    def get_pattern_length(self) -> int:
        return self.pattern_length

    def start_pattern(self):
        pass

    def end_pattern(self):
        for c in self._cur_columns:
            c.terminate()
        self._pattern_started = False

    def start_frame(self):
        pass

    def end_frame(self):
        for c in self._cur_columns:
            c.signal_end_frame()
        self._pattern_started = True

    def push_key_on_cmd(self, channel: int, octave: int, note: str):
        c = self._channels[channel]
        cmd = c.get_key_on(octave, note)
        self._cur_columns[channel].push_event(cmd)

    def push_key_on_volume_cmd(self, channel: int, octave: int, note: str, volume: int):
        c = self._channels[channel]
        cmd = c.get_key_on(octave, note)
        self._cur_columns[channel].push_event(cmd)
        self.push_attenuation_cmd(channel, volume)

    def push_key_off_cmd(self, channel: int):
        c = self._channels[channel]
        cmd = c.get_key_off()
        self._cur_columns[channel].push_event(cmd)

    def push_instrument(self, channel: int, instrument: int):
        c = self._channels[channel]
        instrument_id = self._instrument_global_to_typed[instrument]

        if c.is_fm():
            self.fm_instruments[instrument_id].use_count += 1
        else:
            self.sn_instruments[instrument_id].use_count += 1

        cmd = c.get_instrument(instrument_id)
        self._cur_columns[channel].push_event(cmd)

    def push_attenuation_cmd(self, channel: int, volume: int):
        c = self._channels[channel]
        cmd = c.get_attenuation(volume, force=not self._pattern_started)
        self._cur_columns[channel].push_event(cmd)

    def push_event(self, channel: int, entry: str):
        if entry != "---":
            self._used_channels[channel] = True

        params = entry.split("|")
        note_octave = params[0]
        volume = None
        instrument = None

        for p in params[1:]:
            if p.startswith("v"):
                volume = int(p[1:], 16)
            if p.startswith("i"):
                instrument = int(p[1:], 16)

        if note_octave == "---":
            note_octave = ""
        elif note_octave == "^--":
            note_octave = ""
            self.push_key_off_cmd(channel)
        else:
            try:
                note_str, octave_str = note_octave.split("-")
            except RuntimeError as e:
                print(note_octave)
                raise e
            octave = int(octave_str)

        if instrument is not None:
            self.push_instrument(channel, instrument)

        if note_octave != "":
            if volume is not None:
                self.push_key_on_volume_cmd(channel, octave, note_str, volume)
            else:
                self.push_key_on_cmd(channel, octave, note_str)
        else:
            if volume is not None:
                self.push_attenuation_cmd(channel, volume)

    def get_used_channel_count(self) -> int:
        return sum((1 if u else 0 for u in self._used_channels))


class Song:
    def __init__(self, stream: List[int], pattern_offsets: List[int]):
        self.stream = stream
        self.pattern_offsets = pattern_offsets

    def __repr__(self):
        return f"{self.stream} {self.pattern_offsets}"


class SequenceMacro:
    def __init__(self, loop: int, release: int, period: int, data: List[int]):
        self.length = len(data)
        self.loop = loop
        self.release = release
        self.period = period * 8
        self.data = data

    def __repr__(self):
        return f"{self.length} {self.loop} {self.release} {self.period} {self.data}"


class SN76489Instrument:
    def __init__(self, data):
        self.use_count: int = 0

        unordered_macros = {}
        for k, macro in data.items():
            if len(macro) == 0:
                unordered_macros[k] = SequenceMacro(0, 0, 0, [])
            else:
                unordered_macros[k] = SequenceMacro(
                    macro["loop"],
                    macro["release"],
                    macro["period"],
                    macro["data"],
                )

        self.amplitude: SequenceMacro = unordered_macros["amplitude"]
        self.argpegio: SequenceMacro = unordered_macros["argpegio"]
        self.duty: SequenceMacro = unordered_macros["duty"]
        self.pitch: SequenceMacro = unordered_macros["pitch"]
        self.pan: SequenceMacro = unordered_macros["pan"]
        self.reset_phase: SequenceMacro = unordered_macros["reset_phase"]

    def is_fm(self) -> bool:
        return False


class FmNormalInstrument:
    def __init__(self, data):
        self.data = []
        self.use_count: int = 0

        dtml = [
            (dt << 4) | (ml) for dt, ml in zip(data["op_detune_1"], data["op_multiple"])
        ]
        tl = data["op_total_level"]
        rsa = [
            (rs << 6) | a
            for rs, a in zip(data["op_rate_scaling"], data["op_attack_rate"])
        ]
        amd = [
            ((1 if am == "true" else 0) << 7) | d1
            for am, d1 in zip(data["op_amplitude_modulation"], data["op_decay_1"])
        ]
        d2 = data["op_decay_2"]
        sr = [(s << 4) | r for s, r in zip(data["op_sustain"], data["op_release"])]
        ssg_eg = data["op_ssg_eg"]

        feedback_algo = (data["feedback"] << 3) | data["algorithm"]

        pan_str = data["panning"]
        pan_left = 0x80 if "L" in pan_str else 0
        pan_right = 0x40 if "R" in pan_str else 0

        panamsfms = (
            pan_left
            | pan_right
            | (data["am_sensitivity"] << 3)
            | data["fm_sensitivity"]
        )

        self.data.extend(dtml)
        self.data.extend(tl)
        self.data.extend(rsa)
        self.data.extend(amd)
        self.data.extend(d2)
        self.data.extend(sr)
        self.data.extend(ssg_eg)
        self.data.append(feedback_algo)
        self.data.append(panamsfms)

    def is_fm(self) -> bool:
        return True


class Column:
    def __init__(self, max_size, fm: bool) -> None:
        self._data: List[int] = []
        self._cumulated_frames: int = 0
        self._max_size: int = max_size
        self._had_notes: bool = False
        self.contains_events = False
        self.fm: bool = fm

    def push_event(self, event):
        self._write_end_frame()

        if len(event) > 0:
            if event[0] >= 0x80:
                self._had_notes = True

        self._data.extend(event)
        self.contains_events = True

    def _write_end_frame(self):
        if not self._had_notes:
            if self._cumulated_frames == 1:
                self._data.append(0x19)
            elif self._cumulated_frames >= 2:
                self._data.extend((0x1A, self._cumulated_frames - 1))
        else:
            if self._cumulated_frames == 2:
                self._data.append(0x19)
            elif self._cumulated_frames >= 3:
                self._data.extend((0x1A, self._cumulated_frames - 2))

        if self._cumulated_frames != 0:
            self._cumulated_frames = 0
            self._had_notes = False

    def signal_end_frame(self):
        self._cumulated_frames += 1

    def terminate(self):
        self._write_end_frame()

    def empty(self):
        return len(self._data) == 0

    def get_data(self) -> List[int]:
        return self._data

    def equals(self, other) -> bool:
        if self == other:
            return True

        if len(self._data) != len(other._data):
            return False

        for i in range(len(self._data)):
            if self._data[i] != other._data[i]:
                return False

        return True


class FmChannel:
    NOTE_OFFSET = {
        "B": 11,
        "C": 0,
        "C#": 1,
        "D": 2,
        "D#": 3,
        "E": 4,
        "F": 5,
        "F#": 6,
        "G": 7,
        "G#": 8,
        "A": 9,
        "A#": 10,
    }

    def __init__(self) -> None:
        self.key_off = 0x30
        self.instrument = 0x20
        self.attenuation = 0x60

        self.current_volume = 0
        self.last_instrument = 0xFFFF

    def _get_note_id(self, octave, note) -> int:
        octave = clamp(octave, 0, 7)
        note_id = FmChannel.NOTE_OFFSET[note] + 1
        note_id += octave * 12

        return note_id

    def get_key_on(self, octave, note):
        note_id = self._get_note_id(octave, note)
        return (KEY_ON_GENERIC_CMD + note_id,)

    def get_key_off(self):
        return (self.key_off,)

    def get_attenuation(self, volume, force=False):
        volume = clamp(volume, 0, 127)
        if volume == self.current_volume and not force:
            return ()
        self.current_attenuation = volume
        return (self.attenuation, volume)

    def get_instrument(self, instrument):
        if self.last_instrument != instrument:
            self.last_instrument = instrument
            return (self.instrument, instrument)
        else:
            return ()

    def is_fm(self) -> bool:
        return True


class PsgChannel:
    NOTE_OFFSET = {
        "A": 9,
        "A#": 10,
        "B": 11,
        "C": 0,
        "C#": 1,
        "D": 2,
        "D#": 3,
        "E": 4,
        "F": 5,
        "F#": 6,
        "G": 7,
        "G#": 8,
    }

    def __init__(self) -> None:
        self.key_off_cmd = 0x31
        self.instrument_cmd = 0x21
        self.attenuation_cmd = 0x61

        self.current_attenuation = 0
        self.last_instrument = 0xFFFF

    def _get_note_id(self, octave, note) -> int:
        note_id = PsgChannel.NOTE_OFFSET[note] - 9
        octave = clamp(octave, 0, 7)

        note_id += octave * 12
        note_id = clamp(note_id, 0, 126)

        return note_id

    def get_key_on(self, octave, note):
        note_id = self._get_note_id(octave, note)
        return (KEY_ON_GENERIC_CMD + note_id,)

    def get_key_off(self):
        return (self.key_off_cmd,)

    def get_attenuation(self, volume, force=False):
        att = 15 - clamp(volume, 0, 15)
        if att == self.current_attenuation and not force:
            return ()
        self.current_attenuation = att
        return (self.attenuation_cmd, att)

    def get_instrument(self, instrument):
        if self.last_instrument != instrument:
            self.last_instrument = instrument
            return (self.instrument_cmd, instrument)
        else:
            return ()

    def is_fm(self) -> bool:
        return False


def clamp(value, min_value, max_value):
    if value < min_value:
        return min_value
    elif value > max_value:
        return max_value
    else:
        return value
