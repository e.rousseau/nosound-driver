#
# NOSOUND driver for sega mega drive
# Copyright(c) 2023 Emmanuel Rousseau;
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all; copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

import argparse
import nosound
from vgml.module import Sheet
from instrument import decompose_fm_instrument
from csnake import CodeWriter, Variable, Enum
from typing import List
import os, io
import logging
from binary import as_z80_db, as_z80_dw_str

logger = logging.getLogger(__name__)


nos_commands: list = [
    (0x01, 0x01 + 1, "CMD_LoadSong", "NosCmdSongLoad"),
    (0x02, 0x02 + 1, "CMD_Test", "NosCmdTest"),
    (0x08, 0x08 + 1, "CMD_PlaySfx", "NosCmdPlaySfx"),
    (0x0A, 0x0A + 1, "CMD_Play", "NosCmdPlay"),
    (0x0B, 0x0B + 1, "CMD_Resume", "NosCmdResume"),
    (0x0C, 0x0C + 1, "CMD_Stop", "NosCmdStop"),
    (0x10, 0x10 + 1, "CMD_SetClockDivider", "NosCmdSetClockDivider"),
    (0x18, 0x18 + 1, "CMD_PatternClock"),
    (0x19, 0x19 + 1, "CMD_FrameClock"),
    (0x1A, 0x1A + 1, "CMD_FrameClockRepeated"),
    (0x20, 0x20 + 1, "CMD_FmSetInstrument"),
    (0x21, 0x21 + 1, "CMD_PsgSetInstrument"),
    (0x2C, 0x2C + 1, "CMD_FmChannelsEnabled", "NosCmdFmChannelsEnabled"),
    (0x2D, 0x2D + 1, "CMD_PsgChannelEnabled", "NosCmdPsgChannelsEnabled"),
    (0x30, 0x30 + 1, "CMD_FmKeyOff"),
    (0x31, 0x31 + 1, "CMD_PsgKeyOff"),
    (0x60, 0x60 + 1, "CMD_FmSetAttenuationCurrentChannel"),
    (0x61, 0x61 + 1, "CMD_PsgSetAttenuationCurrentChannel"),
    (0x80, 0x100, "CMD_KeyOn"),
]


def gen_instr_reg_seq():
    start_addresses = [0x30, 0x31, 0x32]
    max_op = 4 * 7

    for j, start_address in enumerate(start_addresses):
        print(f"; {start_address:02x} based instruments")
        print(f"chan{j}_addr_seq:")
        result = []
        for i in range(max_op):
            result.append(start_address + i * 4)
        result.append(0x80 + start_address)
        result.append(0x84 + start_address)
        result.append(0x00)
        result.append(0x00)

        print(as_z80_db(result, max_per_line=16))


def gen_module(
    song_name: str, sheet_filename: str, export_type: str, output_filename: str
):
    sheet = Sheet(sheet_filename)
    sheet.load()
    module: nosound.Module = nosound.Module.load(sheet)

    if export_type == "bin":
        if output_filename != "STDOUT":
            with open(output_filename, "wb") as f:
                f.write(module.binary)
        else:
            print(as_z80_db(module.binary, max_per_line=16))
    elif export_type == "clang":
        cw = CodeWriter()

        variable = Variable(
            song_name,
            primitive="unsigned char",
            qualifiers="const",
            value=module.binary,
        )

        cw.add_variable_initialization(variable)
        if output_filename != "STDOUT":
            cw.write_to_file(output_filename)
        else:
            print(str(cw))


def gen_sfx(
    sheet_filename: str, export_type: str, output_filename: str, use_channel: int
):
    sfx_name = os.path.basename(sheet_filename)
    sfx_name = os.path.splitext(sfx_name)[0]

    sheet = Sheet(sheet_filename)
    sheet.load()
    sfx: nosound.Sfx = nosound.Sfx.load(sheet, use_channel)

    if export_type == "bin":
        if output_filename != "STDOUT":
            with open(output_filename, "wb") as f:
                f.write(sfx.binary)
        else:
            print(as_z80_db(sfx.binary, max_per_line=16))
    elif export_type == "clang":
        cw = CodeWriter()

        variable = Variable(
            sfx_name,
            primitive="unsigned char",
            qualifiers="const",
            value=sfx.binary,
        )

        cw.add_variable_initialization(variable)
        if output_filename != "STDOUT":
            cw.write_to_file(output_filename)
        else:
            print(str(cw))


def bit_reverse(l):
    l = ((l & 0xF0) >> 4) | ((l & 0x0F) << 4)
    l = ((l & 0xCC) >> 2) | ((l & 0x33) << 2)
    l = ((l & 0xAA) >> 1) | ((l & 0x55) << 1)
    return l


def gen_sn76489_notes():
    clock = 3575611

    def to_psg(f):
        n = int(clock // (32 * f))
        # l = n & 0xF
        # h = (n >> 4) & 0x3F
        l = n & 0xFF
        h = (n >> 8) & 0x3

        return l, h

    def note(note_id) -> float:
        """
        note_id = 0 being A-2. A440 is note 24.
        note frequency is given by 2**(x/12) * 440.0, where x=0 is A4-440
        """
        f_fixed = 440.0
        x = note_id - 24
        return f_fixed * (2 ** (x / 12))

    notes: List[int] = []

    for i in range(8 * 12):
        l, h = to_psg(note(i))
        notes.append(l)
        notes.append(h)

    print(as_z80_db(notes, max_per_line=24))


def gen_ym2612_notes():
    base_freq = [
        617,
        653,
        692,
        733,
        777,
        823,
        872,
        924,
        979,
        1037,
        1099,
        1164,
    ]

    result = []

    for octave in range(8):
        reg_A4_oct = octave << 3

        for note in base_freq:
            reg_A0 = note & 0xFF
            reg_A4 = ((note >> 8) & 0x07) | reg_A4_oct

            result.extend([reg_A4, reg_A0])

    print(as_z80_db(result, max_per_line=16))
    print(f"{len(result)} entries")


def nos_decode_table(asm_jump_filepath: str, c_header_filepath: str):
    # z80 assembler jump table
    memory_block = ["NOS_NoCommand"] * 256
    max_mem = 0

    for e in nos_commands:
        start_address = e[0]
        end_address = e[1]
        name = e[2]
        for i in range(start_address, end_address):
            memory_block[i] = name
            if i > max_mem:
                max_mem = i

    memory_block = memory_block[: max_mem + 1]

    z80_jmp_table: io.StringIO = io.StringIO()
    z80_jmp_table.write("; DO NOT EDIT - this file is automatically generated\n")
    z80_jmp_table.write(as_z80_dw_str(memory_block, max_per_line=16))

    with open(asm_jump_filepath, "w") as f:
        f.write(z80_jmp_table.getvalue())

    # C enum for commands
    cw = CodeWriter()
    cw.start_if_def("E_COMMANDS_H", invert=True)
    cw.add_define("E_COMMANDS_H")
    enum = Enum("nos_cmd_id", typedef=True)
    for e in nos_commands:
        if len(e) >= 4:
            enum.add_value(e[3], e[0])

    cw.add_enum(enum)
    cw.end_if_def()

    cw.write_to_file(c_header_filepath)


def read_fm_instrument(str_data: str):
    str_values = str_data.split(",")
    values = []

    for v in str_values:
        if v[0] == "$":
            v = v[1:]

        values.append(int(v, 16))

    instrument = decompose_fm_instrument(values)
    print(instrument)


def decode_note(note_seq):
    notes_str = [s.strip()[1:] for s in note_seq.split(",")]
    notes = [int(n, 16) for n in notes_str]

    instrument = notes[0]
    msb = notes[1]
    lsb = notes[2]
    block = (msb >> 3) & 0x07
    freq = ((msb & 0x7) << 8) | lsb

    print(f"instrument: ${instrument:02x} ({instrument}) ({instrument:08b}b)")
    print(f"block: ${block:01x} ({block}) ({block:03b}b)")
    print(f"freq: ${freq:04x} ({freq}) ({freq:011b}b)")


def get_16attenuation_table():
    tbl = [0x00] * (128)

    for j in range(16):
        for i in range(16):
            c = i if i > j else j
            u = 15 - i
            v = 15 - j
            if i & 1:
                idx = int((v * 16 + u) / 2)
            else:
                idx = int((u * 16 + v) / 2)

            tbl[idx] = 15 - (int(i * j + c) >> 4)

    print(as_z80_db(tbl, max_per_line=8))


if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument("type", type=str, help="generate type")
    argparser.add_argument("-input", "-i", type=str, help="generate type")
    argparser.add_argument(
        "-output", "-o", type=str, help="output filepath", default="STDOUT"
    )
    argparser.add_argument("-export_type", "-t", type=str, help="output format")
    argparser.add_argument("-channel", type=int, help="channel to use")

    logging.basicConfig(level=logging.INFO)

    args = argparser.parse_args()

    if args.type == "ym2612_notes":
        gen_ym2612_notes()
    if args.type == "sn76489_notes":
        gen_sn76489_notes()
    elif args.type == "instr_reg_seq":
        gen_instr_reg_seq()
    elif args.type == "decode_note":
        decode_note(args.input)
    elif args.type == "module":
        gen_module("song", args.input, args.export_type, args.output)
    elif args.type == "sfx":
        gen_sfx(args.input, args.export_type, args.output, args.channel)
    elif args.type == "nos_decode_table":
        nos_decode_table("./src/asm/decode_jump_table.s", "./src/c/e_commands.h")
    elif args.type == "fm_instrument":
        read_fm_instrument(args.input)
    elif args.type == "16attenuation_table":
        get_16attenuation_table()
    else:
        raise Exception("Unknown generate type")
