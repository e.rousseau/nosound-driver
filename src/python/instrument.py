#
# NOSOUND driver for sega mega drive
# Copyright(c) 2023 Emmanuel Rousseau;
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all; copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

from typing import List, Dict


def decompose_fm_instrument(values: List):
    operators: List[Dict[str, int]] = [{}, {}, {}, {}, {}]

    for i, value in enumerate(values[:4]):
        dt = (value >> 4) & 0x7
        ml = value & 0xF
        operators[i]["dt"] = dt
        operators[i]["ml"] = ml

    for i, value in enumerate(values[4:8]):
        operators[i]["tl"] = value

    for i, value in enumerate(values[8:12]):
        rs = (value >> 6) & 0x3
        a = value & 0x1F
        operators[i]["rs"] = rs
        operators[i]["a"] = a

    for i, value in enumerate(values[12:16]):
        am = (value >> 7) & 0x1
        d1 = value & 0x1F
        operators[i]["am"] = am
        operators[i]["d1"] = d1

    for i, value in enumerate(values[16:20]):
        d2 = value & 0x1F
        operators[i]["d2"] = d2

    for i, value in enumerate(values[20:24]):
        s = (value >> 4) & 0xF
        r = value & 0xF
        operators[i]["s"] = s
        operators[i]["r"] = r

    for i, value in enumerate(values[24:28]):
        ssg_eg = value
        operators[i]["ssg_eg"] = ssg_eg

    feedback = (values[28] >> 3) & 0x7
    algorithm = values[28] & 0x7
    stereo = (values[29] >> 6) & 0x3
    ams = (values[29] >> 3) & 0x7
    fms = values[29] & 0x3

    operators[4]["feedback"] = feedback
    operators[4]["algorithm"] = algorithm
    operators[4]["stereo"] = stereo
    operators[4]["ams"] = ams
    operators[4]["fms"] = fms

    return operators
