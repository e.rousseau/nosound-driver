#
# NOSOUND driver for sega mega drive
# Copyright(c) 2023 Emmanuel Rousseau;
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all; copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
from __future__ import annotations
import argparse
import logging
import os, re
from typing import List, Dict, Any
import vgml.module as vgml
import nosound
import csnake as c
from binary import SegmentedBinaryFile, Segment
import rescomp
import yaml

logger = logging.getLogger(__name__)

NOSOUND_PATH = "nosound"


class ToolError(Exception):
    pass


class OrderKeeper:
    def __init__(self, filepath: str):
        self._order: List[str] = []
        self._filepath = filepath

    def save(self):
        with open(
            self._filepath,
            "w",
        ) as f:
            f.write("\n".join(self._order))
            # for line in self._order:

    def get_order(self) -> List[str]:
        return self._order

    def sync(self, in_values: List[str]):
        inset: set = set(in_values)
        outset: set = set(self._order)
        result: List[str] = []
        for v in self._order:
            if v in inset:
                result.append(v)
                outset.add(v)
        for v in in_values:
            if v not in outset:
                result.append(v)

        self._order = result

    @staticmethod
    def load(filepath: str) -> "OrderKeeper":
        orderKeeper = OrderKeeper(filepath)

        if os.path.exists(filepath):
            with open(filepath, "r") as f:
                orderKeeper._order = [l.strip() for l in f.readlines()]

        return orderKeeper


def scrape_dir(dir: str, ext="yaml") -> List[str]:
    result: List[str] = []
    for (path, _, entries) in os.walk(dir):
        for entry in entries:
            entry = os.path.join(path, entry)
            if not os.path.isfile(entry) or not entry.endswith(f".{ext}"):
                continue
            result.append(entry)
    return result


class CompressedAudio:
    def __init__(self, bin_filepath: str, type_: str, comment: str):
        self.bin_filepath:str = bin_filepath
        self.type_:str = type_
        self.comment:str = comment


def export_sfx(in_filepath: str, out_filepath: str) -> CompressedAudio:
    sheet = vgml.Sheet(in_filepath)
    sheet.load()

    segments = in_filepath.split("_")
    chsegment = list(filter(lambda x: x.startswith("ch"), segments))
    if len(chsegment) < 1:
        raise ToolError(
            f"filename {in_filepath} must contain channel for sfx in the form sfx_ch0_NAME.yaml"
        )

    chan = chsegment[0][2:]
    try:
        use_channel = int(chan)
    except ValueError:
        raise ToolError(
            f"filename {in_filepath} must contain channel for sfx in the form sfx_ch0_NAME.yaml"
        )

    sfx: nosound.Sfx = nosound.Sfx.load(sheet, use_channel)
    logger.debug("writing sfx to %s", out_filepath)
    with open(out_filepath, "wb") as f:
        f.write(sfx.binary)

    return CompressedAudio(os.path.abspath(out_filepath), "sfx", sheet.comment)


def export_mod(in_filepath: str, out_filepath: str) -> CompressedAudio:
    sheet = vgml.Sheet(in_filepath)
    sheet.load()
    module: nosound.Module = nosound.Module.load(sheet)
    logger.debug("writing module to %s", out_filepath)
    with open(out_filepath, "wb") as f:
        f.write(module.binary)

    return CompressedAudio(os.path.abspath(out_filepath), "mod", sheet.comment)

def compress_all(import_path: str, output_path: str) -> List[str]:
    bin_files: List[CompressedAudio] = []
    os.makedirs(os.path.join(output_path, NOSOUND_PATH), exist_ok=True)

    files = scrape_dir(import_path)
    sfx_files = list(filter(lambda x: os.path.basename(x).startswith("sfx_"), files))
    mod_files = list(filter(lambda x: os.path.basename(x).startswith("mod_"), files))

    for sfx_file in sfx_files:
        in_filepath = os.path.abspath(sfx_file)
        out_filepath = (
            os.path.join(output_path, NOSOUND_PATH, os.path.basename(sfx_file))[:-4]
            + "nos"
        )
        compressed_audio = export_sfx(in_filepath, out_filepath)
        bin_files.append(compressed_audio)

    for mod_file in mod_files:
        in_filepath = os.path.abspath(mod_file)
        out_filepath = (
            os.path.join(output_path, NOSOUND_PATH, os.path.basename(mod_file))[:-4]
            + "nos"
        )
        compressed_audio = export_mod(in_filepath, out_filepath)
        bin_files.append(compressed_audio)

    return bin_files


def make_chunks(bin_files: List[str]) -> SegmentedBinaryFile:
    segmentedBin: SegmentedBinaryFile = SegmentedBinaryFile()
    for filepath in bin_files:
        with open(filepath, "rb") as f:
            data = f.read()
            segmentedBin.append(data)
    return segmentedBin


def write_chunks(chunks: List[bytes], output_path: str) -> List[str]:
    bin_files: List[str] = []
    os.makedirs(output_path, exist_ok=True)
    for i, chunk in enumerate(chunks):
        bin_chunk_filepath = os.path.join(output_path, f"nosound_chunk_{i}.bin")
        print(bin_chunk_filepath)
        with open(bin_chunk_filepath, "wb") as f:
            f.write(chunk)
        bin_files.append(os.path.abspath(bin_chunk_filepath))

    return bin_files


def write_sgdk_resource(
    chunkFilepaths: List[str], output_path: str, res_filename: str
) -> List[str]:
    res_filepath: str = os.path.join(output_path, res_filename)
    chunks_symbols: List[str] = []

    r: rescomp.Rescomp = rescomp.Rescomp(res_filepath)
    for c in chunkFilepaths:
        chunk_symbol = os.path.basename(c)[:-4]
        r.binary(chunk_symbol, c, align=0x8000)
        chunks_symbols.append(chunk_symbol)
    r.write()

    return chunks_symbols


def replace_ext(filepath: str, new_ext: str) -> str:
    base, _ = os.path.splitext(filepath)
    return f"{base}{new_ext}"


def write_sgdk_linking_unit(
    chunk_symbols: List[str],
    source_files: List[CompressedAudio],
    data_segments: List[Segment],
    output_src: str,
    res_filename: str,
):
    res_header_basename = replace_ext(res_filename, ".h")  # swaps extension
    header_filepath: str = os.path.join(
        output_src,
        replace_ext(res_filename, "_index.h"),
    )
    c_filepath: str = os.path.join(
        output_src,
        replace_ext(res_filename, "_index.c"),
    )

    file_segments = ((f, s) for f, s in zip(source_files, data_segments))

    sfx_entries: List = []
    mod_entries: List = []

    for chunk_symbol in chunk_symbols:
        while True:
            compressed_audio, segment = next(file_segments, None)
            datatype = (
                "NosPlayableTypeSfx"
                if compressed_audio.type_ == "sfx"
                else "NosPlayableTypeMusic"
            )

            comment = compressed_audio.comment
            priority = 0
            if comment != "":
                try:
                    comment_data = yaml.safe_load(comment)
                    priority = comment_data["priority"]
                except Exception as e:
                    logger.debug(f"ignoring comment {comment} -> not YAML")
                    comment = ""

            entry = {
                "data": c.TextModifier(f"&{chunk_symbol}[{segment.offset}]"),
                "type": c.TextModifier(datatype),
                "priority": priority,
            }

            if datatype == "NosPlayableTypeSfx":
                sfx_entries.append(entry)
            else:
                mod_entries.append(entry)

            if segment.last:
                break

    sfx_var = c.Variable(
        "nos_sfx_entries",
        primitive="nos_playable_s",
        qualifiers="const",
        value=sfx_entries,
    )

    mod_var = c.Variable(
        "nos_mod_entries",
        primitive="nos_playable_s",
        qualifiers="const",
        value=mod_entries,
    )

    src = c.CodeWriter()
    header = c.CodeWriter()

    include_guard = escape_to_snake_case(os.path.basename(header_filepath))
    header.start_if_def(include_guard, invert=True)
    header.add_define(include_guard)

    header.include("genesis.h")
    header.include("nosound.h")
    header.include("rom.h")
    header.include(res_header_basename, comment="this file is generated by RESCOMP")

    if sfx_entries:
        header.add_variable_declaration(sfx_var, extern=True)
    if mod_entries:
        header.add_variable_declaration(mod_var, extern=True)

    header.end_if_def()

    src.include(os.path.basename(header_filepath))

    if sfx_entries:
        src.add_variable_initialization(sfx_var)
    if mod_entries:
        src.add_variable_initialization(mod_var)

    header.write_to_file(header_filepath)
    src.write_to_file(c_filepath)


def escape_to_snake_case(s: str) -> str:
    return re.sub("[^_0-9A-Za-z]", r"_", s).upper()


def consolidate(files: List[CompressedAudio], output_path: str, output_src: str, res_filename: str):
    order_filepath: str = os.path.join(output_path, NOSOUND_PATH, "_orders.txt")

    keepr: OrderKeeper = OrderKeeper.load(order_filepath)
    keepr.sync([f.bin_filepath for f in files])
    keepr.save()

    segmentedBin: SegmentedBinaryFile = make_chunks(keepr.get_order())
    chunks, segments = segmentedBin.get_data()
    chunkFiles = write_chunks(chunks, output_path)
    chunk_symbols: List[str] = write_sgdk_resource(
        chunkFiles, output_path, res_filename
    )

    write_sgdk_linking_unit(
        chunk_symbols,
        files,
        segments,
        output_src,
        res_filename,
    )


def process_all(import_path: str, output_path: str, output_src: str, res_filename: str):
    if not res_filename.endswith(".res"):
        res_filename += ".res"

    bin_files: List[CompressedAudio] = compress_all(
        import_path,
        output_path,
    )
    consolidate(bin_files, output_path, output_src, res_filename)


if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        "--verbose",
        "-v",
        action="store_true",
        help="increase output verbosity",
        default=False,
    )
    argparser.add_argument(
        "--vgml_path",
        "-i",
        type=str,
        required=True,
        help="source directory containing music and SFX FUR files ",
    )
    argparser.add_argument(
        "--output_res", "-r", type=str, required=True, help="output resource"
    )
    argparser.add_argument(
        "--res_filename",
        "-f",
        type=str,
        default="res_nosound.res",
        help="SGDK resource file name",
    )
    argparser.add_argument(
        "--output_src",
        "-s",
        type=str,
        default="nosound.res",
        help="SGDK resource file name",
    )
    args = argparser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    process_all(
        args.vgml_path,
        args.output_res,
        args.output_src,
        args.res_filename,
    )
