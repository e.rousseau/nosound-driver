#
# NOSOUND driver for sega mega drive
# Copyright(c) 2023 Emmanuel Rousseau;
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all; copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

import argparse
import logging
import fur
import os
from typing import List, Any

logger = logging.getLogger(__name__)


def scrape_dir(dir: str, ext="fur") -> List[str]:
    result: List[str] = []
    for (path, _, entries) in os.walk(dir):
        for entry in entries:
            entry = os.path.join(path, entry)
            if not os.path.isfile(entry) or not entry.endswith(f".{ext}"):
                continue
            logger.debug(f"loading {entry}")
            result.append(entry)
    return result


def single_file_import(filename, output_path):
    logger.debug(f"loading {filename} -> {output_path}")
    fur.load_fur(filename, output_path)


def path_import(path, output_path):
    logger.debug(f"scraping path {path}")

    p = os.path
    modules: List[str] = scrape_dir(path)
    logger.debug(f"creating export path {output_path}")
    os.makedirs(output_path, exist_ok=True)

    for module in modules:
        in_basename = p.basename(module)
        out_basename = f"{in_basename[:-4]}.yaml"
        out_filepath = p.abspath(p.join(output_path, out_basename))

        single_file_import(p.abspath(module), out_filepath)


if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument("--file", "-f", type=str, help=".fur file to read")
    argparser.add_argument(
        "--path",
        "-p",
        type=str,
        help="input path containing several FUR files",
    )
    argparser.add_argument(
        "--output", "-o", type=str, help="output path", required=True
    )

    logging.basicConfig(level=logging.INFO)
    args = argparser.parse_args()

    has_file = args.file is not None
    has_path = args.path is not None

    if has_file == has_path:
        logger.error("Either --file or --path must be specified")
        argparser.print_help()
        exit(1)

    try:
        if has_file:
            single_file_import(args.file, args.output)
        elif has_path:
            path_import(args.path, args.output)

    except fur.FurError as e:
        logger.error(e)
