#
# NOSOUND driver for sega mega drive
# Copyright(c) 2023 Emmanuel Rousseau;
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all; copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

import vgml.module as module
import logging
from typing import List, Tuple
from binary import ByteArrayBuilder

logger = logging.getLogger(__name__)

NTSC_FREQ = 60
PAL_FREQ = 50

TempoNTSC = int
TempoPAL = int


class Sfx:
    @staticmethod
    def load(sheet_: module.Sheet, use_channel: int) -> "Sfx":
        sfx = Sfx(sheet_, use_channel)
        sfx._serialize()
        return sfx

    def __init__(self, sheet_: module.Sheet, use_channel: int):
        self._song: module.Song = sheet_.get_stream_and_pattern_offsets()
        self._sheet: module.Sheet = sheet_
        self.binary: bytes = None
        self.channel: int = use_channel
        self._sfx_type: int = 0

    def _serialize(self):
        stream, fm = self._sheet.get_sfx_stream()
        header = {}

        if fm and self.channel >= 6:
            raise ValueError("Channel must be between 0 and 6 for FM SFX")
        if not fm and self.channel < 6:
            raise ValueError("Channel must be between 6 and 10 for PSG SFX")

        header["sfx_type"] = 0 if fm else 1
        header["channel"] = self.channel
        header["frame_per_pattern"] = self._sheet.get_pattern_length()
        header["update_period"] = self._sheet.get_sfx_speed() - 1
        header["instrument_addr"] = 0

        music_block_addr = 6
        instruments_addr = music_block_addr + len(stream)
        header["instrument_addr"] = instruments_addr

        bab = ByteArrayBuilder()
        bab.append(header["sfx_type"], 1)
        bab.append(header["channel"], 1)
        bab.append(header["frame_per_pattern"], 1)
        bab.append(header["update_period"], 1)
        bab.append(header["instrument_addr"], 2)
        bab.data.extend(stream)

        if fm:
            for instr in self._sheet.fm_instruments:
                bab.data.extend(instr.data)
                bab.data.extend([0, 0])
        else:
            sn_instrument_data_addr = 32 * len(self._sheet.sn_instruments)
            (
                sn_instruments,
                sn_instruments_data,
            ) = _get_sn_instruments(self._sheet, sn_instrument_data_addr)
            bab.data.extend(sn_instruments.data)
            bab.data.extend(sn_instruments_data)

        self.binary = bytes(bab.data)


class Module:
    @staticmethod
    def load(sheet_: module.Sheet) -> "Module":
        module = Module(sheet_)
        module._serialize()
        return module

    def __init__(self, sheet_: module.Sheet):
        self._song: module.Song = sheet_.get_stream_and_pattern_offsets()
        self._sheet: module.Sheet = sheet_
        self.binary: bytes = None

    def _serialize(self):
        stream, patterns = self._song.stream, self._song.pattern_offsets
        header = {}
        header["pattern_count"] = self._sheet.get_pattern_count()
        header["frame_per_pattern"] = self._sheet.pattern_length
        header["fm_instrument_addr"] = 0
        header["psg_instrument_addr"] = 0

        tempo_ntsc, tempo_pal = _get_music_tempos(self._sheet)
        header["tempo_ntsc"] = tempo_ntsc
        header["tempo_pal"] = tempo_pal

        music_block_addr = 2 * len(patterns) + 2 * len(header)

        for i, p in enumerate(patterns):
            if p == 0xFFFF:
                continue
            patterns[i] = p + music_block_addr

        fm_instruments_addr = music_block_addr + len(stream)
        header["fm_instrument_addr"] = fm_instruments_addr

        fm_instruments = []
        for i, instr in enumerate(self._sheet.fm_instruments):
            fm_instruments.extend(instr.data)
            fm_instruments.extend([0, 0])

        header["psg_instrument_addr"] = fm_instruments_addr + len(fm_instruments)
        sn_instrument_data_addr = 32 * len(self._sheet.sn_instruments)
        (
            sn_instruments,
            sn_instruments_data,
        ) = _get_sn_instruments(self._sheet, sn_instrument_data_addr)

        bab = ByteArrayBuilder()
        bab.append(header["pattern_count"], 2)
        bab.append(header["frame_per_pattern"], 2)
        bab.append(header["fm_instrument_addr"], 2)
        bab.append(header["psg_instrument_addr"], 2)
        bab.append(header["tempo_ntsc"], 2)
        bab.append(header["tempo_pal"], 2)

        for p in patterns:
            bab.append(p, 2)

        bab.data.extend(stream)

        for i, instr in enumerate(self._sheet.fm_instruments):
            bab.data.extend(instr.data)
            bab.data.extend([0, 0])

        bab.data.extend(sn_instruments.data)
        bab.data.extend(sn_instruments_data)

        self.binary = bytes(bab.data)


def _get_music_tempos(sheet_: module.Sheet) -> Tuple[TempoNTSC, TempoPAL]:
    ntsc = sheet_.get_tempo_fraction(base_freq=NTSC_FREQ)
    pal = sheet_.get_tempo_fraction(base_freq=PAL_FREQ)
    tempo_ntsc: TempoNTSC = ntsc.numerator | (ntsc.denominator << 8)
    tempo_pal: TempoPAL = pal.numerator | (pal.denominator << 8)

    logger.debug(f"Tempo NTSC: {ntsc}")
    logger.debug(f"Tempo PAL: {pal}")

    return tempo_ntsc, tempo_pal


def _get_sn_instruments(sheet_: module.Sheet, base_sn_instruments_addr: int = 0):
    sn_instruments = ByteArrayBuilder()
    sn_instruments_data = []
    MACRO_DATA_SIZE = 32

    for instr in sheet_.sn_instruments:
        macros: List[module.SequenceMacro] = [
            instr.amplitude,
            instr.argpegio,
            instr.pitch,
            instr.duty,
        ]

        tmp_array_builder = ByteArrayBuilder()

        for m in macros:
            tmp_array_builder.append(m.length, 1)
            tmp_array_builder.append(m.period, 1)
            tmp_array_builder.append(m.loop, 1)
            tmp_array_builder.append(m.release, 1)
            if m.length != 0:
                tmp_array_builder.append(base_sn_instruments_addr, 2)
            else:
                tmp_array_builder.append(0, 2)

            if len(tmp_array_builder) > MACRO_DATA_SIZE:
                raise ValueError(
                    f"Too much macro data {len(tmp_array_builder)} > {MACRO_DATA_SIZE}"
                )

            if m == instr.duty:
                fill_size = MACRO_DATA_SIZE - len(tmp_array_builder)
                tmp_array_builder.fill(0xFF, fill_size)
                base_sn_instruments_addr -= fill_size

            sn_instruments_data.extend(m.data)
            base_sn_instruments_addr += len(m.data) - 6

        sn_instruments.concat(tmp_array_builder)

    return sn_instruments, sn_instruments_data
