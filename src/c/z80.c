//
// NOSOUND driver for sega mega drive
// Copyright(c) 2023 Emmanuel Rousseau;
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all; copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include "z80.h"
#include "dev/m_devs.h"

#define MAX_Z80_RAM 0x2000

typedef enum bus_req_cmd {
    // unlocks the 8k RAM area at 0xA00000 - 0xA10000 by resuming the z80.
    BusReqDisable = 0x0000,
    // it locks the 8k RAM area at 0xA00000 - 0xA10000 by halting the z80.
    BusReqEnable = 0x0100,
} bus_req_cmd;

typedef enum bus_req_status {
    // when the 8th bit of the io_bus_req status register is 1, it means the
    // z80 is halted.
    BusReqZ80Halted = 0x0100,
} bus_req_status;

typedef enum reset_cmd {
    // resets the z80.
    ResetZ80 = 0x0000,
    // requests the z80 to start executing.
    StartZ80 = 0x0100,
} reset_cmd;

// see sega official documentation for the meaning of these constants
#define IO_MEM_Z80_RAM      (((volatile u8*)0xA00000))
#define IO_REG_Z80_MEM_MODE (*((volatile u16*)0xA11000))
#define IO_REG_Z80_BUS_REQ  (*((volatile u16*)0xA11100))
#define IO_REG_Z80_RESET    (*((volatile u16*)0xA11200))

inline static bool is_stopped(void);
inline static void block_until_stopped(void);

bool Z80_IsStopped(void) {
    return is_stopped();
}

void Z80_Reset(void) {
    IO_REG_Z80_RESET   = ResetZ80;
    IO_REG_Z80_BUS_REQ = BusReqEnable;
    // block_until_stopped();
    IO_REG_Z80_RESET = StartZ80;
}

void Z80_BusReq(bool enable) {
    if (enable) {
        IO_REG_Z80_BUS_REQ = BusReqEnable;
    } else {
        IO_REG_Z80_BUS_REQ = BusReqDisable;
    }
    vu8 a = 20;
    while (a--) {
    }
}

void Z80_Clear(void) {
    ns_assert(is_stopped(), "Z80_Clear() called while z80 isn't halted");
    volatile u8* ram = IO_MEM_Z80_RAM;

    for (u16 i = MAX_Z80_RAM; i-- > 0; ram++) {
        *ram = 0;
    }
}

void Z80_Write16(u16 addr, u16 w) {
    ns_assert(is_stopped(), "Z80 still running, can't write");
    ns_assert(addr < MAX_Z80_RAM,
              "addr is invalid, max is 0x%04x, got 0x%04x",
              MAX_Z80_RAM - 1,
              addr);
    volatile u8* dst = IO_MEM_Z80_RAM;
    dst[addr]        = w & 0xFF;
    dst[addr + 1]    = (w & 0xFF00) >> 8;
}

u16 Z80_Read16(u16 addr) {
    ns_assert(is_stopped(), "Z80 still running, can't write");
    ns_assert(addr < MAX_Z80_RAM,
              "addr is invalid, max is 0x%04x, got 0x%04x",
              MAX_Z80_RAM - 1,
              addr);
    u16 v            = 0;
    volatile u8* dst = IO_MEM_Z80_RAM;

    v = dst[addr];
    v |= dst[addr + 1] << 8;
    return v;
}

u8 Z80_Read8(u16 addr) {
    ns_assert(is_stopped(), "Z80 still running, can't write");
    ns_assert(addr < MAX_Z80_RAM,
              "addr is invalid, max is 0x%04x, got 0x%04x",
              MAX_Z80_RAM - 1,
              addr);
    u8 v             = 0;
    volatile u8* dst = IO_MEM_Z80_RAM;

    v = dst[addr];
    return v;
}

void Z80_Write8(u16 addr, u8 b) {
    ns_assert(is_stopped(), "Z80 still running, can't write");
    ns_assert(addr < MAX_Z80_RAM,
              "addr is invalid, max is 0x%04x, got 0x%04x",
              MAX_Z80_RAM - 1,
              addr);
    volatile u8* dst = IO_MEM_Z80_RAM;
    dst[addr]        = b;
}

void Z80_WriteMem(u16 addr, u8* data, u16 len) {
    ns_assert(is_stopped(), "Z80 still running, can't write");
    ns_assert(addr < MAX_Z80_RAM,
              "addr is invalid, max is 0x%04x, got 0x%04x",
              MAX_Z80_RAM - 1,
              addr);
    ns_assert(addr + len < MAX_Z80_RAM,
              "addr is invalid, max is 0x%04x, got 0x%04x",
              MAX_Z80_RAM - 1,
              addr + len);
    volatile u8* dst = IO_MEM_Z80_RAM;
    for (u16 i = 0; i < len; i++) {
        dst[addr + i] = data[i];
    }
}

void Z80_Upload(const u8* const data, u16 size) {
    ns_assert(is_stopped(), "Z80 still running, can't upload");
    ns_assert(size <= MAX_Z80_RAM,
              "block too large, max is %d bytes, got %d bytes",
              MAX_Z80_RAM,
              size);

    volatile u8* dst = IO_MEM_Z80_RAM;
    const u8* src    = data;

    for (u16 i = MAX_Z80_RAM; i-- > 0; dst++, src++) {
        *dst = *src;
    }
}

void Z80_Start(void) {
    if (!is_stopped()) {
        ns_warn_print("Z80 already running");
        return;
    }

    IO_REG_Z80_RESET = ResetZ80;
    vu8 a            = 20;
    while (a--) {
    }

    IO_REG_Z80_RESET   = StartZ80;
    IO_REG_Z80_BUS_REQ = BusReqDisable;
}

inline static bool is_stopped(void) {
    vu16* io_bus_req = (u16*)0xA11100;
    // ns_debug_print("%04x", *io_bus_req);
    return (*io_bus_req & 0x0100) == 0;
}

inline static void block_until_stopped(void) {
    while (!is_stopped()) {
    }
    ns_debug_print("z80 halted");
}
