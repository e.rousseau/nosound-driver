//
// NOSOUND driver for sega mega drive
// Copyright(c) 2023 Emmanuel Rousseau;
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all; copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef H_M_DEVS
#define H_M_DEVS

#define offsetof(TYPE, MEMBER) __builtin_offsetof(TYPE, MEMBER)

#define MAX_LOGBUFFER 192
extern char logbuffer[MAX_LOGBUFFER];

#ifdef DEBUG

#define ns_debug_print(msg, ...)    \
    sprintf(logbuffer,              \
            "%s:%d : DEBUG : " msg, \
            __FILE__,               \
            __LINE__,               \
            ##__VA_ARGS__);         \
    KLog(logbuffer)
#define ns_warn_print(msg, ...)                       \
    sprintf(logbuffer,                                \
            "%s:%d: \033[01;33mWARNING\033[m : " msg, \
            __FILE__,                                 \
            __LINE__,                                 \
            ##__VA_ARGS__);                           \
    KLog(logbuffer)
#define ns_error_print(msg, ...)                    \
    sprintf(logbuffer,                              \
            "%s:%d: \033[01;31mERROR\033[m : " msg, \
            __FILE__,                               \
            __LINE__,                               \
            ##__VA_ARGS__);                         \
    KLog(logbuffer)
#define ns_fatal_print(msg, ...)                    \
    sprintf(logbuffer,                              \
            "%s:%d: \033[01;31mFATAL\033[m : " msg, \
            __FILE__,                               \
            __LINE__,                               \
            ##__VA_ARGS__);                         \
    KLog(logbuffer)

#else  // DEBUG

#define ns_debug_print(msg, ...) ;
#define ns_warn_print(msg, ...)  ;
#define ns_error_print(msg, ...) ;
#define ns_fatal_print(msg, ...) ;

#endif  // DEBUG

#ifdef ENABLE_ASSERT
#define ns_assert(x, msg, ...)                                 \
    if (!(x)) {                                                \
        sprintf(logbuffer,                                     \
                "%s:%d: \033[01;31mFATAL\033[m : " #x " " msg, \
                __FILE__,                                      \
                __LINE__,                                      \
                ##__VA_ARGS__);                                \
        KLog(logbuffer);                                       \
        SYS_die(msg);                                          \
    }

#else  // ENABLE_ASSERT
#define ns_assert(x, msg, ...)
#endif  // ENABLE_ASSERT

// @todo : once in production lets transform die into a console reset
//         with a bonus for the player. Let's set a flag in SRAM to apologize.
#define die(msg, ...)                                 \
    sprintf(logbuffer,                                \
            "%s:%d: : \033[01;31mFATAL\033[m : " msg, \
            __FILE__,                                 \
            __LINE__,                                 \
            ##__VA_ARGS__);                           \
    KLog(logbuffer);                                  \
    SYS_die(msg)

// does nothing, its used to identify output parameters
#define OUT

/// SWAP the value between two variables. use only with symbols, don't input
/// expressions.
#define SWAP(a, b)                            \
    {                                         \
        __typeof__(b) tmp = (__typeof__(b))a; \
        a                 = b;                \
        b                 = tmp;              \
    }

#endif /* H_M_DEVS */
