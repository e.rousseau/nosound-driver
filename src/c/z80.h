//
// NOSOUND driver for sega mega drive
// Copyright(c) 2023 Emmanuel Rousseau;
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all; copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef H_Z80
#define H_Z80

#include "m_base.h"

// Z80_Reset the z80 CPU. The routine is blocking and waits for the z80 to
// stop. Then the CPU is reset, it's PC is replaced at 0x0000.
void Z80_Reset(void);
// Z80_IsStopped returns true if the z80 is stopped and BUS_REQ ready.
bool Z80_IsStopped(void);
// Z80_Clear the z80 S.RAM to NOP.
void Z80_Clear(void);
// Z80_Upload a block of data to the z80 S.RAM at the address 0x0000 (z80 mem).
void Z80_Upload(const u8* const data, u16 size);
// Z80_Start execution of the z80 CPU. Doesn't reset the CPU. To reset, call
// Z80_Stop() and then Z80_Start().
void Z80_Start(void);
// Z80_BusReq enables or disables access to the z80 bus.
void Z80_BusReq(bool enable);
// write 16 bit value into the z80 S.RAM at the address addr. z80 must be
// first stopped for any write or read operations.
void Z80_Write16(u16 addr, u16 w);
void Z80_Write8(u16 addr, u8 b);
u16 Z80_Read16(u16 addr);
u8 Z80_Read8(u16 addr);
void Z80_WriteMem(u16 addr, u8* data, u16 len);

/// Z80_IsRunning is inverse if Z80_IsStopped(). Syntaxic sugar.
static inline bool Z80_IsRunning(void) {
    return !Z80_IsStopped();
}

#endif /* H_Z80 */
