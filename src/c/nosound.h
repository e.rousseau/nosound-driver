//
// NOSOUND driver for sega mega drive
// Copyright(c) 2023 Emmanuel Rousseau;
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all; copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef H_NOSOUND
#define H_NOSOUND

struct nos_playable_s;

// the state of the API. This *may& represent the state the driver is
// expected to be. Anyway, this is internal stuff.
typedef enum {
    NosStateInvalid,
    NosStateStopped,
    NosStatePaused,
    NosStatePlaying,
    NosState__COUNT,
} nos_playback_state_id;

typedef enum {
    NosSfxStateFlagNone          = 0x00,
    NosSfxStateFlagPlayRequested = 0x01,
} nos_sfx_state_f;

/// nosound_s is the data structure used by the 68k to track audio.
typedef struct nosound_s {
    struct {
        // binary data for the driver
        const u8* binary;
        // driver size in bytes
        u16 size;
    } driver;

    const struct nos_playable_s* next_sfx;
    const struct nos_playable_s* current_sfx;

    u8* cmd_buffer;
    u8 cmd_buffer_size;
    u8 cmd_buffer_capacity;
    /// tells in which state the no sound system is
    nos_playback_state_id playback_state;
    nos_sfx_state_f sfx_state;
} nosound_s;

// NO_NewDriver creates and initialize NOS driver. The Z80 is stopped and reset
// and the driver is uploaded to the z80 main memory. No playback is started.
// The driver expects a module to be loaded and Play to be called.
nosound_s* NO_NewDriver(const u8* const driver_binary, u16 byte_size);
// NO_FreeDriver from MD's heap. Doesn't desinstall the driver for z80 though.
void NO_FreeDriver(nosound_s** nos);
// NO_UpdateAudio call this once per frame to update the sound system.
void NO_UpdateAudio(nosound_s* nos);
// NO_Play request the driver to rewind song and play it.
void NO_Play(nosound_s* nos);
// NO_Stop request the driver to stop playing song, doesn't rewind. If resume
// is called, the playback will continue where it stopped.
void NO_Stop(nosound_s* nos);
// NO_Resume request the driver to resume playing song where it stopped.
void NO_Resume(nosound_s* nos);
// NO_LoadSong request the driver to load a new song.
void NO_LoadSong(nosound_s* nos, const u8* song_data);
// NO_QueueSFX schedules a SFX to be played at the next NO_UpdateAudio call.
void NO_QueueSfx(nosound_s* nos, const struct nos_playable_s* sfx);
// NO_FmChannelsEnabled request the driver to enable FM channels or disable
// them depending of the value of the provided bits. 0x01 enable channels 0 only
// 0x3F enable them all. etc...
void NO_FmChannelsEnabled(nosound_s* nos, u8 bits);
// NO_PsgChannelsEnabled request the driver to enable PSG channels or disable
// them depending of the value of the provided bits. 0x01 enable channels 7 only
// 0x0F enable channel 7, 8, 9, 10 (formerly PSG channel 0, 1, 2, 3)
void NO_PsgChannelsEnabled(nosound_s* nos, u8 bits);
// NO_TestCommand runs a test routine on the driver. This is meant for
// debugging only for the development of the driver.
void NO_TestCommand(nosound_s* nos);

// NO_SetClockDivider is how the tempo is set. Normally the driver will set
// this by itself from the song header. But a game could want to make the
// song faster or slower for some reasons. The only drawback being, the
// played song shouldn't contain tempo change commands within a sequence
// frame.
//
// Numerator and Denominator of value 0 is illegal and the command will be
// ignored.
//
// The way this work is that numerator and denominator represent a rationnal
// number multiplying the wanted BPM. But it must also be ajusted to reflect
// the number of sequencer frames per beat.
//
// Let's say you want to set the tempo to 130 BPM and you want 8 frames per
// beats. The calculation is:
//
//  * 130 BPM = 130/60 beats per seconds (2.16 BPS)
//  * 8 frames/beat * 130/60 beats/second = 52/3 frames/beat (17.3333...)
//  * hence, numerator is 52 and denominator is 3.
//
// Input these values here and you've got a song playing at 130 BPM where a
// single beats has 8 frames (or rows in Tracker language).
void NO_SetClockDivider(nosound_s* nos, u8 numerator, u8 denominator);

// -----------------------------------------------------------------------------
// low level API, use at your own risk.

// NO_NewDriverEx is the overloaded version of NO_NewDriver. It allows to tune
// the size of the command buffer. Stick with the simpler version unless you
// really know what you're doing.
nosound_s* NO_NewDriverEx(const u8* const driver_binary, u16 byte_size,
                          u8 max_cmd_buffer_size);

// NO_StartDriver starts the Z80 CPU and set the SDK in Stopped State.
void NO_StartDriver(nosound_s* nos);
// NO_QueueCommand push a command to the command buffer.
void NO_QueueCommand(nosound_s* nos, u8* data, u8 size);
// NO_QueueCommit send the command buffer to the z80 Queue.
void NO_QueueCommit(nosound_s* nos);
// NO_StopDriver stop the z80 CPU.
void NO_StopDriver(nosound_s* nos);
// NO_PlaySfxRaw request the driver to play a sound effect. This is the
// raw call do not use it in the context of a game, since, several source in a
// given frame might request a sfx to be played. Use the SFX api (NO_QueueSfx)
// instead (which ends up calling this command once per frame).
void NO_PlaySfxRaw(nosound_s* nos, const u8* sfx_data);

#endif /* H_NOSOUND */
