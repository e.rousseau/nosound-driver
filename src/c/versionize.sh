#!/bin/bash

# this script augment the version of this project by changing VERSION constant
# value in inc/m_devs.h

set -e

function get_version() {
    FILE=$1
    echo $(awk  -F '"' '/#define NOSOUND_BUILD/ {print $2}' "$FILE")
}

function increment() {
    VERSION=$1
    echo "$VERSION + 1" | bc
}

function replace() {
    FILE=$3
    OLD_VERSION=$1
    NEW_VERSION=$2
    sed -i "s/#define NOSOUND_BUILD \"$OLD_VERSION\"/#define NOSOUND_BUILD \"$NEW_VERSION\"/g" "$FILE"
}

CURRENT_VERSION=$(get_version "m_base.h")
NEXT_VERSION=$(increment $CURRENT_VERSION)

echo "==== incrementing version automatically ===="
replace "$CURRENT_VERSION" "$NEXT_VERSION" "m_base.h"
echo "version increment is $CURRENT_VERSION -> $NEXT_VERSION"
