//
// NOSOUND driver for sega mega drive
// Copyright(c) 2023 Emmanuel Rousseau;
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all; copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include "m_types.h"
#include "nosound.h"
#include "m_base.h"
#include "dev/m_devs.h"
#include "rom.h"
#include "z80.h"
#include "e_commands.h"

#define DEFAULT_CMD_BUFFER_SIZE 32

#define NO_CMD_Q_BASE_ADDR    0x0100
#define NO_CMD_Q_CAPACITY     NO_CMD_Q_BASE_ADDR
#define NO_CMD_Q_ACCEPT_WRITE (NO_CMD_Q_BASE_ADDR + 1)
#define NO_CMD_Q_SIZE         (NO_CMD_Q_BASE_ADDR + 2)
#define NO_CMD_Q_DATA         (NO_CMD_Q_BASE_ADDR + 3)

// see nosound.s ASM file for the documentation of these variables
#define NO_VAR_SYS_MISSED_FRAMES  0x0010
#define NO_VAR_DRIVER_STATE_FLAGS 0x0011

#define ADDR_TO_Z80_PAGE(addr)   (((addr) >> 15) & 0xFF)
#define ADDR_TO_Z80_OFFSET(addr) (addr & 0x7FFF)

typedef enum {
    NosDriverStateFlagNone       = 0x00,
    NosDriverStateFlagSfxPlaying = 0x01,
} nos_driver_state_f;

static void log_command(const u8* const command, u8 size);

typedef struct page_offset_t {
    u16 page, offset;
} page_offset_t;

static page_offset_t addr_to_page_offset(u32 addr);

nosound_s* NO_NewDriver(const u8* const driver_binary, u16 byte_size) {
    return NO_NewDriverEx(driver_binary, byte_size, DEFAULT_CMD_BUFFER_SIZE);
}

nosound_s* NO_NewDriverEx(const u8* const driver_binary, u16 byte_size,
                          u8 cmd_buffer_capacity) {
    ns_assert(driver_binary != NULL, "driver_binary is NULL");
    ns_assert(byte_size != NULL, "driver byte_size is 0");

    nosound_s* nos = (nosound_s*)malloc(sizeof(nosound_s));

    *nos = (nosound_s){
        .driver.binary       = driver_binary,
        .driver.size         = byte_size,
        .playback_state      = NosStateInvalid,
        .next_sfx            = NULL,
        .current_sfx         = NULL,
        .sfx_state           = NosSfxStateFlagNone,
        .cmd_buffer          = (u8*)malloc(sizeof(u8) * cmd_buffer_capacity),
        .cmd_buffer_size     = 0,
        .cmd_buffer_capacity = cmd_buffer_capacity,
    };

    Z80_Reset();
    Z80_Clear();
    Z80_Upload(driver_binary, byte_size);
    ns_debug_print("Uploaded NoSound driver to Z80 (%d bytes)", byte_size);

    NO_StartDriver(nos);
    ns_debug_print("NoSound bootstrapped");
    ns_debug_print("NoSound 68k command buffer capacity : %d bytes",
                   cmd_buffer_capacity);

    return nos;
}

void NO_FreeDriver(nosound_s** nos) {
    ns_assert(nos != NULL, "nos is NULL");
    ns_assert(*nos != NULL, "*nos is NULL");

    nosound_s* nosp = *nos;
    if (nosp->playback_state == NosStatePlaying) {
        NO_StopDriver(nosp);
    }
    free(nosp->cmd_buffer);
    nosp->cmd_buffer = NULL;
    free(nosp);
    *nos = NULL;
}

void NO_StopDriver(nosound_s* nos) {
    ns_assert(nos != NULL, "nos is NULL");

    if (nos->playback_state != NosStatePlaying) {
        return;
    }

    if (Z80_IsRunning()) {
        Z80_Reset();
    }
    nos->playback_state = NosStateStopped;

    // TODO we should wait one frames or two to ensure the driver read the
    // command before shutting it down, in order to gracefully shut down the
    // YM2612 and the TMS.
}

void NO_StartDriver(nosound_s* nos) {
    Z80_Start();
    nos->playback_state = NosStateStopped;
}

void NO_Play(nosound_s* nos) {
    ns_assert(nos != NULL, "nos is NULL");
    u8 cmd = NosCmdPlay;
    NO_QueueCommand(nos, &cmd, 1);
    NO_QueueCommit(nos);
}

void NO_Stop(nosound_s* nos) {
    ns_assert(nos != NULL, "nos is NULL");
    u8 cmd = NosCmdStop;
    NO_QueueCommand(nos, &cmd, 1);
    NO_QueueCommit(nos);
}

void NO_Resume(nosound_s* nos) {
    ns_assert(nos != NULL, "nos is NULL");
    u8 cmd = NosCmdResume;
    NO_QueueCommand(nos, &cmd, 1);
    NO_QueueCommit(nos);
}

void NO_LoadSong(nosound_s* nos, const u8* song_data) {
    ns_assert(nos != NULL, "nos is NULL");
    ns_assert(song_data != NULL, "song_data is NULL");
    const u32 addr = (u32)song_data;
    if (addr > 0xFF0000) {
        ns_warn_print("NoSound cannot load song from the 68000's RAM (%p)",
                      song_data);
    }
    ns_debug_print("NoSound loading song from 68000's mem (%p)", song_data);

    page_offset_t po = addr_to_page_offset(addr);

    u8 cmd[5] = {
        (u8)NosCmdSongLoad,
        (u8)(po.page & 0xFF),
        (u8)((po.page >> 8) & 0xFF),
        (u8)(po.offset & 0xFF),
        (u8)((po.offset >> 8) & 0xFF),
    };

    NO_QueueCommand(nos, cmd, 5);
    NO_QueueCommit(nos);
}

void NO_SetClockDivider(nosound_s* nos, u8 numerator, u8 denominator) {
    ns_assert(nos != NULL, "nos is NULL");
    u8 cmd[3] = {
        (u8)NosCmdSetClockDivider,
        (u8)numerator,
        (u8)denominator,
    };
    NO_QueueCommand(nos, cmd, 3);
    NO_QueueCommit(nos);
}

void NO_FmChannelsEnabled(nosound_s* nos, u8 bits) {
    ns_assert(nos != NULL, "nos is NULL");
    u8 cmd[2] = {
        (u8)NosCmdFmChannelsEnabled,
        (u8)bits,
    };
    NO_QueueCommand(nos, cmd, 2);
    NO_QueueCommit(nos);
}

void NO_PsgChannelsEnabled(nosound_s* nos, u8 bits) {
    ns_assert(nos != NULL, "nos is NULL");
    u8 cmd[2] = {
        (u8)NosCmdPsgChannelsEnabled,
        (u8)bits,
    };
    NO_QueueCommand(nos, cmd, 2);
    NO_QueueCommit(nos);
}

void NO_TestCommand(nosound_s* nos) {
    ns_assert(nos != NULL, "nos is NULL");
    u8 cmd = NosCmdTest;
    NO_QueueCommand(nos, &cmd, 1);
    NO_QueueCommit(nos);
}

void NO_PlaySfxRaw(nosound_s* nos, const u8* sfx_data) {
    ns_assert(nos != NULL, "nos is NULL");
    ns_assert(sfx_data != NULL, "sfx_data is NULL");

    const u32 addr = (u32)sfx_data;
    if (addr > 0xFF0000) {
        ns_warn_print("NoSound cannot load SFX from the 68000's RAM (%p)",
                      sfx_data);
    }
    ns_debug_print("NoSound loading SFX from 68000's mem (%p)", sfx_data);

    page_offset_t po = addr_to_page_offset(addr);

    u8 cmd[5] = {
        (u8)NosCmdPlaySfx,
        (u8)(po.page & 0xFF),
        (u8)((po.page >> 8) & 0xFF),
        (u8)(po.offset & 0xFF),
        (u8)((po.offset >> 8) & 0xFF),
    };
    NO_QueueCommand(nos, cmd, 5);
    NO_QueueCommit(nos);
}

void NO_ClearSfxQueue(nosound_s* nos) {
    nos->next_sfx = NULL;
    nos->sfx_state &= ~NosSfxStateFlagPlayRequested;
}

void NO_QueueSfx(nosound_s* nos, const nos_playable_s* p) {
    ns_assert(nos != NULL, "nos is NULL");
    ns_assert(p != NULL, "p is NULL");
    ns_assert(p->type == NosPlayableTypeSfx, "p is not NosPlayableTypeSfx");

    // prevent driver corruption
    if (p->type != NosPlayableTypeSfx) {
        return;
    }

    // decide if the sfx may be queued
    if (nos->current_sfx != NULL) {
        if (p->priority < nos->current_sfx->priority) {
            // not allowed to interrupt currently playing SFX
            return;
        }
    }

    if (nos->next_sfx != NULL) {
        if (p->priority < nos->next_sfx->priority) {
            // not allowed to replace currently queued SFX
            return;
        }
    }

    nos->sfx_state |= NosSfxStateFlagPlayRequested;
    nos->next_sfx = p;
}

void NO_QueueCommand(nosound_s* nos, u8* data, u8 size) {
    ns_assert(nos->cmd_buffer_size <= nos->cmd_buffer_capacity,
              "buffer overflow, %d >= %d",
              nos->cmd_buffer_size,
              nos->cmd_buffer_capacity);

    if (nos->cmd_buffer_size + size >= nos->cmd_buffer_capacity) {
        ns_warn_print("NO_QueueCommand capacity exceeded %d < %d",
                      nos->cmd_buffer_size + size,
                      nos->cmd_buffer_capacity);
        return;
    }

    u8* dest = nos->cmd_buffer;
    u8* src  = data;

    log_command(data, size);

    nos->cmd_buffer_size += size;
    while (size-- > 0) {
        *(dest++) = *(src++);
    }

    ns_debug_print("NO_QueueCommand size : %d", nos->cmd_buffer_size);
}

static inline void update_sfx(nosound_s* nos, nos_driver_state_f driver_flags) {
    // if sfx has ended release current sfx
    if (nos->current_sfx != NULL &&
        !(driver_flags & NosDriverStateFlagSfxPlaying)) {
        ns_debug_print("sfx has ended");
        nos->current_sfx = NULL;
    }

    if (!(nos->sfx_state & NosSfxStateFlagPlayRequested)) {
        return;
    }

    if (nos->next_sfx == NULL) {
        return;
    }

    ns_debug_print("sfx has started");

    NO_PlaySfxRaw(nos, nos->next_sfx->data);
    nos->current_sfx = nos->next_sfx;
    NO_ClearSfxQueue(nos);
}

void NO_UpdateAudio(nosound_s* nos) {
    Z80_BusReq(TRUE);
    u8 missed_frames                = Z80_Read8(NO_VAR_SYS_MISSED_FRAMES);
    nos_driver_state_f driver_flags = Z80_Read8(NO_VAR_DRIVER_STATE_FLAGS);
    Z80_BusReq(FALSE);

    if (missed_frames) {
        ns_debug_print("NO_UpdateAudio missed_frames : %d", missed_frames);
    }

    update_sfx(nos, driver_flags);
}

void NO_QueueCommit(nosound_s* nos) {
    __label__ ret;
    ns_assert(nos != NULL, "nos is NULL");

    ns_assert(nos->playback_state != NosStateInvalid,
              "driver not started or stopped");

    ns_assert(nos->cmd_buffer_size <= nos->cmd_buffer_capacity,
              "buffer overflow, %d >= %d",
              nos->cmd_buffer_size,
              nos->cmd_buffer_capacity);

    if (nos->cmd_buffer_size == 0) {
        return;
    }

    Z80_BusReq(TRUE);
    u8 accept_write = Z80_Read8(NO_CMD_Q_ACCEPT_WRITE);
    if (!accept_write) {
        ns_warn_print("z80 refused write");
        goto ret;
    }

    u8 z80capacity = Z80_Read8(NO_CMD_Q_CAPACITY);
    u8 z80size     = Z80_Read8(NO_CMD_Q_SIZE);
    if (z80capacity - z80size < nos->cmd_buffer_size) {
        ns_warn_print("z80 queue capacity exceeded, delaying commit");
        goto ret;
    }

    Z80_WriteMem(NO_CMD_Q_DATA, nos->cmd_buffer, nos->cmd_buffer_size);
    Z80_Write8(NO_CMD_Q_SIZE, nos->cmd_buffer_size);

    nos->cmd_buffer_size = 0;
ret:
    Z80_BusReq(FALSE);
}

static page_offset_t addr_to_page_offset(u32 addr) {
    return (page_offset_t){
        .page   = ADDR_TO_Z80_PAGE(addr),
        .offset = ADDR_TO_Z80_OFFSET(addr) + 0x8000,
    };
}

static void log_command(const u8* const command, u8 size) {
#ifdef DEBUG
    ns_debug_print("NoSound command");
    switch (size) {
    case 1:
        ns_debug_print(" $%02x", *command);
        break;
    case 2:
        ns_debug_print(" $%02x $%02x", *command, *(command + 1));
        break;
    case 3:
        ns_debug_print(" $%02x $%02x $%02x",
                       *command,
                       *(command + 1),
                       *(command + 2));
        break;
    case 4:
        ns_debug_print(" $%02x $%02x $%02x $%02x",
                       *command,
                       *(command + 1),
                       *(command + 2),
                       *(command + 3));
        break;
    case 5:
        ns_debug_print(" $%02x $%02x $%02x $%02x $%02x",
                       *command,
                       *(command + 1),
                       *(command + 2),
                       *(command + 3),
                       *(command + 4));
        break;
    default:
        break;
    }
#endif
}