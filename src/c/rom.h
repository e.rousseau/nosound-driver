//
// NOSOUND driver for sega mega drive
// Copyright(c) 2023 Emmanuel Rousseau;
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all; copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#ifndef H_ROM
#define H_ROM

#include "m_base.h"

// nos_playable_type describe the type of data that can be played.
typedef enum {
    NosPlayableTypeMusic = 0,
    NosPlayableTypeSfx   = 1,
    NosPlayableType__COUNT,
} nos_playable_id;

// describes a ROM entry for playable data.
typedef struct nos_playable_s {
    // pointer to the binary data
    const u8* const data;
    // type indicate what data contains
    const nos_playable_id type;
    // priority is used by sound effects to decide if a SFX may stop the
    // currently playing one or not.
    const u8 priority;
} nos_playable_s;

#endif /* H_ROM */
