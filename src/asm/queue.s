; ------------------------------------------------------------------------------
; NOSOUND driver for sega mega drive
;
; Copyright (c) 2023 Emmanuel Rousseau
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
; ------------------------------------------------------------------------------
; communication io between z80 and the 68k.

; size in message, must be power of two, using binary masking
Q_MAX_SIZE      equ     32
; the max size is put in RAM to allow the 68k to read the limit
Q_maxSize       db      Q_MAX_SIZE
; when false, the m68 cannot write (this is a mutex)
Q_acceptWrite   db      $00
; m68 write position
Q_size          db      $00
; inbound queue is where 68k writes and z80 reads inbound commands.
Q_inBound       ds      Q_MAX_SIZE, $00

; ------------------------------------------------------------------------------
; execute the commands on the Queue
    .local
Q_Execute::
    ; first lets check if there is something to consume - - - - - - - - - - - -

    ; Looks like a race condition, but it's okay. If the 68k is writing to the
    ; Q after the size is read:
    ;
    ; * While the Q size is 0, the Q will be processed at the next iteration,
    ;   the driver is faster than the 68k at writting stuff as it waits for
    ;   messages.
    ; * While the Q size isn't 0, it won't be a problem because, the size is
    ;   read again after Q_acceptWrite is set down.
    ;
    ; The idea here is to prevent the 68k to be blocked by the mutex when the
    ; Q is empty. Oterwise, it could introduce delays in command processing and
    ; in the worst case, could cascade to impossible writes if the 68k is doomed
    ; with very unlucky timings.

    ld      A, (Q_size)         ; if Q_size == 0
    or      A                   ;
    ret     Z                   ; early exit

    ; block m68k from writing - - - - - - - - - - - - - - - - - - - - - - - - -
    xor     A                       ;ld  A, False
    ld      (Q_acceptWrite), A

    ; execute all the commands in the Queue - - - - - - - - - - - - - - - - - -
    ld      HL, Q_inBound           ; load the queue head into the NOS_decoder
    ld      (NOS_decodePtr), HL
    ld      BC, (Q_size)            ; find the limit address that the
    ld      A, C                    ;   NOS_decoder should read
    add     A, L
    ld      (workMem0), A           ; store that limit in a temp variable

foreach_q_commands:
    call    NOS_Decode              ; call the decoder
    ld      HL, (NOS_decodePtr)     ; read NOS_decoderPtr position in order
    ld      A, (workMem0)           ; to compare with the _readLimit that
    cp      L                       ; we found earlier. 8bit comparison here
    jp      NC, reached_limit       ; is enough, because we are in 0 page.
    jp      foreach_q_commands

reached_limit:
    xor     A                       ; set the queue size to 0
    ld      (Q_size), A
    ; re-enable writing for the m68k - - - - - - - - - - - - - - - - - - - - - -
q_execute_exit:
    cpl                             ; ld  A, True
    ld      (Q_acceptWrite), A
    ret

    .endlocal

; ------------------------------------------------------------------------------
; clear the message queue
Q_Clear::
    ; block m68k from writing - - - - - - - - - - - - - - - - - - - - - - - - -
    xor     A                       ;ld   A, False
    ld      (Q_acceptWrite), A

    ; reset queue size - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      (Q_size), A

    ; re-enable writing for the m68k - - - - - - - - - - - - - - - - - - - - - -
    cpl                            ;ld    A, True
    ld      (Q_acceptWrite), A

    ret

