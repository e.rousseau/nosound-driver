; ------------------------------------------------------------------------------
; NOSOUND driver for sega mega drive
;
; Copyright (c) 2023 Emmanuel Rousseau
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
; ------------------------------------------------------------------------------
; Command subroutines.

; These variables holds the addresses for the instrument database to use by
; the set instrument commands. They must be set by the sequencer and the SFX
; sequencer.
CMD_psgInstrumentBankAddr       dw  $0000       ; psg instrument database
CMD_fmInstrumentBankAddr        dw  $0000       ; fm instrument database

; ------------------------------------------------------------------------------
; [Command] SEQ_LoadSong - setup the bank switching variables
CMD_LoadSong:
    ; skip command byte - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, (NOS_decodePtr)
    inc     HL

    ; stop playback completely - - - - - - - - - - - - - - - - - - - - - - - - -
    di
    ld      A, (SEQ_state)
    res     SEQ_STATE_PLAYING, A
    ld      (SEQ_state), A

    ; read m68k page where the song is located - - - - - - - - - - - - - - - - -
    ld      C, (HL)                     ; read 16bit value from command to BC
    inc     HL
    ld      B, (HL)
    inc     HL
    ld      (SEQ_songMemPage), BC       ; write in the variable

    ; read the address where the song is - - - - - - - - - - - - - - - - - - - -
    ld      C, (HL)                     ; read 16 bit value from command to BC
    inc     HL
    ld      B, (HL)
    inc     HL
    ld      (SEQ_songAddr), BC          ; write song address (required by
                                        ;   SEQ_InitSong
    ld      (NOS_decodePtr), HL         ; write the decoder's position

    call    SEQ_BankSwitchToSong        ; bank switch to song
    jp      SEQ_InitSong                ; initialize the sequencer with the song

; ------------------------------------------------------------------------------
; [Command] request starting blackback
CMD_Play:
    ; increment the NOS_decodePtr
    call    NOS_SkipByte

    ; validate a song is loaded - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (SEQ_state)
    bit     SEQ_STATE_SONG_LOADED, A
    ret     Z

    set     SEQ_STATE_PLAYING, A
    ld      (SEQ_state), A

    jp      SEQ_Rewind

; ------------------------------------------------------------------------------
; [Command] request the playback to stop by simply disable interruptions
CMD_Stop:
    call    NOS_SkipByte

    ; validate a song is loaded - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (SEQ_state)
    bit     SEQ_STATE_SONG_LOADED, A
    ret     Z

    res     SEQ_STATE_PLAYING, A
    ld      (SEQ_state), A

    jp      SEQ_Silence

; ------------------------------------------------------------------------------
; [Command] set channels enabled or disabled by bit values
CMD_FmChannelsEnabled::
    call    NOS_SkipByte

    ; loads the status bits - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      C, (HL)                 ; statuses in C
    inc     HL
    ld      (NOS_decodePtr), HL

    ld      HL, SEQ_channelStatus   ; channel statuses addr to write
    ld      B, SEQ_MAX_FM_COLS      ; channel count (6 FM channels)

    jp      SEQ_MuteFMChannelsByBits  ; execute

; ------------------------------------------------------------------------------
; [Command] set PSG channels enabled or disabled by bit values
CMD_PsgChannelEnabled::
    call    NOS_SkipByte

    ; loads the status bits - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      C, (HL)                 ; statuses in C
    inc     HL
    ld      (NOS_decodePtr), HL

    ld      HL, SEQ_channelStatus+6 ; channel statuses addr to write
    ld      B, SEQ_MAX_PSG_COLS     ; channel count (6 FM channels)

    jp      SEQ_MutePsgChannelsByBits  ; execute

; ------------------------------------------------------------------------------
; [Command] Continue play where it stopped
CMD_Resume:
    call    NOS_SkipByte

    ; validate a song is loaded - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (SEQ_state)
    bit     SEQ_STATE_SONG_LOADED, A
    ret     Z

    set     SEQ_STATE_PLAYING, A
    ld      (SEQ_state), A

    ret

; ------------------------------------------------------------------------------
; [Command] set the clock divider values
CMD_SetClockDivider:
    .local
    ; read command byte - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, (NOS_decodePtr)
    inc     HL

    ; reads the numerator and the denominator - - - - - - - - - - - - - - - - -
    ld      A, (HL)                 ; numerator
    inc     HL
    or      A
    jp      Z, ignore_command
    ld      B, A                    ; B is numerator

    ld      A, (HL)                 ; denominator
    or      A
    jp      Z, ignore_command

    ld      (SEQ_clockDiv), A
    ld      A, B
    ld      (SEQ_clockInc), A

ignore_command:
    inc     HL
    ld      (NOS_decodePtr), HL

    ret
    .endlocal

; ------------------------------------------------------------------------------
; [Command] This command set the sequencer to wait for the next clock.
CMD_FrameClock:
    ld      HL, (NOS_decodePtr)
    inc     HL
    ld      (NOS_decodePtr), HL
    jp      SEQ_FrameClock

; ------------------------------------------------------------------------------
; [Command] This command set the sequencer current channel to wait for the next
; clock and additionnal frames if the parameter isn't zero
    .local
CMD_FrameClockRepeated::
    call    NOS_SkipByte
    ; load the wait increment parameter - - - - - - - - - - - - - - - - - - - -
    ld      A, (HL)                         ; E contains the wait increment
    inc     HL
    ld      (NOS_decodePtr), HL

    ; persist instrument (only from sequencer) - - - - - - - - - - - - - - - - -
    ld      HL, SEQ_state
    bit     SEQ_STATE_UPDATING, (HL)
    jp      NZ, sequencer_wait

    ; get the SFX sequencer wait value - - - - - - - - - - - - - - - - - - - - -
sfx_wait:
    ld      HL, SFX_frameWaitDelay
    jp      increment

    ; get the current channel wait value - - - - - - - - - - - - - - - - - - - -
sequencer_wait:
    ld      BC, (SEQ_currentChannelIdx)
    ld      B, 0
    ld      HL, SEQ_channelFrameWaitDelay
    add     HL, BC                          ; the position to read current wait

    ; increments the wait - - - - - - - - - - - - - - - - - - - - - - - - - - -
increment:
    add     A, (HL)
    ld      (HL), A

    jp      SEQ_FrameClock
    .endlocal

; ------------------------------------------------------------------------------
; [Command] This command inform the sequencer a pattern ended
CMD_PatternClock:
    jp $

; ------------------------------------------------------------------------------
; [Command] assign fm instrument
; up to 256 instruments are supported
    .local
CMD_FmSetInstrument::
    ; read the NOS command, the channel is in the 4bit LSB of the command - - -
    ld      HL, (NOS_decodePtr)
    inc     HL                       ; increment decoder address

    ld      BC, DATA_offsetTo32      ; instruments are 32 bytes, to find
    ld      C, (HL)                  ;
    sla     C                        ; BC now has the correct address in the LUT

    inc     HL                       ; lets get rid of decoder ptr
    ld      (NOS_decodePtr), HL      ; write the decoder address

    ld      HL, BC
    ld      C, (HL)
    inc     HL
    ld      B, (HL)                  ; we now have the correct offset in BC

    ld      HL, (CMD_fmInstrumentBankAddr)
    add     HL, BC                   ; we now have the instrument address
    ld      (INS_instrumentRomAddr), HL

    ; persist instrument (only from sequencer) - - - - - - - - - - - - - - - - -
    ld      DE, HL
    ld      A, (SEQ_state)
    bit     SEQ_STATE_UPDATING, A
    call    NZ, SEQ_PersistInstrumentAddr   ; DE = instrument addr

    ; check if the channel is currently muted - - - - - - - - - - - - - - - - -
    ld      A, (SEQ_currentChannelIdx)
    call    SEQ_IsChannelPlayable
    or      A
    ret     Z

    ld      A, (SEQ_currentChannelIdx)
    jp      INS_LoadFmInstrument            ; A: Channel; INS_instrument_addr
    .endlocal

; ------------------------------------------------------------------------------
; [Command] Psg Key on
CMD_PsgSetInstrument:
    ; get affected channel - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, (NOS_decodePtr)             ; loads the channel value
    inc     HL

    ld      A, (SEQ_currentChannelIdx)      ; load current channel in A
    sub     SEQ_MAX_FM_COLS
    and     $03
    ld      (INS_curChannel), A             ; set cur PSG channel in input

    ; get the address of the instrument in ROM - - - - - - - - - - - - - - - - -
    ld      A, (HL)
    inc     HL
    ld      (NOS_decodePtr), HL             ; save back decode pointer

    rrca                                    ; mutiplies by 32 (instruments have
    rrca                                    ; 32 bytes in size). Rotate >>3 is
    rrca                                    ; faster than bitshift <<5
    ld      B, 0
    ld      C, A
    ld      HL, (CMD_psgInstrumentBankAddr)
    add     HL, BC
    ld      (INS_instrumentRomAddr), HL     ; loads the instrument address
                                            ;   parameter
    ; persist instrument (only from sequencer) - - - - - - - - - - - - - - - - -
    ld      DE, HL
    ld      A, (SEQ_state)
    bit     SEQ_STATE_UPDATING, A
    call    NZ, SEQ_PersistInstrumentAddr   ; DE = instrument addr

    ; check if the channel is currently muted - - - - - - - - - - - - - - - - -
    ld      A, (SEQ_currentChannelIdx)
    call    SEQ_IsChannelPlayable
    or      A
    ret     Z

    jp      INS_LoadPsgInstrument

; ------------------------------------------------------------------------------
; [Command] key on, whatever the channel number is. Delegate the execution to
; a more specific subroutine.
CMD_KeyOn::
    ; a key on always end a frame
    call    SEQ_FrameClock

    ld      A, (SEQ_currentChannelIdx)
    cp      SEQ_MAX_FM_COLS
    jp      C, CMD_FmKeyOn
    jp      CMD_PsgKeyOn

; ------------------------------------------------------------------------------
; [Command]
; input : (NOS_decodePtr) the complete command including the command code
;         A: current global channel
    .local
CMD_FmKeyOn::
    and     $0F
    ld      C, A                    ; C = channel

    ; read the NOS command - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, (NOS_decodePtr)
    ld      A, (HL)                 ; read the note id in D
    sub     $80                     ; remove the command offset $80
    ld      D, A                    ; move the note in D
    inc     HL
    ld      (NOS_decodePtr), HL     ; save back the decode position

    ; check if the channel is currently muted - - - - - - - - - - - - - - - - -
    ld      A, C
    call    SEQ_IsChannelPlayable
    or      A
    ret     Z

    ; write requested note in the instrument engine - - - - - - - - - - - - - -
    ld      A, C                    ;
    ld      HL, INS_fmInputNote     ; load the right input note address
    add     L                       ; by using the channel as address offset
    ld      L, A
    ld      (HL), D                 ; write the note id as input note

    ; write the key on signal in the instrument engine  - - - - - - - - - - - -
    ld      A, C                    ; reload channel in A
    ld      HL, INS_fmSignals       ; compute fm signal command
    add     L
    ld      L, A
    set     INS_SIGNAL_KEY_ON, (HL) ; raise the key-on bit

    ret
    .endlocal

; ------------------------------------------------------------------------------
; [Command] SEQ_FmKeyOff
; input : NOS_decodePtr
    .local
CMD_FmKeyOff::
    ; read the NOS command, the channel is in the 4bit LSB of the command - - -
    ld      HL, (NOS_decodePtr)
    inc     HL
    ld      (NOS_decodePtr), HL         ; save back the decode position

    ld      A, (SEQ_currentChannelIdx)  ; load current channel
    and     $0F                         ; keep the target channel bits
    ld      C, A                        ; load channel in C

    ; check if the channel is currently muted - - - - - - - - - - - - - - - - -
    call    SEQ_IsChannelPlayable
    or      A
    ret     Z

    ; write the key off signal in the instrument engine  - - - - - - - - - - - -
    ld      A, C
    ld      HL, INS_fmSignals
    add     L
    ld      L, A
    set     INS_SIGNAL_KEY_OFF, (HL) ; raise the key-on bit

    ret
    .endlocal

; ------------------------------------------------------------------------------
; [Command] Psg Key on
; input
;     A: global channel idx (7-10)
CMD_PsgKeyOn:
    sub     SEQ_MAX_FM_COLS
    ld      C, A                    ; C = channel

    ; get the values to update from the command - - - - - - - - - - - - - - - -
    ld      HL, (NOS_decodePtr)
    ld      A, (HL)                 ; loads the note id in A
    inc     HL
    ld      (NOS_decodePtr), HL

    sub     $80                     ; remove the command offset 80
    ld      IXL, A                  ; save note in IXL

    ; check if the channel is currently muted - - - - - - - - - - - - - - - - -
    ld      A, SEQ_MAX_FM_COLS
    add     A, C
    call    SEQ_IsChannelPlayable
    or      A
    ret     Z

    ; cutoff on bad note value - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, IXL
    cp      DATA_MAX_PSG_NOTES      ; if note is off limit, cut the channel off
    jp      NC, SEQ_PsgNoteOff      ; cut the note and return

    ; set the note in the instrument engine - - - - - - - - - - - - - - - - - -
    ld      A, C
    ld      HL, INS_psgInputNote
    add     L
    ld      L, A
    ld      A, IXL
    ld      (HL), A

    ; clears period delta - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, INS_psgPeriodDeltas
    ld      A, C
    add     L
    ld      L, A
    xor     A
    ld      (HL), A

    ; write the signal key-on for the instrument - - - - - - - - - - - - - - - -
    ld      HL, INS_psgSignals
    ld      A, C
    add     L
    ld      L, A
    set     INS_SIGNAL_KEY_ON, (HL)

    ret

; ------------------------------------------------------------------------------
; [Command] Psg Key on
CMD_PsgKeyOff:
    ld      HL, (NOS_decodePtr)
    inc     HL
    ld      (NOS_decodePtr), HL

    ld      A, (SEQ_currentChannelIdx)  ; load current channel in A
    sub     SEQ_MAX_FM_COLS
    and     $03
    ld      C, A

    ; check if the channel is currently muted - - - - - - - - - - - - - - - - -
    ld      A, SEQ_MAX_FM_COLS
    add     A, C
    call    SEQ_IsChannelPlayable
    or      A
    ret     Z

    ld      A, C                    ; restore channel value

    jp      SEQ_PsgNoteOff

; ------------------------------------------------------------------------------
; [Command]
CMD_PsgSetAttenuationCurrentChannel::
    call    NOS_SkipByte

    ; extract attenuation parameter  - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (HL)
    ld      (INS_inputAttenuation), A
    ld      E, A

    inc     HL
    ld      (NOS_decodePtr), HL

    ; load current PSG channel - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (SEQ_currentChannelIdx)
    ld      C, A                ; current channel in C

    ; check if the channel is currently muted - - - - - - - - - - - - - - - - -
    call    SEQ_IsChannelPlayable
    or      A
    ret     Z

    ld      A, C                    ; restore channel value
    sub     SEQ_MAX_FM_COLS         ; convert to psg channel
    ld      (INS_curChannel), A     ; write in variable

    ; persist attenuation (only from sequencer) - - - - - - - - - - - - - - - -
    ld      A, (SEQ_state)
    bit     SEQ_STATE_UPDATING, A
    call    NZ, SEQ_PersistAttenuation      ; E = attenuation

    jp      INS_SetPsgAttenuation           ; update instrument

; ------------------------------------------------------------------------------
; [Command] set the attenuation level of the current FM channel.
    .local
CMD_FmSetAttenuationCurrentChannel::
    call    NOS_SkipByte

    ; extract attenuation parameter  - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (HL)
    ld      E, A                        ; set attenuation in E
    ld      (INS_inputAttenuation), A

    inc     HL
    ld      (NOS_decodePtr), HL

    ; load current FM channel - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (SEQ_currentChannelIdx)
    ld      (INS_curChannel), A

    ; check if the channel is currently muted - - - - - - - - - - - - - - - - -
    call    SEQ_IsChannelPlayable
    or      A
    ret     Z

    ; persist attenuation (only from sequencer) - - - - - - - - - - - - - - - -
    ld      A, (SEQ_state)
    bit     SEQ_STATE_UPDATING, A
    call    NZ, SEQ_PersistAttenuation      ; E = attenuation,

    jp      INS_SetFMAttenuation
    .endlocal

; ------------------------------------------------------------------------------
; [Command] request playback of a sound effect
CMD_PlaySfx::
    ld      HL, (NOS_decodePtr)
    inc     HL

    ; load page bank - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      C, (HL)
    inc     HL
    ld      B, (HL)
    inc     HL
    ld      (SFX_bank), BC

    ; load SFX offset address in bank (the $8000 is already provided) - - - - -
    ld      C, (HL)
    inc     HL
    ld      B, (HL)
    inc     HL
    ld      (SFX_addr), BC
    ld      (NOS_decodePtr), HL

    ; request playback of the SFX to SFX module  - - - - - - - - - - - - - - - -
    jp      SFX_Play

; ------------------------------------------------------------------------------
; [Command] test command
CMD_Test::
    call    NOS_SkipByte
    xor     A
    jp      SEQ_ReserveChannel

