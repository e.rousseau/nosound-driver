; ------------------------------------------------------------------------------
; NOSOUND driver for sega mega drive
;
; Copyright (c) 2023 Emmanuel Rousseau
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
; ------------------------------------------------------------------------------
; some math routines recovered various places

; ------------------------------------------------------------------------------
; multiplies two 8bits values and return the result as 16bits in A:E.
; adapted from <https://www.cpcwiki.eu/index.php/Programming:Integer_Multiplication>
; input: A,B
; returns: A:E
; destroys: AF, BC, DE, HL
    .local
IMUL_8::
    cp      B
    jr      NC, l1

    ld      E, A
    ld      A, B
    ld      B, E

l1:
    ld      C, A
    sub     B
    rra
    ld      D, A
    ld      A, C
    add     B
    rra
    ld      L, A
    ld      H, DATA_sqrlo / 256
    ld      A, (HL)
    ld      E, L
    ld      L, D
    jr      NC, l2

    sub     (HL)   ; odd
    ld      L, E
    ld      E, A
    inc     H      ; loads high(sqrhi)
    ld      A, (HL)
    ld      L, D
    sbc     A, (HL)
    ld      D, A

    ld      A, E
    add     A, B
    ld      E, A
    ld      A, D
    adc     A, 0
    ret

l2:
    sub     (HL)   ; even
    ld      L, E
    ld      E, A
    inc     H
    ld      A, (HL)
    ld      L, D
    sbc     A, (HL)
    ret
    .endlocal

; ------------------------------------------------------------------------------
; scales 4 bit attenuation values together
; performs 15-((15-D) * (15-E)/16) where D and E are 4 bit numbers
; in 86 cycles
    .local
M_ScaleAttenuation_4::
    ld      H, DATA_4bitAttenuationScale/256
    ld      A, D
    or      E
    ld      B, A            ; store parity result in B

    bit     0, B
    jp      NZ, odd

odd:
    ld      A, E
    rlca
    rlca
    rlca
    srl     D
    or      D
    ld      L, A
    ld      A, (HL)
    ret

even:
    ld      A, D
    rlca
    rlca
    rlca
    srl     E
    or      E
    ld      L, A
    ld      A, (HL)
    ret

    .endlocal
