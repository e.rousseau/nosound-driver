; ------------------------------------------------------------------------------
; NOSOUND driver for sega mega drive
;
; Copyright (c) 2023 Emmanuel Rousseau
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
; ------------------------------------------------------------------------------
; aligned lookup tables

; ------------------------------------------------------------------------------
; This table is used by IMUL_8 subroutine
    .align  256
DATA_sqrlo:     ; low(x*x)   should be at the page border
    db $00,$01,$04,$09,$10,$19,$24,$31,$40,$51,$64,$79,$90,$a9,$c4,$e1
    db $00,$21,$44,$69,$90,$b9,$e4,$11,$40,$71,$a4,$d9,$10,$49,$84,$c1
    db $00,$41,$84,$c9,$10,$59,$a4,$f1,$40,$91,$e4,$39,$90,$e9,$44,$a1
    db $00,$61,$c4,$29,$90,$f9,$64,$d1,$40,$b1,$24,$99,$10,$89,$04,$81
    db $00,$81,$04,$89,$10,$99,$24,$b1,$40,$d1,$64,$f9,$90,$29,$c4,$61
    db $00,$a1,$44,$e9,$90,$39,$e4,$91,$40,$f1,$a4,$59,$10,$c9,$84,$41
    db $00,$c1,$84,$49,$10,$d9,$a4,$71,$40,$11,$e4,$b9,$90,$69,$44,$21
    db $00,$e1,$c4,$a9,$90,$79,$64,$51,$40,$31,$24,$19,$10,$09,$04,$01
    db $00,$01,$04,$09,$10,$19,$24,$31,$40,$51,$64,$79,$90,$a9,$c4,$e1
    db $00,$21,$44,$69,$90,$b9,$e4,$11,$40,$71,$a4,$d9,$10,$49,$84,$c1
    db $00,$41,$84,$c9,$10,$59,$a4,$f1,$40,$91,$e4,$39,$90,$e9,$44,$a1
    db $00,$61,$c4,$29,$90,$f9,$64,$d1,$40,$b1,$24,$99,$10,$89,$04,$81
    db $00,$81,$04,$89,$10,$99,$24,$b1,$40,$d1,$64,$f9,$90,$29,$c4,$61
    db $00,$a1,$44,$e9,$90,$39,$e4,$91,$40,$f1,$a4,$59,$10,$c9,$84,$41
    db $00,$c1,$84,$49,$10,$d9,$a4,$71,$40,$11,$e4,$b9,$90,$69,$44,$21
    db $00,$e1,$c4,$a9,$90,$79,$64,$51,$40,$31,$24,$19,$10,$09,$04,$01
DATA_sqrhi:     ; high(x*x)
    db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
    db $01,$01,$01,$01,$01,$01,$01,$02,$02,$02,$02,$02,$03,$03,$03,$03
    db $04,$04,$04,$04,$05,$05,$05,$05,$06,$06,$06,$07,$07,$07,$08,$08
    db $09,$09,$09,$0a,$0a,$0a,$0b,$0b,$0c,$0c,$0d,$0d,$0e,$0e,$0f,$0f
    db $10,$10,$11,$11,$12,$12,$13,$13,$14,$14,$15,$15,$16,$17,$17,$18
    db $19,$19,$1a,$1a,$1b,$1c,$1c,$1d,$1e,$1e,$1f,$20,$21,$21,$22,$23
    db $24,$24,$25,$26,$27,$27,$28,$29,$2a,$2b,$2b,$2c,$2d,$2e,$2f,$30
    db $31,$31,$32,$33,$34,$35,$36,$37,$38,$39,$3a,$3b,$3c,$3d,$3e,$3f
    db $40,$41,$42,$43,$44,$45,$46,$47,$48,$49,$4a,$4b,$4c,$4d,$4e,$4f
    db $51,$52,$53,$54,$55,$56,$57,$59,$5a,$5b,$5c,$5d,$5f,$60,$61,$62
    db $64,$65,$66,$67,$69,$6a,$6b,$6c,$6e,$6f,$70,$72,$73,$74,$76,$77
    db $79,$7a,$7b,$7d,$7e,$7f,$81,$82,$84,$85,$87,$88,$8a,$8b,$8d,$8e
    db $90,$91,$93,$94,$96,$97,$99,$9a,$9c,$9d,$9f,$a0,$a2,$a4,$a5,$a7
    db $a9,$aa,$ac,$ad,$af,$b1,$b2,$b4,$b6,$b7,$b9,$bb,$bd,$be,$c0,$c2
    db $c4,$c5,$c7,$c9,$cb,$cc,$ce,$d0,$d2,$d4,$d5,$d7,$d9,$db,$dd,$df
    db $e1,$e2,$e4,$e6,$e8,$ea,$ec,$ee,$f0,$f2,$f4,$f6,$f8,$fa,$fc,$fe

; these are the frequencies for every notes. The driver accept note commands
; so they need to be converted to their frequency counterpart.
    align 256
DATA_ym_notes:
    db $02,$69,$02,$8d,$02,$b4,$02,$dd,$03,$09,$03,$37,$03,$68,$03,$9c
    db $03,$d3,$04,$0d,$04,$4b,$04,$8c,$0a,$69,$0a,$8d,$0a,$b4,$0a,$dd
    db $0b,$09,$0b,$37,$0b,$68,$0b,$9c,$0b,$d3,$0c,$0d,$0c,$4b,$0c,$8c
    db $12,$69,$12,$8d,$12,$b4,$12,$dd,$13,$09,$13,$37,$13,$68,$13,$9c
    db $13,$d3,$14,$0d,$14,$4b,$14,$8c,$1a,$69,$1a,$8d,$1a,$b4,$1a,$dd
    db $1b,$09,$1b,$37,$1b,$68,$1b,$9c,$1b,$d3,$1c,$0d,$1c,$4b,$1c,$8c
    db $22,$69,$22,$8d,$22,$b4,$22,$dd,$23,$09,$23,$37,$23,$68,$23,$9c
    db $23,$d3,$24,$0d,$24,$4b,$24,$8c,$2a,$69,$2a,$8d,$2a,$b4,$2a,$dd
    db $2b,$09,$2b,$37,$2b,$68,$2b,$9c,$2b,$d3,$2c,$0d,$2c,$4b,$2c,$8c
    db $32,$69,$32,$8d,$32,$b4,$32,$dd,$33,$09,$33,$37,$33,$68,$33,$9c
    db $33,$d3,$34,$0d,$34,$4b,$34,$8c,$3a,$69,$3a,$8d,$3a,$b4,$3a,$dd
    db $3b,$09,$3b,$37,$3b,$68,$3b,$9c,$3b,$d3,$3c,$0d,$3c,$4b,$3c,$8c

    align 256
DATA_psg_notes:
    db $f7,$03,$be,$03,$88,$03,$56,$03,$26,$03,$f8,$02,$ce,$02,$a5,$02,$7f,$02
    db $5b,$02,$3a,$02,$1a,$02,$fb,$01,$df,$01,$c4,$01,$ab,$01,$93,$01,$7c,$01
    db $67,$01,$52,$01,$3f,$01,$2d,$01,$1d,$01,$0d,$01,$fd,$00,$ef,$00,$e2,$00
    db $d5,$00,$c9,$00,$be,$00,$b3,$00,$a9,$00,$9f,$00,$96,$00,$8e,$00,$86,$00
    db $7e,$00,$77,$00,$71,$00,$6a,$00,$64,$00,$5f,$00,$59,$00,$54,$00,$4f,$00
    db $4b,$00,$47,$00,$43,$00,$3f,$00,$3b,$00,$38,$00,$35,$00,$32,$00,$2f,$00
    db $2c,$00,$2a,$00,$27,$00,$25,$00,$23,$00,$21,$00,$1f,$00,$1d,$00,$1c,$00
    db $1a,$00,$19,$00,$17,$00,$16,$00,$15,$00,$13,$00,$12,$00,$11,$00,$10,$00
    db $0f,$00,$0e,$00,$0e,$00,$0d,$00,$0c,$00,$0b,$00,$0b,$00,$0a,$00,$09,$00
    db $09,$00,$08,$00,$08,$00,$07,$00,$07,$00,$07,$00,$06,$00,$06,$00,$05,$00
    db $05,$00,$05,$00,$04,$00,$04,$00,$04,$00,$04,$00

DATA_MAX_PSG_NOTES      equ     ($ - DATA_psg_notes)/2

; this array convert indexes to multiples of 32. This table is used to fetch
; fm instrument data.
    align 256
DATA_offsetTo32:
counter = $0000
    .rept 32
    dw  counter
counter = counter + $20
    .endm

; this table is used to compute attenuation scaling of 4 bit values against
; 4 bit values
; A * B/16 where A and B are 4 bits numbers.
; the table packs result in 4bit representation to save space, upper triangle
; are odd mul result, lower triangle are odd mul result
    align 256
DATA_4bitAttenuationScale:
    db $00,$02,$04,$06,$08,$0a,$0c,$0e
    db $01,$03,$05,$07,$08,$0a,$0c,$0e
    db $02,$04,$06,$07,$09,$0b,$0c,$0e
    db $03,$05,$06,$08,$09,$0b,$0c,$0e
    db $04,$06,$07,$09,$0a,$0b,$0d,$0e
    db $05,$07,$08,$09,$0a,$0c,$0d,$0e
    db $06,$07,$09,$0a,$0b,$0c,$0d,$0e
    db $07,$08,$09,$0a,$0b,$0c,$0d,$0e
    db $08,$09,$0a,$0b,$0c,$0d,$0e,$0f
    db $09,$0a,$0b,$0c,$0c,$0d,$0e,$0f
    db $0a,$0b,$0b,$0c,$0d,$0e,$0e,$0f
    db $0b,$0b,$0c,$0d,$0d,$0e,$0e,$0f
    db $0c,$0c,$0d,$0d,$0e,$0e,$0f,$0f
    db $0d,$0d,$0d,$0e,$0e,$0f,$0f,$0f
    db $0e,$0e,$0e,$0e,$0f,$0f,$0f,$0f
    db $0f,$0f,$0f,$0f,$0f,$0f,$0f,$0f

; ------------------------------------------------------------------------------
; These are the addresses for the byte instrument sequences
; 30h based instruments
DATA_chan0_addr_seq:
    db $30,$34,$38,$3c,$40,$44,$48,$4c,$50,$54,$58,$5c,$60,$64,$68,$6c
    db $70,$74,$78,$7c,$80,$84,$88,$8c,$90,$94,$98,$9c,$b0,$b4,$00,$00
; 31h based instruments
DATA_chan1_addr_seq:
    db $31,$35,$39,$3d,$41,$45,$49,$4d,$51,$55,$59,$5d,$61,$65,$69,$6d
    db $71,$75,$79,$7d,$81,$85,$89,$8d,$91,$95,$99,$9d,$b1,$b5,$00,$00
; 32h based instruments
DATA_chan2_addr_seq:
    db $32,$36,$3a,$3e,$42,$46,$4a,$4e,$52,$56,$5a,$5e,$62,$66,$6a,$6e
    db $72,$76,$7a,$7e,$82,$86,$8a,$8e,$92,$96,$9a,$9e,$b2,$b6,$00,$00

; maps the sequential FM channels to YM hardware encoding
DATA_fmChannelsToHWChannels  db  $00, $01, $02, $04, $05, $06

