; ------------------------------------------------------------------------------
; NOSOUND driver for sega mega drive
;
; Copyright (c) 2023 Emmanuel Rousseau
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
; ------------------------------------------------------------------------------
; YM init and constants

; ------------------------------------------------------------------------------
; ym2612 registers

; YM2612 register port addresses
; part-1 address register
YM1_ADR             equ $4000
; part-1 data register
YM1_VAL             equ $4001

; part-2 address register
YM2_ADR             equ $4002
; part-2 data register
YM2_VAL             equ $4003

; status of the ym2612 bus to read from
YM_ST               equ $4000

YM_LFO              equ $22
YM_TIMER_A_H        equ $24
YM_TIMER_A_L        equ $25
YM_TIMER_B          equ $26
YM_CH3MD_TIMERS     equ $27
YM_KEY              equ $28
YM_DAC              equ $2A
YM_DAC_ENABLED      equ $2B

YM_TL_CH1_OP1       equ $40     ; TL for ch1 op1

YM_FREQ_LSB_2       equ $A0
YM_FREQ_MSB_1       equ $A4     ; A4 must be written first

YM_CH3MOD_NORMAL    equ $0
YM_CH3MOD_SPECIAL   equ $1

; ------------------------------------------------------------------------------
; some usefull macros

; ------------------------------------------------------------------------------
; writes to part1 marco
; ym1(reg, val) [!HL, A]
ymWritePart1: macro &reg,&val
    ld      HL, &reg + (&val << 8)
    call    YM_WritePart1
    endm

ymSetLfo: macro &enable,&freq
    ymWritePart1     YM_LFO, (&enable<<3) | (&freq & $7)
    endm

ymSetTimerA: macro &duration
    ymWritePart1     YM_TIMER_A_H, (&msb >> 2) & $FF
    ymWritePart1     YM_TIMER_A_L, &msg & $3
    endm

ymSetTimerB: macro &duration
    ymWritePart1     YM_TIMER_B, &duration & $FF
    endm

ymSetCh3MdTimers: macro &ch3mod, &rst_b, &rst_a, &en_b, &en_a, &ld_b, &ld_a
    ymWritePart1     YM_CH3MD_TIMERS, (&ch3mod<<6) | (&rst_b<<5) | (&rst_a<<4) | (&en_b<<3) | (&en_a<<2) | (&ld_b<<1) | (&ld_a)
    endm

ymDacEnable: macro &enable
    ymWritePart1     YM_DAC_ENABLED, &enable << 7
    endm

; ------------------------------------------------------------------------------
; initialize the YM, to use when the driver boots to kill sounds
YM_Init:
    ymSetLfo          True, $0
    ymSetCh3MdTimers  YM_CH3MOD_NORMAL, False, False, False, False, False, False
    ymWritePart1      YM_KEY, $00
    ymWritePart1      YM_KEY, $01
    ymWritePart1      YM_KEY, $02
    ymWritePart1      YM_KEY, $04
    ymWritePart1      YM_KEY, $05
    ymWritePart1      YM_KEY, $06
    ymDacEnable       False

    ret

; ------------------------------------------------------------------------------
; write on the YM registers, Part-I
; input:
;    L:addr,
;    H:value
; destroys A
YM_WritePart1:
    ld      A, L
    ld      (YM1_ADR), A

    .local
loop:
    ; ensure the YM is ready (maybe the z80 is slow enough)
    ld      A, (YM_ST)
    bit     7, A
    jp      nz, loop
    .endlocal

    ld      A, H
    ld      (YM1_VAL), A
    ret
