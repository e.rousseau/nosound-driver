; ------------------------------------------------------------------------------
; NOSOUND driver for sega mega drive
;
; Copyright (c) 2023 Emmanuel Rousseau
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
; ------------------------------------------------------------------------------
; main module

    include "macros.s"

; ------------------------------------------------------------------------------
; z80 entry point, the usual ceremony
    org     $0000
    jp      main

; ------------------------------------------------------------------------------
; shared memory with m68k (do not move this around)
    org     $0010

; when a frame is missed, this variable gets incremented
NOS_sysMissedClock          db $00

; state variable indicated various internal driver states
NOS_STATEBIT_SFX_PLAYING    equ 0
NOS_state                   db  $00

    include "interrupt.s"       ; interruption subroutine

; ------------------------------------------------------------------------------
; communication io between z80 and the 68k
    org     $0100

    include "queue.s"

; multipurpose variable
workMem0:           dw $0000

; ------------------------------------------------------------------------------
; main entry point
    .local
main::
    ; CPU setup  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    di                          ; when CPU is reset, iterrupts are disabled by
                                ;   default, but in case of strange behaviour,
                                ;   lets disable it before doing these risky
                                ;   operations.
    ld      SP, $1FFF           ; set the stack at the end of S.RAM

    im      1                   ; set interruption in mode 1. On the MD,
                                ;   a VBlank interrupt will load ISR at addr
                                ;   $0038. See interrupt.s
    ; reset driver state variables - - - - - - - - - - - - - - - - - - - - - - -
    xor     A
    ld      (NOS_state), A
    ld      (NOS_sysMissedClock), A

    ; reset bank switch to 0 - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, 0
    ld      (SGA_selectedBank), HL
    call    SGA_BankSwitch

    ; reset synthesizer module - - - - - - - - - - - - - - - - - - - - - - - - -
    call    INS_Reset
    ; reset and clear the communication Queue  - - - - - - - - - - - - - - - - -
    call    Q_Clear
    ; reset the YM2612, lets set it in a previsible state  - - - - - - - - - - -
    call    YM_Init
    ; reset the music sequencer  - - - - - - - - - - - - - - - - - - - - - - - -
    call    SEQ_Reset
    ; reset and init the SFX sequencer - - - - - - - - - - - - - - - - - - - - -
    call    SFX_Init
    ei
    ; driver core loop - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
ad_nauseam_infinitum:
    ; process the Queue as often as possible - - - - - - - - - - - - - - - - - -
    call    SEQ_BankSwitchToSong
    call    Q_Execute

    ; Skip Sequencer and SFX update if no sys clock  - - - - - - - - - - - - - -
    ld      A, (NOS_sysClock)           ; load sys clock
    or      A                           ; if the sys clock isn't true
    jp      Z, update_instruments       ; skip the update of the sequencers
    xor     A                           ; else lets clear the sysclock
    ld      (NOS_sysClock), A

    call    SEQ_Update                  ; and call update on the sequencer
    call    SFX_Update                  ; and call update on the sfx sequencer

update_instruments:
    call    INS_UpdateFmChannels        ; TODO evaluate if they shouldn't be
    call    INS_UpdatePsgChannels       ;  updated on the sys clock too.

    jp      ad_nauseam_infinitum
    .endlocal

; ------------------------------------------------------------------------------
    include "sega.s"            ; sega hardware related subroutines

    include "ym2612.s"          ; YM2612 definitions and initialization
    include "instruments.s"     ; synthesizer engine
    include "sequencer.s"       ; music sequencer
    include "sfx.s"             ; SFX sequencer
    include "decoder.s"         ; command decoder and its jump table
    include "commands.s"        ; command subroutines

    include "mem.s"             ; mem subroutines
    include "math.s"            ; math subroutines
    include "tables.s"          ; lookup tables

; ------------------------------------------------------------------------------

    end
