; ------------------------------------------------------------------------------
; NOSOUND driver for sega mega drive
;
; Copyright (c) 2023 Emmanuel Rousseau
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
; ------------------------------------------------------------------------------
; the sequencer module has the responsibility to play music partitions.

; number of fm channels
SEQ_MAX_FM_COLS     equ 6
; number of SN76489 channels
SEQ_MAX_PSG_COLS    equ 4
; total of channels combined, YM2612 + SN76489
SEQ_MAX_COLS        equ SEQ_MAX_FM_COLS + SEQ_MAX_PSG_COLS

; ------------------------------------------------------------------------------
; Resetable block, byte before is the default reset value, there values must
; be defaulted when the driver is reset or oddities might happen.

; each time it's true, the system emited a clock signal
NOS_sysClock        db  False

; State management - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; test bits
SEQ_STATE_SONG_LOADED   equ     0
SEQ_STATE_PLAYING       equ     1
SEQ_STATE_UPDATING      equ     2       ; is true when the sequencer is
                                        ;  performing an update. This flags is
                                        ;  used by the commands to detect from
                                        ;  which source the command is coming
                                        ;  from.

; various bits used to control the sequencer
SEQ_state           db  $00

; tempo management - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; SEQ_songTimer is used to count the tempo. Every sysClock, SEQ_songTimer
; reduced by SEQ_clockDiv. When SEQ_songTimer overflows, the next song frame
; executed (a frame in the drive is the equivalent of a row in a tracker) and
; SEQ_songTimer is augmented by SEQ_clockInc.
SEQ_songTimer       db  $00

; tempo control
SEQ_clockDiv        db  $01
SEQ_clockInc        db  $08

; when false, read commands until $19 (Frame Clock cmd)
SEQ_waitNextFrame   db  $00

; ------------------------------------------------------------------------------

; this table contains all wait frame values. These are counters, when the
; counter isn't zero, it means that more than one frame must be skipped
SEQ_channelFrameWaitDelay:
    .rept SEQ_MAX_COLS
    db $00
    .endm

; playback pointers - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; playback pointer addr of current pattern, this value is incremented by
; SEQ_MAX_COLS * 2 each time a pattern ends
SEQ_pp              dw  $0000

;
SEQ_CH_STATUS_MUTED     equ 0
SEQ_CH_STATUS_RESERVED  equ 1

SEQ_CH_ANY_MUTING_MASK  equ (1<<SEQ_CH_STATUS_MUTED) | (1<<SEQ_CH_STATUS_RESERVED)

    align 16
SEQ_channelStatus:
    .rept SEQ_MAX_COLS
    db $00
    .endm

; playback pointer of the each channels
    align 32
SEQ_channelPPs:
    .rept SEQ_MAX_COLS
    dw $0000
    .endm

; current channel instrument addresses
    align 32
SEQ_channelInstruments:
    .rept SEQ_MAX_COLS
    db  $0000
    .endm

; current channel attenuation
    align 16
SEQ_channelAttenuations:
    .rept SEQ_MAX_COLS
    db  $00
    .endm

; table containing the addresses of SEQ_channelPPs
_SEQ_channelPPAddresses:
addr = SEQ_channelPPs
    .rept SEQ_MAX_COLS
    dw addr
addr = addr + 2
    .endm


SEQ_channelPPAddresses: equ _SEQ_channelPPAddresses + (SEQ_MAX_COLS*2) - 1

; address in ROM of the current loaded song
SEQ_songAddr            dw  $0000
; which bank switch page the song is
SEQ_songMemPage         dw  $0000
; currently update channel
SEQ_currentChannelIdx   db  $00
; number of patterns in the current song
SEQ_patternCount        dw  $0000
; currently played pattern
SEQ_currentPatternId    db  $00
; number of frames in a pattern (for the whole song)
SEQ_framesPerPattern    db  $00
; currently played frame
SEQ_currentFrameId      db  $00
; base address for the pattern offsets
SEQ_patternsBaseAddr    dw  $0000
; address of the FM instrument database
SEQ_fmInstrumentsAddr   dw  $0000
; address of the psg instrument database
SEQ_psgInstrumentsAddr  dw  $0000

; ------------------------------------------------------------------------------
; reset sequencer values
SEQ_Reset:
    .local
    di
    xor     A
    ld      (NOS_sysClock), A
    ld      (SEQ_state), A
    ld      (SEQ_songTimer), A
    ld      (SEQ_waitNextFrame), A
    ld      A, $08
    ld      (SEQ_clockInc), A
    ld      A, $01
    ld      (SEQ_clockDiv), A

    call    INS_Reset

    .endlocal
    ; fall-through

; ------------------------------------------------------------------------------
; reset the wait frame delays counters
SEQ_ResetFrameWaitDelays:
    xor     A
    ld      (SEQ_waitNextFrame), A

    ld      B, SEQ_MAX_COLS
    ld      HL, SEQ_channelFrameWaitDelay
    jp      M_MemSet

; ------------------------------------------------------------------------------
;
    .local
SEQ_SetAllChannelUnmuted::
    ld      HL, SEQ_channelStatus
    ld      B, SEQ_MAX_COLS
foreach_channels:
    res     SEQ_CH_STATUS_MUTED, (HL)
    inc     HL
    djnz    foreach_channels
    ret
    .endlocal

; ------------------------------------------------------------------------------
; input: A channel
; ouput: A true or false
; destroys: HL
    .local
SEQ_IsChannelPlayable::
    ld      HL, SEQ_channelStatus   ; read the channel status
    add     L
    ld      L, A
    ld      A, (HL)
    and     SEQ_CH_ANY_MUTING_MASK  ; if no muting or reservation, go
    jp      Z, yes                  ;  ahead.

    ld      A, (SEQ_state)          ; check if this command happened when
    and     1<<SEQ_STATE_UPDATING   ;  within the sequencer. If it's the case
    jp      NZ, no                  ;  channel isn't playable.
yes:
    ld      A, True
    ret
no:
    xor     A
    ret

    .endlocal

; ------------------------------------------------------------------------------
; SEQ_InitSong - prepare the sequencer for playing the song at address pointed
; by SEQ_songAddr
; input: SEQ_songAddr
    .local
SEQ_InitSong::
    ; TODO include a magic checksum to prevent weird things if something goes
    ; wrong
    call    SEQ_Reset

    ; setup song addresses - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, (SEQ_songAddr)

    ; load pattern count - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      C, (HL)
    inc     HL
    ld      B, (HL)
    inc     HL
    ld      (SEQ_patternCount), BC

    ; load frame count per pattern - - - - - - - - - - - - - - - - - - - - - - -
    ld      C, (HL)
    inc     HL
    ld      B, (HL)
    inc     HL
    ld      (SEQ_framesPerPattern), BC

    ; load instrument list offset - - - - - - - - - - - - - - - - - - - - - - -
    ld      C, (HL)
    inc     HL
    ld      B, (HL)
    inc     HL
    ld      IX, (SEQ_songAddr)
    add     IX, BC
    ld      (SEQ_fmInstrumentsAddr), IX

    ld      C, (HL)
    inc     HL
    ld      B, (HL)
    inc     HL
    ld      IX, (SEQ_songAddr)
    add     IX, BC
    ld      (SEQ_psgInstrumentsAddr), IX

    ; load tempo values  - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (SGA_system)
    cp      SGA_NTSC
    jp      NZ, pal
ntsc:
    ld      A, (HL)
    ld      (SEQ_clockInc), A
    inc     HL
    ld      A, (HL)
    ld      (SEQ_clockDiv), A
    ld      BC, 3
    add     HL, BC
    jp      load_patterns
pal:
    ld      BC, 2
    add     HL, BC
    ld      (SEQ_clockInc), A
    inc     HL
    ld      A, (HL)
    ld      (SEQ_clockDiv), A
    inc     HL
    ; load the pattern base address - - - - - - - - - - - - - - - - - - - - - -
load_patterns:
    ld      (SEQ_patternsBaseAddr), HL
    ld      (SEQ_pp), HL

    ld      A, $00
    ld      (SEQ_currentPatternId), A

    ; set song loaded state - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (SEQ_state)
    set     SEQ_STATE_SONG_LOADED, A
    ld      (SEQ_state), A

    call    SEQ_SetAllChannelUnmuted

    call    SEQ_LoadPattern
    ei
    .endlocal
    ; ** fallthrough => SEQ_BankSwitchToSong

; ------------------------------------------------------------------------------
; SEQ_BankSwichToSong - switch to the m68k bank where the song was told to be
SEQ_BankSwitchToSong:
    ld      HL, (SEQ_songMemPage)
    ;call    SGA_IsUsingBank         ; if True, Z = True
    ;ret     NZ
    ld      (SGA_selectedBank), HL
    jp      SGA_BankSwitch

; ------------------------------------------------------------------------------
; SEQ_Rewind
; input: SEQ_songAddr
SEQ_Rewind:
    ld      A, (SEQ_state)
    bit     SEQ_STATE_SONG_LOADED, A
    ret     Z

    ld      HL, (SEQ_patternsBaseAddr)
    ld      (SEQ_pp), HL
    xor     A
    ld      (SEQ_currentPatternId), A
    jp      SEQ_LoadPattern

; ------------------------------------------------------------------------------
; input: (SEQ_pp)
SEQ_LoadPattern:
    ; set frame id to 0
    xor     A
    ld      (SEQ_currentFrameId), A

    ; assign all PPs of every channels to their proper addresses - - - - - - - -
    ld      B, SEQ_MAX_COLS
load_pattern_loop:
    ld      HL, (SEQ_pp)            ; SEQ_pp contains the current pattern
                                    ;   address
    ; load pattern column in DE - - - - - - - - - - - - - - - - - - - - - - - -
    ld      E, (HL)                 ; Load the column offset in DE
    inc     HL                      ;   the offset is calculated from the
    ld      D, (HL)                 ;   start of the song.
    inc     HL                      ;
    ld      (SEQ_pp), HL            ; Write back the pattern read position until
                                    ;   next loop

    ld      HL, (SEQ_songAddr)      ; load the song base addr
    add     HL, DE                  ; add the offset to the song base addr
    ld      DE, HL                  ; DE contains the address of the column
    ; write the channel read address position - - - - - - - - - - - - - - - - -
    ld      HL, SEQ_channelPPs
    ld      A, SEQ_MAX_COLS         ; write offset is found relative to the
    sub     B                       ;   iteration value, SEQ_MAX_COLS - B
    sla     A                       ; double, 16bit addresses
    add     A, L
    ld      L, A
    ld      (HL), E                 ; *(SEQ_channelPPs +
                                    ;  (SEQ_MAX_COLS - B)*2) = DE
    inc     HL
    ld      (HL), D

    djnz    load_pattern_loop       ; until all pattern loaded
    jp      SEQ_ResetFrameWaitDelays

; ------------------------------------------------------------------------------
; request a sequencer increment
    .local
SEQ_Update::
    ; check if a song is loaded and in play state - - - - - - - - - - - - - - -
    ld      HL, SEQ_state
    ld      A, (HL)
    and     2^SEQ_STATE_PLAYING | 2^SEQ_STATE_SONG_LOADED
    cp      2^SEQ_STATE_PLAYING | 2^SEQ_STATE_SONG_LOADED
    ret     NZ

    ; set the updating flag - - - - - - - -  - - - - - - - - - - - - - - - - - -
    set     SEQ_STATE_UPDATING, (HL)

    ; check if the main clock should be updated
    ld      A, (SEQ_clockDiv)
    ld      B, A
    ld      A, (SEQ_songTimer)
    sub     B
    jp      M, seq_clock
    jp      Z, seq_clock
    ld      (SEQ_songTimer), A
    jp      end_update

seq_clock:
    ld      HL, SEQ_clockInc
    ld      B, (HL)
    add     A, B
    ld      (SEQ_songTimer), A

    call    SEQ_PlayFrame               ; run commands

end_update:
    ld      HL, SEQ_state
    res     SEQ_STATE_UPDATING, (HL)

    ret
    .endlocal

; ------------------------------------------------------------------------------
; SEQ_PlayFrame plays commands until a Frame Clock command ($19) is encountered.
; FIXME: this subroutine should probably rewritten, it's getting ugly as things
;        get added. Rewrite candidate once everything is in place.
    .local
SEQ_PlayFrame::
    ; setup command engine - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, (SEQ_fmInstrumentsAddr)
    ld      (CMD_fmInstrumentBankAddr), HL
    ld      HL, (SEQ_psgInstrumentsAddr)
    ld      (CMD_psgInstrumentBankAddr), HL

    ; reset current channel variable -  - - - - - - - - - - - - - - - - - - - -
    xor     A
    ld      (SEQ_currentChannelIdx), A

    ; append on the stack the PP addresses for each channels - - - - - - - - - -
    ld      IX, 0
    add     IX, SP                          ; lets put SP in DE
    ld      DE, IX                          ;
    dec     DE                              ; prepare DE for LDDR

    ld      HL, SEQ_channelPPAddresses      ; prepare HL for LDDR
    ld      BC, 2 * (SEQ_MAX_COLS)          ; set the number of bytes to move
    lddr                                    ; block transfer on the stack

    ld      HL, DE                          ; write back the new stack
    inc     HL                              ;   position
    ld      SP, HL
    ; iterate all channels - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      C, 0
for_each_channels:
    ld      B, 0
    ld      HL, SEQ_channelFrameWaitDelay   ; lets load the wait counter in A
    add     HL, BC                          ;
    ld      A, (HL)                         ;
    or      A
    jp      Z, run_channel                  ; if the value has reached 0
    dec     A                               ;
    ld      (HL), A
    ld      A, True                         ; lets clear the wait signal
    ld      (SEQ_waitNextFrame), A          ;

run_channel:
    pop     HL                              ; read channel PP
    ld      (workMem0), HL                  ; write the PP in temp variable
    ld      E, (HL)                         ; put PP address in DE
    inc     HL                              ;
    ld      D, (HL)                         ;
    ; iterate all commands of the current frame - - - - - - - - - - - - - - - -
    ld      (NOS_decodePtr), DE             ; write the channel cmds addr
    push    BC                              ; save C, the channel iteration
until_next_frame:
    ld      A, (SEQ_waitNextFrame)          ; check if the channel met wait
    bit     0, A                            ;    signal
    jp      NZ, wait_next_frame
    call    NOS_Decode                      ; decode instuction

    jp      until_next_frame                ; if not execute next command
    ; save the decode ptr into the sequence's playback pointer - - - - - - - - -
wait_next_frame:
    ld      HL, (workMem0)                  ; restore channel's PP
    ld      DE, (NOS_decodePtr)             ; load current channel's cmd addr
    ld      (HL), E                         ; save it in the channel's PP
    inc     HL                              ;
    ld      (HL), D                         ;

    xor     A                               ; lets clear the wait signal
    ld      (SEQ_waitNextFrame), A          ;

    ld      HL, SEQ_currentChannelIdx       ; increment current channel variable
    inc     (HL)                            ;

    pop     BC                              ; restore B
    inc     C
    ld      A, C
    cp      SEQ_MAX_COLS
    jp      NZ, for_each_channels
    ; increment the frame count  - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (SEQ_framesPerPattern)       ; load number of frames in a pattern
    ld      B, A                            ;   to B
    ld      A, (SEQ_currentFrameId)         ; load the current frame
    inc     A                               ; augment it
    cp      B                               ; if A reached B
    jp      Z, increment_pattern            ; go to next pattern
    ld      (SEQ_currentFrameId), A         ; else write current frame to mem
    ret
    ; increment the pattern - - - - - - - - - - - - - - - - - - - - - - - - - -
increment_pattern:
    ld      A, (SEQ_patternCount)           ; load pattern count of the song
    ld      B, A                            ;   in B
    ld      A, (SEQ_currentPatternId)       ; load the current pattern ID
    inc     A
    cp      B
    jp      Z, loop_song
    ld      (SEQ_currentPatternId), A
    jp      SEQ_LoadPattern                 ; updates channel's PPs
    ; loop song - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
loop_song:
    xor     A
    ld      (SEQ_currentPatternId), A       ; reset current pattern Id
    ld      HL, (SEQ_patternsBaseAddr)      ; reset song PP to start
    ld      (SEQ_pp), HL
    jp      SEQ_LoadPattern                 ; updates channel's PPs

    .endlocal

; ------------------------------------------------------------------------------
; Reserve channel
; input: A: channel to reserve
SEQ_ReserveChannel::
    ld      HL, SEQ_channelStatus
    add     L
    ld      L, A
    set     SEQ_CH_STATUS_RESERVED, (HL)
    ret

; ------------------------------------------------------------------------------
; Release reserved channel, doesn't restore anything though
; input: A: channel to release
SEQ_ReleaseChannel::
    ld      HL, SEQ_channelStatus
    add     L
    ld      L, A
    res     SEQ_CH_STATUS_RESERVED, (HL)
    ret

; ------------------------------------------------------------------------------
; Silence all the channels
    .local
SEQ_Silence::
    ld      B, SEQ_MAX_FM_COLS
    ld      DE, DATA_fmChannelsToHWChannels

foreach_channels:
    ; set key off    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, YM1_ADR
    ld      (HL), YM_KEY
    ld      A, (DE)
    inc     DE
    ld      (YM1_VAL), A

    djnz    foreach_channels
    ret
    .endlocal

; ------------------------------------------------------------------------------
; input: HL addresse if the first status
;        C mute bits
;        B channel count
    .local
SEQ_MutePsgChannelsByBits::
for_each_channels:
    rrc     C
    jp      C, unmuted
muted:
    set     SEQ_CH_STATUS_MUTED, (HL)
    jp      next
unmuted:
    res     SEQ_CH_STATUS_MUTED, (HL)
next:
    inc     HL
    djnz    for_each_channels

    ret
    .endlocal

; ------------------------------------------------------------------------------
; input: HL addresse if the first status
;        C mute bits
;        B channel count
    .local
SEQ_MuteFMChannelsByBits::
    ld      DE, DATA_fmChannelsToHWChannels
for_each_channels:
    rrc     C
    jp      C, unmuted
muted:
    set     SEQ_CH_STATUS_MUTED, (HL)

    ; sends keyoff to YM channel  - - - - -  - - - - - - - - - - - - - - - - - -
    push    HL
    ld      HL,  YM1_ADR
    ld      (HL), YM_KEY
    ld      A, (DE)
    ld      (YM1_VAL), A
    pop     HL

    jp      next
unmuted:
    res     SEQ_CH_STATUS_MUTED, (HL)
next:
    inc     HL
    inc     DE
    djnz    for_each_channels

    ret
    .endlocal


; ------------------------------------------------------------------------------
; set the sequencer current channel to wait for the next clock
SEQ_FrameClock:
    ld      A, True
    ld      (SEQ_waitNextFrame), A
    ret

; ------------------------------------------------------------------------------
;
SEQ_PsgNoteOff:
    ; write the signal key-on for the instrument - - - - - - - - - - - - - - - -
    ld      HL, INS_psgSignals
    add     L
    ld      L, A
    set     INS_SIGNAL_KEY_OFF, (HL)

    ; set volume to max on the PSG
    ld      HL, PSG_REG
    rrca                            ; move channel value in the right places
    rrca                            ; 1 CC R FFFF : latch, channel, register
    rrca
    or      $9F
    ld      (HL), A

    ret

; ------------------------------------------------------------------------------
; restaure channel's attenuation and instrument
; input: SEQ_currentChannelIdx
    .local
SEQ_RestoreChannelProps::
    call    SEQ_BankSwitchToSong

    ; load channel - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (SEQ_currentChannelIdx)
    ld      C, A

    ; load attenuation in instrument input - - - - - - - - - - - - - - - - - - -
    ld      HL, SEQ_channelAttenuations
    add     A, L
    ld      L, A
    ld      A, (HL)
    ld      (INS_inputAttenuation), A

    ; load instrument addr in instrument input - - - - - - - - - - - - - - - - -
    ld      A, C
    sla     A
    ld      HL, SEQ_channelInstruments
    add     A, L
    ld      L, A
    ld      E, (HL)
    inc     HL
    ld      D, (HL)
    ld      (INS_instrumentRomAddr), DE

    ; select the restore logic depending on type - - - - - - - - - - - - - - - -
    ld      A, C
    cp      SEQ_MAX_FM_COLS
    jp      NC, restore_psg

    ; restore fm channel properties  - - - - - - - - - - - - - - - - - - - - - -
restore_fm:
    ld      A, C
    ld      (INS_curChannel), A
    call    INS_SetFMAttenuation

    ld      A, (SEQ_currentChannelIdx)
    jp      INS_LoadFmInstrument

    ; restore psg channel properties - - - - - - - - - - - - - - - - - - - - - -
restore_psg:
    ld      A, C                            ; restore channel value
    sub     SEQ_MAX_FM_COLS                 ; convert to psg channel
    ld      (INS_curChannel), A             ; write in variable
    call    INS_SetPsgAttenuation
    res     INS_SIGNAL_SET_VOLUME, (HL)     ; we clear the volume signal because
                                            ;  dont want the update the PSG on
                                            ;  the next update, it's gonna
                                            ;  produce a glitch
    jp      INS_LoadPsgInstrument
    .endlocal

; ------------------------------------------------------------------------------
; save channel attenuation level in the sequencer attenuation vector.
; input:
;   SEQ_currentChannelIdx (0-10)
;   E: attenuation level (with value suitable for its channel)
SEQ_PersistAttenuation::
    ld      A, (SEQ_currentChannelIdx)
    ld      HL, SEQ_channelAttenuations
    add     A, L
    ld      L, A
    ld      (HL), E
    ret

; ------------------------------------------------------------------------------
; save channel instrument address the sequencer instrument vector.
; input:
;   A: SEQ_currentChannelIdx (0-10)
;   DE: instrument address
SEQ_PersistInstrumentAddr::
    ld      A, (SEQ_currentChannelIdx)
    ld      HL, SEQ_channelInstruments
    sla     A
    add     A, L
    ld      L, A
    ld      (HL), E
    inc     HL
    ld      (HL), D
    ret
