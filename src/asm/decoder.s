; ------------------------------------------------------------------------------
; NOSOUND driver for sega mega drive
;
; Copyright (c) 2023 Emmanuel Rousseau
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
; ------------------------------------------------------------------------------
; decode commands and jump to the its subroutine

; Decode pointer. This is used by the decoder to call the proper command
; subroutine. This pointer may addresses any memory containing a stream
; of command. Each command subroutines are responsible for incrementing it as
; they read the content of this command.
NOS_decodePtr    dw $0000

NOS_lastCmdId    db $00

; ------------------------------------------------------------------------------
; input : (NOS_decodePtr)
; decodes the command pointed by NOS_decodePtr
NOS_Decode:
    ld          HL, (NOS_decodePtr)     ; read the next command current pointer
    ld          B, 0                    ; read the next command opcode in BC
    ld          C, (HL)                 ;
    ld          A, C
    ld          (NOS_lastCmdId), A


    ; find instruction decode table - - - - - - - - - - - - - - - - - - - - - -
    or          A                       ; clear Carry
    rl          C                       ; to find jump address, we take the
    rl          B                       ;  opcode and mul by two and add it to
    ld          HL, NOS_jumpTable       ;  the address of the jump table
    add         HL, BC                  ;  to find the correct address to jump to

    ld          C, (HL)                 ; the address is put into BC
    inc         HL
    ld          B, (HL)

    ; call the decoder's routine - - - - - - - - - - - - - - - - - - - - - - - -
    push        BC
    ret

; ------------------------------------------------------------------------------
; just skip a byte, the slow way :)
NOS_SkipByte:
    ld      HL, (NOS_decodePtr)
    inc     HL
    ld      (NOS_decodePtr), HL
    ret

; ------------------------------------------------------------------------------
; [Command] NOS_NoCommand, stop the driver, the command is unknown and we
; dont know how many bytes to skip.
NOS_NoCommand:
    jp      $

NOS_jumpTable:
    include "decode_jump_table.s"

