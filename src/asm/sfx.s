; ------------------------------------------------------------------------------
; NOSOUND driver for sega mega drive
;
; Copyright (c) 2023 Emmanuel Rousseau
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
; ------------------------------------------------------------------------------
; sfx module is a scaled down version of the sequencer.
;
; the sfx sequencer talks with the main sequencer to reserve a single channel
; in order to execute a sound effect. Only one sound effect at a time can be
; executed.
;

; all SFX has a header of 5 bytes
SFX_HEADER_TYPE_O       equ     0
SFX_HEADER_CHANNEL_O    equ     1
SFX_HEADER_FRM_SIZE_O   equ     2
SFX_HEADER_UPD_PRIOD_O  equ     3
SFX_HEADER_INSTR_ADDR_O equ     4
SFX_HEADER_SIZE         equ     6

; State bits for SFX_stateBits
SFX_STATE_ENABLED           equ     0   ; sfx playback is enabled
SFX_STATE_CHANNEL_LOCK      equ     1   ; a channel is currently
                                        ;  monopolized
; SFX type enums
SFX_TYPE_FM         equ     0           ; SFX uses YM2612
SFX_TYPE_PSG        equ     1           ; SFX uses SN76489

SFX_INVALID_CHANNEL equ     $FF

; ------------------------------------------------------------------------------
; SFX ROM Configuration
; currently played sfx address in ROM, relative to the page
SFX_addr            dw  $0000
; bank where the SFX is stored
SFX_bank            dw  $0000

; ------------------------------------------------------------------------------
; SFX static data; this data comes from the ROM and is immutable

SFX_romData:
; type of SFX, either YM2612 or SN76489
SFX_type            db  $00
; reserved channel
SFX_channelIdx      db  $00
; size in frame count for this SFX
SFX_frameSize       db  $00
; period to wait before going to next frame, works in tadem with SFX_frameTimer
SFX_updatePeriod    db  $00
; instrument database for this SFX
SFX_instrumentsAddr dw  $0000
; pattern address for the SFX
SFX_patternAddr     dw  $0000

; ------------------------------------------------------------------------------
; Playback state

; current state
SFX_stateBits       db  $00
; playback pointer of the sfx. This is the address of the fx row.
SFX_pp              dw  $0000
; when this value reached 0, a SFX step is executed
SFX_frameTimer      db  $00
; number of frame to wait before continuing executing PP.
SFX_frameWaitDelay  db  $00
; the currently reserved channel
SFX_reservedChannel db  $00
; the current frame in the SFX, this value is between 0 and SFX_frameSize
SFX_currentFrameId  db  $00

; ------------------------------------------------------------------------------
; Initialize the module in a correct state, this is called when the driver
; reset is requested.
SFX_Init::
    xor     A
    ld      (SFX_stateBits), A
    ret

; ------------------------------------------------------------------------------
; bank switch to SFX bank
SFX_EnableBank:
    ld      HL, (SFX_bank)
    call    SGA_IsUsingBank         ; if True, Z = False
    ret     Z
    ld      (SGA_selectedBank), HL
    jp      SGA_BankSwitch

; ------------------------------------------------------------------------------
; load a SFX and starts the playback
; input:
;       SFX_addr
;       SFX_bank
SFX_Play::
    ; guard against bank mismatch  - - - - - - - - - - - - - - - - - - - - - - -
    call    SFX_EnableBank

    ; TODO - replace with bank switching mechanics
    ; ld      HL, (SFX_bank)
    ; call    SGA_IsUsingBank         ; if True, Z = True
    ; ret     NZ                      ;

    ; raise playing state for m68k - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, NOS_state
    set     NOS_STATEBIT_SFX_PLAYING, (HL)

    ; reset state - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    xor     A
    ld      (SFX_frameTimer), A
    ld      (SFX_frameWaitDelay), A
    ld      (SFX_currentFrameId), A

    ; load SFX static data - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ; SFX_type, SFX_channelIdx, SFX_frameSize, SFX_updatePeriod
    ld      HL, (SFX_addr)
    ld      DE, SFX_romData
    ld      BC, SFX_HEADER_SIZE - 2
    ldir

    ; load static instrumnet addr - - - - - - - - - - - - - - - - - - - - - - -
    push    DE                          ; save write position
    ld      DE, (HL)                    ; read the instrument offset
    ld      BC, (SFX_addr)              ; read sfx address
    ex      DE, HL                      ; transfert read address to DE
    add     HL, BC                      ; add the offset to write address
    ex      DE, HL                      ; transert back read address to HL
    pop     BC                          ; restore write position
    ld      A, E
    ld      (BC), A                     ; write lsb address of the instruments
    inc     BC
    ld      A, D
    ld      (BC), A                     ; write msb address of the instruments
    inc     BC

    ; load read pattern addr of the SFX - - - - - - - - - - - - - - - - - - - -
    inc     HL
    inc     HL
    ld      (SFX_patternAddr), HL
    ld      (SFX_pp), HL

    ; reserve channel for playback - - - - - - - - - - - - - - - - - - - - - - -
    call    SFX_ReserveChannel

    ; set the SFX engine in enabled state - - - - - - - - - - - - - - - - - - -
    ld      HL, SFX_stateBits
    set     SFX_STATE_ENABLED, (HL)     ; set in enabled mode so the update can
                                        ;  now start the playback

    ret

; ------------------------------------------------------------------------------
; reserves the channel, if a channel is already reserved, the channel is first
; released (only if it's different)
; input: SFX_reservedChannel, SFX_channelIdx, SFX_stateBits
    .local
SFX_ReserveChannel::
    ; check if the engine already reserved a channel - - - - - - - - - - - - - -
    ld      HL, SFX_stateBits               ; if the reserve bit is up, we
    bit     SFX_STATE_CHANNEL_LOCK, (HL)    ;  need to release before reserving
    jp      Z, reserve_channel

    ; if its the case release it first - - - - - - - - - - - - - - - - - - - - -
release_channel:
    ld      A, (SFX_channelIdx)         ; prevent reserve the same channel
    ld      L, A
    ld      A, (SFX_reservedChannel)    ; first lets prevent to release and
    cp      L                           ;  if req channel and reserved channel
    ret     Z                           ;  match, lets return; nothing to do

    ld      (SEQ_currentChannelIdx), A  ; this variable is required for the
                                        ;  call to SEQ_RestoreChannelProps
    call    SEQ_ReleaseChannel          ; release channel A
    call    SEQ_RestoreChannelProps
    call    SFX_EnableBank

    ; then ask the Sequencer to reserve a channel for the SFX playback - - - - -
reserve_channel:
    ld      A, (SFX_channelIdx)             ; load which channel to reserve
    ld      (SFX_reservedChannel), A        ; save the channel in reserved var
    call    SEQ_ReserveChannel              ; reserve channel A

    ; raise the reserved channel state flag - - - - - - - - - - - - - - - - - -
    ld      HL, SFX_stateBits
    set     SFX_STATE_CHANNEL_LOCK, (HL)

    ret
    .endlocal

; ------------------------------------------------------------------------------
; Release the reserved channel
SFX_ReleaseChannel::
    ; check if the engine already reserved a channel - - - - - - - - - - - - - -
    ld      HL, SFX_stateBits               ; if the reserve bit is not up,
    bit     SFX_STATE_CHANNEL_LOCK, (HL)    ;  there's nothing to release
    ret     Z

    ; release channel - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    res     SFX_STATE_CHANNEL_LOCK, (HL)    ; clear the lock

    ld      A, (SFX_reservedChannel)
    ld      (SEQ_currentChannelIdx), A
    call    SEQ_ReleaseChannel              ; release channel A
    call    SEQ_RestoreChannelProps

    ; set the current reserved channel to a invalid value
    ld      A, SFX_INVALID_CHANNEL
    ld      (SFX_reservedChannel), A

    ret

; ------------------------------------------------------------------------------
; Update the SFX sequencer
    .local
SFX_Update::
    ; if the SFX sequencer is disabled, do nothing - - - - - - - - - - - - - - -
    ld      HL, SFX_stateBits
    bit     SFX_STATE_ENABLED, (HL)
    ret     Z

    call    SFX_EnableBank

    ; handles timer, the timer is responsible for the rate of frame  - - - - - -
    ld      HL, SFX_frameTimer
    ld      A, (HL)
    or      A
    jp      Z, handle_wait_frame_delay
    dec     (HL)
    ret

handle_wait_frame_delay:
    ld      A, (SFX_updatePeriod)           ; reset frame timer to sfx period
    ld      (HL), A

    ; handles the wait frame delay, this value is set by wait commands - - - - -
    ld      HL, SFX_frameWaitDelay
    ld      A, (HL)
    or      A
    jp      Z, step_engine
    dec     (HL)
    ;ld      HL, SFX_currentFrameId      ; increment the current frame
    ;inc     (HL)                        ;
    jp      update_frame_counter

    ; entering here, we are about to do an engine increment  - - - - - - - - - -
step_engine:
    ld      HL, (SFX_instrumentsAddr)       ; lets prepare the command module
    ld      (CMD_fmInstrumentBankAddr), HL  ;  for instrument loading
    ld      (CMD_psgInstrumentBankAddr), HL ;

    ld      A, (SFX_reservedChannel)        ; load in the sequencer the channel
    ld      (SEQ_currentChannelIdx), A      ;  because most commands use it

    ld      DE, (SFX_pp)                    ; load the PP addr in the
    ld      (NOS_decodePtr), DE             ;  decoder command variable

    ; decoding commands loop - - - - - - - - - - - - - - - - - - - - - - - - - -
until_next_frame:
    call    NOS_Decode                      ; call the decoder until

    ld      A, (SEQ_waitNextFrame)          ; load the wait next frame flag
    or      A                               ; if the flag isn't set
    jp      Z, until_next_frame             ;  execute next command

    ; lets update PP - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      DE, (NOS_decodePtr)             ; save the decoder address
    ld      (SFX_pp), DE                    ;  to playback position

    ; clear the wait next frame flag - - - - - - - - - - - - - - - - - - - - - -
    xor     A
    ld      (SEQ_waitNextFrame), A

    ; update the frame counter - - - - - - - - - - - - - - - - - - - - - - - - -
update_frame_counter:
    ld      A, (SFX_frameSize)          ; load size of this SFX in frames
    ld      C, A                        ;  put the value is C for later

    ld      HL, SFX_currentFrameId      ; increment the current frame
    inc     (HL)                        ;

    ld      A, (HL)                     ; lets compare current frame with size
    cp      C                           ;  if the frame ID eq frame size
    jp      Z, stop_playback            ;  lets stop the playback

    ret

    ; stop playback when the SFX reaches its end   - - - - - - - - - - - - - - -
stop_playback:
    ld      HL, SFX_stateBits           ; lets clear the enable state bit to
    res     SFX_STATE_ENABLED, (HL)     ;  prevent further updates

    ; write the key off signal in the instrument engine  - - - - - - - - - - - -
    ld      A, (SFX_type)               ; depending on the type of channel,
    or      A                           ;  the signal variable differs.
    jp      NZ, cut_psg
cut_fm:
    ld      HL, INS_fmSignals           ; lets assign the key off signal to
    jp      set_key_off_signal          ;  the FM var
cut_psg:
    ld      HL, INS_psgSignals          ; lets assign the key off signal to
                                        ;  the PSG var
set_key_off_signal:
    ld      A, (SFX_reservedChannel)    ; lets use the current reserved channel
    add     L                           ;  to compute the right channel signal
    ld      L, A                        ;  address.
    set     INS_SIGNAL_KEY_OFF, (HL)    ; raise the key-on bit

    ; clears the sfx playing variable for m68k - - - - - - - - - - - - - - - - -
    ld      HL, NOS_state
    res     NOS_STATEBIT_SFX_PLAYING, (HL)

    ; release channel  - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    jp      SFX_ReleaseChannel          ; release the currently reserved channel

    .endlocal

