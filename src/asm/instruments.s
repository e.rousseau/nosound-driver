; ------------------------------------------------------------------------------
; NOSOUND driver for sega mega drive
;
; Copyright (c) 2023 Emmanuel Rousseau
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
; ------------------------------------------------------------------------------

YM_DELAY    equ 5

ym_delay: .macro &p
    .rept &p
    nop
    .endm
    .endm

; maximum updatable macros
INS_MAX_MACROS              equ 16
; number of macro per instruments
INS_MAX_INSTR_MACROS        equ 4
; when a PSG macro is not used, it's register is set to $FF
INS_UNSET_MACRO             equ $FF
INS_INFINITY                equ $FF
INS_MACRO_STATE_NORMAL      equ $00
INS_MACRO_STATE_RELEASED    equ $01
INS_MACRO_STATE_STOPPED     equ $02

; type of dirtyness
INS_DIRTY_ATTENUATION_BIT   equ 0
INS_DIRTY_PITCH_BIT         equ 1
INS_DIRTY_NOISE_BIT         equ 2

INS_DIRTY_ALL               equ 3

;
INS_ROM_INSTRUMENT_SIZE     equ 32
; offset in rom for the data of a macro
INS_ROM_MACRO_DATA_OFFSET   equ 4

INS_ROM_MACRO_SIZE          equ 6

INS_PSG_REG_ATTENUATION_BIT equ 4

; this value is an input parameter for LoadFmInstrument and LoadPsgInstrument
; subroutine (instrument in ROM)
INS_instrumentRomAddr:      dw $0000

; `cur` variables are memory space used to speed up processing in various
; ways. Either by storing the values once calculted here for later reuse
; or because it is incrementaly computed withing a loop.

; current address of the FM channel data (within INS_fmChannel)
INS_curFMChannelDataAddr        dw $0000
; current FM or PSG channel index (0-6) or (0-4)
INS_curChannel                  db $00
; current register used by the PSG channel [deprecated?]
INS_cur_psg_register            db $00
; address of the current updated macro
INS_cur_macro_addr              dw $0000
; address of the current operated PSG frequency (PSG uses periods)
INS_cur_psg_perdiod_delta_addr  dw $0000

; signal bits
INS_SIGNAL_KEY_ON               equ 0
INS_SIGNAL_KEY_OFF              equ 1
INS_SIGNAL_SET_VOLUME           equ 2

; each bytes here contains a list of bits indicating if the corresponding
; operator is affected by volume command on the channel
    .align  8
INS_ALGO_OP_OUT:
    db  1000b       ; algorithm 0: op4 only
    db  1000b       ; algorithm 1: op4
    db  1000b       ; algorithm 2: op4
    db  1000b       ; algorithm 3: op4
    db  1100b       ; algorihtm 4: op4 and op2
    db  1110b       ; algorithm 5: op2 op3 and op4
    db  1110b       ; algorithm 6: op2 op3 and op4
    db  1111b       ; algorithm 7: all

; ------------------------------------------------------------------------------
; FM instrument channel structure
    struct
    f_u8        INS_FM_CHANNEL_ATTENUATION_O    ; current attenuation of channel
    f_u8        INS_FM_ALGO_O       ; currently used algorithm
    f_u8        INS_FM_ATT_BITS_O   ; the operators affected by attenuation
    f_u8        INS_FM_ATT0_O       ; attenuation of operator 0
    f_u8        INS_FM_ATT1_O       ; attenuation of operator 1
    f_u8        INS_FM_ATT2_O       ; attenuation of operator 2
    f_u8        INS_FM_ATT3_O       ; attenuation of operator 3
    end_struct  INS_FM

INS_FM_CHANNEL_RAM_SIZE     equ 8
; ensure the structure has a size smaller than the alignment size
.assert     INS_FM_SIZE <= INS_FM_CHANNEL_RAM_SIZE
; channel data
    .align 64
INS_fmChannel:
    ds      INS_FM_CHANNEL_RAM_SIZE * SEQ_MAX_FM_COLS, $FA
; input notes for all the PSG channels
    .align 16
INS_psgInputNote:
    db  $FC, $FC, $FC, $FC
INS_psgInputAttenuations:
    db  $00, $00, $00, $00
INS_psgMacroAttenuations:
    db  $00, $00, $00, $00
; there values are computed by the macros of each instruments.
INS_psgPeriodDeltas:
    db  $FE, $FE, $FE, $FE
; psg signals is a communication channel for the sequencer or other instrument
; subroutine to request instrument operation. See INS_SIGNAL_* defines above.
    .align 4
INS_psgSignals:
    db  $FB, $FB, $FB, $FB

; fm signals is a communication channel for the sequencer or other instrument
; subroutine to request instrument operation. See INS_SIGNAL_* defines above.
    .align 8
INS_fmSignals:
    ds SEQ_MAX_FM_COLS, $00

    .align 8
INS_fmInputNote:
    ds SEQ_MAX_FM_COLS, $F1

; noise source for the PSG Channel 4
INS_psgNoiseSource:
    db  $00

; ------------------------------------------------------------------------------
; PSG macro structure in RAM

    struct
    f_u8        INS_PSG_AFFECTED_REG_O
    f_u8        INS_PSG_PTIMER_O
    f_ptr       INS_PSG_MACRO_ADDR_O
    f_ptr       INS_PSG_MACRO_PP_O
    f_u8        INS_PSG_MACRO_POS_O
    f_u8        INS_PSG_STATE_O
    end_struct  INS_PSG

; size of a macro block (INS_PSG)
INS_RAM_MACRO_SIZE  equ 8
; precalculated the position of each channel's instrument
    .align 4
INS_RAM_INSTRUMENT_OFFSETS  db  0, 32, 64, 96

; ensures macro structure is smaller than requested block
.assert INS_PSG_SIZE <= INS_RAM_MACRO_SIZE

; this variable is used to pass attenuation values to the instrument engine from
; the sequencer, either FM or SN.
INS_inputAttenuation:
    db  $00

; ------------------------------------------------------------------------------
; block of every macros. Macros are regrouped in blocks of 4. Only 3 per block
; are used. For the pulse channel, volume, arpegios and pitch
INS_macros:
    ds      INS_RAM_MACRO_SIZE * INS_MAX_MACROS, $FF

; ------------------------------------------------------------------------------
; clears all the instrument data
INS_Reset::
    .local
    ; clear the macro data - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, INS_macros
    ld      A, $FF
    ld      B, INS_MAX_MACROS * INS_RAM_MACRO_SIZE
foreach_macros:
    ld      (HL), A
    inc     HL
    djnz    foreach_macros

    ; reset FM instrument channel data
    ld      HL, INS_fmChannel
    ld      B, INS_FM_CHANNEL_RAM_SIZE * SEQ_MAX_FM_COLS
    ld      A, $7F
foreach_fm_channel_ram_data:
    ld      (HL), A
    inc     HL
    djnz    foreach_fm_channel_ram_data

    ; reset signals - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      IX, $0000
    add     IX, SP

    ld      DE, $0000
    ld      HL, INS_fmInputNote+8
    ld      SP, HL
    ld      B, 6
foreach_signals:
    push    DE
    push    DE
    djnz    foreach_signals
    ld      SP, IX

    ; setup YM address writing - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, channelFmRegisterAddr+1
    ld      (HL), $40                       ; $40 is the MSB addr MMIO of the YM
    ld      HL, channelFmValAddr+1
    ld      (HL), $40

    ret
    .endlocal

; ------------------------------------------------------------------------------
; load a PSG instrument
; ------------------------------------------------------------------------------
; LoadFmInstrument
; input :       INS_instrumentRomAddr
;               INS_curPsgChannel (0-4)
INS_LoadPsgInstrument::
    ld      A, (INS_curChannel)
    ; set INS_cur_macro_addr to the macro space of the requested channel - - - -
    ld      D, 0                            ; lets get the proper instrument
    ld      E, A                            ; offset
    ld      HL, INS_RAM_INSTRUMENT_OFFSETS
    add     HL, DE
    ld      A, (HL)                         ; A = instrument offset

    ld      HL, INS_macros                  ; get the offset of the first
    ld      B, 0                            ;  macro address of the channel
    ld      C, A                            ;  by using the instrument offset
    add     HL, BC                          ;
    ld      (INS_cur_macro_addr), HL        ; INS_cur_macro_addr is macro0 addr

    ; store the base register value in INS_cur_psg_register - - - - - - - - - -
    or      $90                             ; set latch bit and attenuation bit
    ld      (INS_cur_psg_register), A       ; store channel in register format

    ; load attenuation macro - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      DE, (INS_instrumentRomAddr)
    ld      A, D
    or      A
    ret     Z                               ; return if instrumnet addr is invalid

    call    INS_AllocatePsgMacro

    ; clear the PSG Macro Attenuation to 0 - - - - - - - - - - - - - - - - - - -
    ld      HL, INS_psgMacroAttenuations
    ld      A, (INS_curChannel)             ; read current PSG channel
    add     L
    ld      L, A
    xor     A
    ld      (HL), A

    ; load arpegio macro - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (INS_cur_psg_register)   ; change to frequency register
    and     $EF                         ; clears attenuation bit, make frequency
    ld      (INS_cur_psg_register), A   ;  register

    ld      BC, INS_RAM_MACRO_SIZE      ; increment write position
    ld      HL, (INS_cur_macro_addr)    ;
    add     HL, BC                      ;
    ld      (INS_cur_macro_addr), HL    ;

    ld      BC, INS_ROM_MACRO_SIZE      ; increment read position
    ld      HL, DE                      ;
    add     HL, BC                      ;
    ld      DE, HL                      ; in DE

    call    INS_AllocatePsgMacro

    ; load pitch macro - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      BC, INS_RAM_MACRO_SIZE      ; increment write position
    ld      HL, (INS_cur_macro_addr)    ;
    add     HL, BC                      ;
    ld      (INS_cur_macro_addr), HL    ;

    ld      BC, INS_ROM_MACRO_SIZE      ; increment read position
    ld      HL, DE                      ;
    add     HL, BC                      ;
    ld      DE, HL                      ; in DE

    call    INS_AllocatePsgMacro

    ; load duty cycle macro - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, $E0                      ; duty register is always $E0
    ld      (INS_cur_psg_register), A

    ld      BC, INS_RAM_MACRO_SIZE      ; increment write position
    ld      HL, (INS_cur_macro_addr)    ;
    add     HL, BC                      ;
    ld      (INS_cur_macro_addr), HL    ;

    ld      BC, INS_ROM_MACRO_SIZE      ; increment read position
    ld      HL, DE                      ;
    add     HL, BC                      ;
    ld      DE, HL                      ; in DE

    jp      INS_AllocatePsgMacro

; ------------------------------------------------------------------------------
; input :   INS_cur_macro_addr: macro address to allocate in RAM
;           INS_cur_psg_channel: register affecting this macro
;           DE: ROM address of the macro
; destroys : HL, A
INS_AllocatePsgMacro::
    .local
    ld      HL, (INS_cur_macro_addr)    ; write register to update
    ; check if the macro should be allocated or freed  - - - - - - - - - - - - -
    ld      A, (DE)                     ; load macro size
    or      A                           ; compare to 0
    jp      Z, freePsgMacro             ; free macro instead

    ; write register this macro affects  - - - - - - - - - - - - - - - - - - - -
    ld      A, (INS_cur_psg_register)
    ld      (HL), A
    inc     HL

    ; reset ptimer to zero - - - - - - - - - - - - - - - - - - - - - - - - - - -
    xor     A
    ld      (HL), A
    inc     HL

    ; set the ROM address  - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      (HL), E
    inc     HL
    ld      (HL), D
    inc     HL

    ret
freePsgMacro:
    ld      (HL), INS_UNSET_MACRO
    ret
    .endlocal

; ------------------------------------------------------------------------------
; Reset the current PSG instruments to zero, this happens when a note is being
; played. Resetting an instrument reset the PP at 0 for the macros that are
; enabled (have register not being $FF).
;
; input:
;  HL: ram instrument, first macro address
; destroys: B, DE, HL
INS_ResetPsgInstrument::
    .local
    ; places HL at macro pos - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      DE, INS_PSG_MACRO_PP_O
    add     HL, DE
    ld      DE, INS_RAM_MACRO_SIZE

    ld      B, INS_MAX_INSTR_MACROS
foreach_macros:
    xor     A
    ld      (HL), A
    add     HL, DE
    djnz    foreach_macros
    ret
    .endlocal

; ------------------------------------------------------------------------------
; INS_UpdatePsgChannels is the main update subroutine for the intrument engine
; it updates macros behind PSG intruments.

channelSignal           db  $00
channelSignalAddr       dw  $0000

; harware channel identifier. In the case of FM, this is 0, 1, 2, 4, 5, 6
; in the case of PSG this is 0, 32, 64, 96
channelHWChannel        db  $00
channelFmRegisterAddr:  dw  $0000       ; where to write YM registers
channelFmValAddr        dw  $0000       ; where to write values for YM register
channelNote             db  $00
channelAmplitude:                       ; aliases, amplitude or attenuation
channelAttenuation:
    db  $00
channelMacroAttenuation db  $00
channelPeriodDelta      db  $00

    .local
macroRamLocalStruct:
macroRegister       db  $00         ; PSG register
macroTimer          db  $00         ; timer before next update
macroRomAddr        dw  $0000       ; address of macro in ROM
macroPP             dw  $0000       ; current macro data playback pointer
macroCounter        db  $00         ; index in the macro data (analogous to PP)
macroState          db  $00         ; current state of the macro
macroRamLocalStruct_end:

macroRomLocalStruct:
macroSize           db  $00
macroPeriod         db  $00
macroLoop           db  $00
macroRelease        db  $00
macroDataAddrOffset dw  $0000
macroRomLocalStruct_end:

macroDirty          db  $00

INS_UpdatePsgChannels::
    xor     A                           ; psg channel
    ld      (INS_curChannel), A

    ld      HL, INS_macros
    ld      (INS_cur_macro_addr), HL    ; ram macro address

    ld      HL, INS_psgSignals
    ld      (channelSignalAddr), HL     ; signal addresse

    ld      HL, INS_psgPeriodDeltas
    ld      (INS_cur_psg_perdiod_delta_addr), HL    ; period deltas address

foreach_channels:
    ; load channel current note - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, INS_psgInputNote
    add     L
    ld      L, A
    ld      A, (HL)
    ld      (channelNote), A

    ; load channel current attenuation - - - - - - - - - - - - - - - - - - - - -
    set     2, L                        ; => ld   HL, INS_psgInputAttenuations
    ld      A, (HL)                     ;           + channel
    ld      (channelAttenuation), A     ; load ch. attenuation in variable

    ; load channel current macro attenuation (this is computed by macros) - - -
    set     3, L                        ; => ld   HL, INS_psgMacroAttenuations
    res     2, L                        ;           + channel
    ld      A, (HL)                     ; loads ch. macro attenuation in
    ld      (channelMacroAttenuation), A;   variable

    ; load channel current period delta  - - - - - - - - - - - - - - - - - - - -
    ld      HL, (INS_cur_psg_perdiod_delta_addr)    ; FIXME remove this variable and use `set` instead?
    ld      A, (HL)
    ld      (channelPeriodDelta), A

    ; clear dirty flag - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    xor     A
    ld      (macroDirty), A

    ; load and process signal - - - - - - - - - - - - - - - - - - - - - - - - -
    ;  in the case of the PSG, the key on and key off signal is only consumed by
    ;  the macros. The volume change is consumed by the channel itself though.
    ld      HL, (channelSignalAddr)
    ld      A, (HL)                     ; load signal value
    ld      (channelSignal), A          ; write it in the variable

    bit     INS_SIGNAL_SET_VOLUME, A    ; check if the set volume signal is set
    jp      Z, update_instruments

    ld      HL, macroDirty              ; force attenuation update
    set     INS_DIRTY_ATTENUATION_BIT, (HL)

    ; call updates on all macros of this instrument - - - - - - - - - - - - - -
update_instruments:
    call    INS_UpdateInstrument

    ; apply update if dirty - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (INS_curChannel)         ; calculates the register to write
    rrca                                ; move channel value in the right places
    rrca                                ; 1 CC R FFFF : latch, channel, register
    rrca                                ;               frequency
    ld      (channelHWChannel), A

    ld      A, (macroDirty)
    bit     INS_DIRTY_NOISE_BIT, A
    jp      Z, set_pitch

set_noise_ctrl:
    call    INS_WriteChannelNoiseSource

    ld      A, (macroDirty)
set_pitch:
    bit     INS_DIRTY_PITCH_BIT, A
    jp      Z, set_attenuation

    call    INS_GetPSGPeriodForNote
    call    INS_WriteChannelPeriod

    ld      A, (macroDirty)
set_attenuation:
    bit     INS_DIRTY_ATTENUATION_BIT, A
    jp      Z, clear_signal

    call    INS_WritePsgChannelAttenuation

clear_signal:
    ld      A, (INS_curChannel)
    ; save note back  - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, INS_psgInputNote
    add     L
    ld      L, A
    ld      A, (channelNote)
    ld      (HL), A

    ; save delta period back and increment address pointer - - - - - - - - - - -
    ld      A, (channelPeriodDelta)                 ; load new delta value
    ld      HL, (INS_cur_psg_perdiod_delta_addr)    ; load channel delta addr
    ld      (HL), A                                 ; write new value to channel
    inc     HL                                      ; increment the delta
                                                    ; value addr for the next
    ld      (INS_cur_psg_perdiod_delta_addr), HL    ; channel

    ; clear the signal  - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, (channelSignalAddr)
    ld      (HL), $00

    ; go to next channel signal  - - - - - - - - - - - - - - - - - - - - - - - -
    inc     HL
    ld      (channelSignalAddr), HL     ; save it back

    ;  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, INS_curChannel          ; load current psg channel
    inc     (HL)                        ; increment psg channel
    ld      A, (HL)                     ; load current psg channel in A for cp

    cp      SEQ_MAX_PSG_COLS            ; while cur psg channel < 4
    jp      NZ, foreach_channels

    ret

; ------------------------------------------------------------------------------
; update the PSG instruments state, loads each macros in a dedicated memory
; space then operates transformations and save back. Once this is done, each
; macros are processed to update the state of its channel.
; input:    all macro and channel variable.
; output:   same as input
; destroys: all all registers except IY
INS_UpdateInstrument::
    ld      HL, (INS_cur_macro_addr)
    ld      B, INS_MAX_INSTR_MACROS
foreach_macro:
    ; RAM macro load phase  - - - - - - - - - - - - - - - - - - - - - - - - - -
    ; check if the macro is active
    ld      A, (HL)
    ld      (macroRegister), A
    cp      INS_UNSET_MACRO             ; if macro is not enabled
    jp      Z, next_macro               ; skip it

    call    INS_ReadMacro
    call    INS_ProcessTimer
    call    INS_SaveMacro

    ; check if the macro requested an instrument update - - - - - - - - - - - -
    ld      A, IXL                      ; [is IXL realy requered here?]
    or      A
    jp      Z, next_macro

    ; jump table used to process the macros - - - - - - - - - - - - - - - - - -
    ld      A, B                        ; multiplies by three
    add     B                           ; this is the size of `jp` instruction
    add     B                           ;

    ld      ($+4), A                    ; lets rewrite the relative jump
    jr      $                           ;   instruction so it will select the
                                        ;   proper `jp` in the table.
    db      $00, $00, $00
    jp      duty_macro
    jp      pitch_macro
    jp      arpegio_macro
    jp      attenuation_macro

    ; process duty cycle macro (noise channel) - - - - - - - - - - - - - - - - -
duty_macro:
    ld      HL, (macroPP)
    ld      A, (HL)
    ld      (INS_psgNoiseSource), A
    jp      next_macro

    ; process pitch bends - - - - - - - - - - - - - - - - - - - - - - - - - - -
pitch_macro:
    ld      HL, (macroPP)               ; load pitch delta
    ld      E, (HL)                     ;   in E
    ld      A, (channelPeriodDelta)     ; load in HL the current period delta
    add     E                           ; adds new delta value
    ld      (channelPeriodDelta), A     ; write it back
    jp      next_macro

    ; process arpegio macro - - - - - - - - - - - - - - - - - - - - - - - - - -
arpegio_macro:
    ld      HL, (macroPP)               ; load the next note delta
    ld      E, (HL)                     ;   in E
    ld      A, (channelNote)            ; apply this delta to the current note
                                        ;   played by this channel
    add     E
    ld      (channelNote), A
    jp      next_macro

    ; process attenuation macro - - - - - - - - - - - - - - - - - - - - - - - -
attenuation_macro:
    ld      HL, (macroPP)
    ld      A, (HL)
    ld      (channelMacroAttenuation), A

next_macro:
    ld      DE, INS_RAM_MACRO_SIZE
    ld      HL, (INS_cur_macro_addr)
    add     HL, DE
    ld      (INS_cur_macro_addr), HL
    djnz    foreach_macro
    ret

; ------------------------------------------------------------------------------
; returns IXL = True if the macro needs to update the instrument
;
INS_ProcessTimer::
    .local
    ld      IXL, False              ; clear the `must update instrument` flag
    ld      A, (channelSignal)

    ; check what signal to process - - - - - - - - - - - - - - - - - - - - - - -
    bit     INS_SIGNAL_KEY_ON, A
    jp      NZ, key_pressed
    bit     INS_SIGNAL_KEY_OFF, A
    jp      NZ, key_released

    ; if macro state is stopped - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (macroState)
    cp      INS_MACRO_STATE_STOPPED
    ret     Z                           ; lets leave then

    ; normal update management - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (macroTimer)         ; check if the timer reached zero
    or      A                       ;
    jp      Z, move_counter         ; if its the case lets move the counter
    cp      INS_INFINITY            ; if timer is set to infinity, it cant be
                                    ; decremented. LIMIT Timer < 254
    ret     Z
    dec     A                       ; else lets decrement it and do nothing
    ld      (macroTimer), A
    ret

    ; lets increment the macroCounter
move_counter:
    ld      IXL, True               ; request instrument to be updated

    ld      A, (macroPeriod)        ; the period is assigned
    ld      (macroTimer), A         ;   to timer (reseting it)

    ; jump to release handling if state is in release mode
    ld      A, (macroState)
    cp      INS_MACRO_STATE_RELEASED
    jp      Z, release_handler

    ; jump to loop handling if loop is set
    ld      A, (macroLoop)          ; store loop value in L
    ld      L, A
    cp      INS_INFINITY            ; check if the loop value is set
    jp      NZ, loop_handler

release_handler:
    ld      A, (macroSize)          ; load size of the macro
    ld      C, A
    ld      A, (macroCounter)
    inc     A
    cp      C                       ; if counter == size, lets stop the macro
    jp      Z, INS_StopMacro
    jp      set_pp                  ; A contains macro Counter value

    ; the subroutine check if the macro reached some loop conditions it its the
    ; case, the counter is reset to loop position  - - - - - - - - - - - - - - -
loop_handler:
    ; handle when the counter reaches end of macro or `release` point  - - - - -
    ld      A, (macroRelease)
    ld      E, A                    ; E contains macroRelease
    ld      A, (macroSize)
    ld      D, A                    ; D contains macroSize

    ld      A, (macroCounter)       ; lets compare the macro counter to
    inc     A
    cp      E                       ; macroRelease
    jp      Z, apply_loop           ; then apply the loop
    cp      D                       ; else compare to size
    jp      NZ, end_loop            ; skip apply_loop if the end isn't reached
apply_loop:
    ld      A, L                    ; replace the counter to the loop position
end_loop:
    jp      set_pp                  ; change the macroCounter and macroPP values

    ; process key-on signal - - - - - - - - - - - - - - - - - - - - - - - - - -
    ; uppon keupress, the timer, macro pointer and its counter are reset
key_pressed:
    xor     A                               ; reset macroCounter to period
    ld      (macroCounter), A               ;   counter

    ld      A, (macroPeriod)
    ld      (macroTimer), A

    ld      HL, (macroDataAddrOffset)
    ld      DE, (macroRomAddr)
    add     HL, DE
    ld      (macroPP), HL

    ld      A, INS_MACRO_STATE_NORMAL       ; set state of the macro
    ld      (macroState), A
    ld      HL, macroDirty
    ld      (HL), INS_DIRTY_ALL             ; set channel dirty

    ld      IXL, True                       ; signal the macro changed the
                                            ;   instrument
    ret

    ; process key-off signal - - - - - - - - - - - - - - - - - - - - - - - - - -
    ; uppon key release, the macro counter is set at release value (if set) else
    ; the timer is set to infinity ($FF)
key_released:
    ld      A, (macroState)                 ; check that state is normal
    cp      INS_MACRO_STATE_RELEASED        ; if not, lets skip the transition
    jp      Z, set_pp

    ld      A, INS_MACRO_STATE_RELEASED     ; set the state to `released`
    ld      (macroState), A

    ld      A, (macroRelease)               ; check if the release marker is
    cp      INS_INFINITY                    ; within the macro size
    jp      Z, INS_StopMacro                ; if not, stop the macro

    ; ** fallthrough - `A` contains the Release Position

set_pp:
    ld      (macroCounter), A               ; save counter state
    ld      HL, macroRegister               ; load SN register
    ld      A, (HL)                         ; into C
    ld      HL, macroDirty                  ; load dirty flags address for later
    bit     INS_PSG_REG_ATTENUATION_BIT, A  ; if the attenuation bit
    jp      NZ, set_pp_attenuation          ; go to attenuation

set_pp_pitch_or_duty:                       ; TODO move that in the upper level
    cp      11100000b
    jp      NZ, set_pp_pitch
    set     INS_DIRTY_NOISE_BIT, (HL)
    jp      set_pp_2

set_pp_pitch:
    set     INS_DIRTY_PITCH_BIT, (HL)       ; set the pitch dirty bit
    jp      set_pp_2

set_pp_attenuation:                         ; TODO move that in the upper level
    set     INS_DIRTY_ATTENUATION_BIT, (HL) ; set the attenuation dirty bit

set_pp_2:
    ld      HL, (macroDataAddrOffset)             ; start of the data array
    ld      DE, (macroRomAddr)
    add     HL, DE
    ld      A, (macroCounter)
    ld      D, 0
    ld      E, A
    add     HL, DE
    ld      (macroPP), HL
    ld      IXL, True                       ; signal the macro need to update
                                            ;   the instrument
    ret
    .endlocal

; ------------------------------------------------------------------------------
; set the macro state to stopped, so it's no longer updated nor written
; to the SN.
INS_StopMacro:
    ld      A, INS_INFINITY                 ; set the timer to infinity, for
    ld      (macroTimer), A                 ;   safety
    ld      A, INS_MACRO_STATE_STOPPED      ; then set the state to stopped
    ld      (macroState), A                 ;   it's now going to be skipped
    ret

; ------------------------------------------------------------------------------
; read macro data into local variables
; input: INS_cur_macro_addr
INS_ReadMacro:
    exx
    ; read the dynamic part
    ld      DE, macroRamLocalStruct
    ld      HL, (INS_cur_macro_addr)
    ld      BC, INS_PSG_SIZE
    ldir

    ; read the static part
    ld      DE, macroRomLocalStruct
    ld      HL, (macroRomAddr)
    ld      BC, macroRomLocalStruct_end - macroRomLocalStruct
    ldir
    exx

    ret

; ------------------------------------------------------------------------------
; INS_WriteChannelNoise into the noise channel
INS_WriteChannelNoiseSource::
    ld      HL, PSG_REG

    ld      A, (INS_psgNoiseSource)         ; lets load noise source ctrl values
    ; noise source is always register 6 (110b)
    or      $E0
    ld      (HL), A
    ret

; ------------------------------------------------------------------------------
; INS_WritePsgAttenuation into the current channel pointed by `channelSNChannel`
INS_WritePsgChannelAttenuation::
    ; compute final attenuation in B following the different parameters - - - -
    ld      HL, INS_psgMacroAttenuations
    ld      A, (INS_curChannel)             ; save back in the channel
    add     L                               ;  the macro attenuation value
    ld      L, A                            ;  before setting up E for the scale
    ld      A, (channelMacroAttenuation)    ;  operation
    ld      (HL), A

    ld      E, A                        ; prepare scale attenuation operation
    ld      A, (channelAttenuation)     ;  load the attenuation values in
    ld      D, A                        ;  D and E
    call    M_ScaleAttenuation_4        ; (D*E) => A

    ; write attenuation in the SN register - - - - - - - - - - - - - - - - - - -
    ld      HL, PSG_REG                     ; HL points on the sn76489 address
    ld      E, A                            ; E = final attenuation
    ld      A, (channelHWChannel)           ; calculates the register to write
    or      $90                             ; adds latch bit and attenuation bit
    or      E                               ; adds attenuation
    ld      (HL), A                         ; write to SN76489
    ret

; ------------------------------------------------------------------------------
; change the volume base value for a given FM channel
; input: INS_inputAttenuation
;        INS_curChannel (0-4)
INS_SetPsgAttenuation::
    ; compute channel attenuation addr - - - - - - - - - - - - - - - - - - - - -
    ld      HL, INS_psgInputAttenuations
    ld      A, (INS_curChannel)
    ld      C, A                    ; save channel in C
    add     L
    ld      L, A                    ; HL points at the input attenuation

    ; write attenuation and set volume flag - - - - - - - - - - - - - - - - - -
    ld      A, (INS_inputAttenuation)
    ld      (HL), A

    ld      HL, INS_psgSignals
    ld      A, C
    add     L
    ld      L, A

    set     INS_SIGNAL_SET_VOLUME, (HL)

    ret

; ------------------------------------------------------------------------------
; INS_GetPSGPeriodForNote load the proper frequency for a given input note
; input: channelNote
; output: DE: period
INS_GetPSGPeriodForNote:
    ; compute period for note - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (channelNote)
    ld      HL, DATA_psg_notes      ; read the PSG period table
    sla     A                       ; A contains the note, let multiply by 2
    add     L                       ; augment address by A
    ld      L, A
    ld      E, (HL)
    inc     HL
    ld      D, (HL)                 ; E contains LSB period, D contains MSBs

    ; apply pitch delta - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (channelPeriodDelta)
    or      A
    jp      Z, skip_pitch
    ld      HL, DE                  ; DE will be incremented by that period
    ld      E, A                    ; lets extend A to 16 bits
    add     A, A                    ; push sign bit into carry
    sbc     A, A                    ; A = 0 if carry == 0 else $FF
    ld      D, A                    ; now DE contains the extended version of A
    add     HL, DE                  ; HL is the correct period
    ld      DE, HL                  ; lets put back in DE

skip_pitch:
    ; transform to SN76489 format - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, workMem0            ; swapout freq values
    ld      (HL), E
    ld      A, D
    rld
    rld
    ld      E, A
    ld      D, (HL)

    ret

; ------------------------------------------------------------------------------
; INS_WriteChannelPeriod writes the period into the current channel
; input
;     DE: frequency to write
    .local
NOISE_CHANNEL_IDX       equ 3
CHANNEL3_IDX            equ 2
INS_WriteChannelPeriod::
    ; loads the channel to write in - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (channelHWChannel)
    cp      NOISE_CHANNEL_IDX       ; if channel is the noise channel, we need
    jp      NZ, first_write         ;   to update the frequency in channel #3
    ld      A, CHANNEL3_IDX

    ; Pulse channels first write  - - - - - - - - - - - - - - - - - - - - - - -
    ; to write a note on the SN76489, we must do two writes, first byte sets
    ; the register and the least significant bits of the period.
first_write:
    ld      HL, PSG_REG             ; set HL to point to the PSG register
    or      $80                     ; add latch bit
    or      E                       ; set LSB perdiod values
    ld      (HL), A                 ; write first bit

    ; second write - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ; the second write sets the 6 most significant bits
    ld      A, D
    ld      (HL), A
    ret
    .endlocal

; ------------------------------------------------------------------------------
; save the macro variables into the macro memory
; input : INS_cur_macro_addr
INS_SaveMacro:
    exx
    ; read the dynamic part
    ld      DE, (INS_cur_macro_addr)
    ld      HL, macroRamLocalStruct
    ld      BC, INS_PSG_SIZE
    ldir
    exx
    ret

    .endlocal

; ------------------------------------------------------------------------------
; process the update of the channels through their respective instruments
    .local
INS_UpdateFmChannels::
    ld      HL, INS_fmSignals
    ld      (channelSignalAddr), HL

    ld      HL, INS_fmInputNote
    ld      A, (HL)
    ld      (channelNote), A

    ; load INS_fmChannel in INS_curFMChannelDataAddr - - - - - - - - - - - - - -
    ld      HL, INS_fmChannel
    ld      (INS_curFMChannelDataAddr), HL

    ; let starts with part-I channels - - - - - - - - - - - - - - - - - - - - -
    ld      HL, channelFmRegisterAddr
    ld      (HL), YM1_ADR & $FF
    ld      HL, channelFmValAddr
    ld      (HL), YM1_VAL & $FF

    xor     A
    ld      (INS_curChannel), A

foreach_channels:
    ; load channel current note - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, INS_fmInputNote
    add     L
    ld      L, A
    ld      A, (HL)
    ld      (channelNote), A

    ; lets compute the channel base value to write on the YM - - - - - - - - - -
    ld      A, (INS_curChannel)     ; lets convert channel ID to channel
    cp      3                       ; YM value, 0, 1, 2, 4, 5, 6
    jp      C, part1                ; if lesser than 3; it's 0, 1, 2
part2:
    inc     A                       ;  else it's 4, 5, 6
part1:
    ; write base key on/off channel value
    ld      (channelHWChannel), A

    ; process signal - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, (channelSignalAddr)
    bit     INS_SIGNAL_SET_VOLUME, (HL)
    jp      Z, process_key_signal
    call    INS_UploadFMAttenuation

process_key_signal:
    ld      HL, (channelSignalAddr)
    bit     INS_SIGNAL_KEY_OFF, (HL)
    jp      NZ, do_key_off
    bit     INS_SIGNAL_KEY_ON, (HL)
    jp      Z, clear_signal

do_key_on:
    call    INS_FmKeyOn
    jp      clear_signal
do_key_off:
    call    INS_FmKeyOff

clear_signal:
    ; clear signal and increment current channel signal addr - - - - - - - - - -
    xor     A
    ld      HL, (channelSignalAddr)
    ld      (HL), A
    inc     HL
    ld      (channelSignalAddr), HL

    ; increment INS_curFMChannelDataAddr by the size of the a channel data block
    ld      HL, (INS_curFMChannelDataAddr)      ; load address value in HL
    ld      A, INS_FM_CHANNEL_RAM_SIZE          ; increment by the block size
    add     L                                   ; offset L by that amount
    ld      L, A                                ; (FM_fmChannel is aligned)
    ld      (INS_curFMChannelDataAddr), HL      ; write back the value

    ; goto part-II when channel is 2 - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (INS_curChannel)
    cp      2
    jp      NZ, still_part_1
    ; setting part-II - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, channelFmRegisterAddr
    ld      (HL), YM2_ADR & $FF
    ld      HL, channelFmValAddr
    ld      (HL), YM2_VAL & $FF

still_part_1:
    ; goto next FM channel - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, INS_curChannel
    inc     (HL)
    ld      A, (HL)

    cp      SEQ_MAX_FM_COLS         ; until all channels are processed
    jp      NZ, foreach_channels

    ret
    .endlocal

; ------------------------------------------------------------------------------
; write on the YM chip the key-off value to the proper register
; input:
;   channelHWChannel (in hardware notation 0, 1, 2, 4, 5, 6)
    .local
INS_FmKeyOff::
    ; first, lets call Key Off on the YM - - - - - - - - - - - - - - - - - - - -
    ld      A, (channelHWChannel)
    ld      HL, YM1_ADR
    ld      (HL), YM_KEY
    and     $0F
    ld      (YM1_VAL), A

    ret
    .endlocal

; ------------------------------------------------------------------------------
; write on the YM chip the key-on value to the proper register. This function
; shouldn't be called by other subroutines than INS_UpdateFmChannels
; input:
;       channelHWChannel (channel in YM notation)
;       channelNote must be set to something
;       channelFmRegisterAddr channels addresse register for the requested channel
;       channelFmValAddr value register for the requested channel
    .local
INS_FmKeyOn::
    ; first, lets call Key Off on the YM - - - - - - - - - - - - - - - - - - - -
    ld      A, (channelHWChannel)   ; load LSB channel value (0, 1, 2)
    ld      HL, YM1_ADR
    ld      (HL), YM_KEY
    ld      (YM1_VAL), A

    ; read the note - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      BC, DATA_ym_notes   ; BC is the frequency address
    ld      HL, channelNote     ; load note ID, which is the offset in the table
    ld      C, (HL)             ; apply the offset on BC
    sla     C                   ; mul by two because freqs are 16 bit values

    ld      A, (channelHWChannel)
    and     $03                 ; remove bit 2, it's not needed for freq

    ; set the frequencies  - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ; frequencies must be written for all operators of the instrument
    ld      D, YM_FREQ_MSB_1        ; D = $A4
    add     A, D                    ; increment A by D, A was the channel number,
                                    ;    now its the register
    ld      E, A                    ; save register in E
    ld      HL, (channelFmRegisterAddr)
    ld      (HL), A                 ; write YM register (A4+)

    ld      A, (BC)                 ; read MSB and block octave
    ld      HL, (channelFmValAddr)
    ld      (HL), A                 ; write it

    ld      A, E                    ; restore YM register
    sub     4                       ; reduce by 4, to write to A0+ registers
    ld      HL, (channelFmRegisterAddr)
    ld      (HL), A                 ; write second register (A0+)
    inc     BC                      ; set BC to read the next frequency
    ld      A, (BC)                 ; read LSB frequency data
    ld      HL, (channelFmValAddr)
    ld      (HL), A                 ; write it

    ; set key on - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      A, (channelHWChannel)   ; get channel
    or      $F0                     ; convert it to a key on directive by
                                    ;    setting all operators to ON
    ld      HL, YM1_ADR
    ld      (HL), YM_KEY            ; write key register
    ld      (YM1_VAL), A            ; write key on

    ret
    .endlocal

; ------------------------------------------------------------------------------
; compute the effective attenuation based on several parameters and send it to
; the YM chip.
    .local
ym_register     db      $00
INS_UploadFMAttenuation::
    ; get channel base attenuation - - - - - - - - - - - - - - - - - - - - - - -
    ld      HL, (INS_curFMChannelDataAddr)
    ld      A, (HL)
    ld      (channelAmplitude), A

    ; compute first operator write register
    ld      D, YM_TL_CH1_OP1
    ld      A, (channelHWChannel)
    add     D
    ld      (ym_register), A

    ; get affected operator bits - - - - - - - - - - - - - - - - - - - - - - - -
    inc     HL
    inc     HL
    ld      C, (HL)             ; C contains affected operator bits

    ; transfert INS_fmChannel ptr to DE - - - - - - - - - - - - - - - - - - - -
    ex      DE, HL              ; DE is the pointer to the instrument oper TL

    ; load fm registers - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      IX, (channelFmRegisterAddr)
    ld      IY, (channelFmValAddr)

    ld      B, 4
foreach_operators:
    inc     DE                  ; move to operator instrument defined TL addr

    ; if operator bit is on, TL must be scaled given the channel base - - - - -
    ; attenuation.
    bit     0, C
    jp      Z, skip_operator

    ld      A, (DE)                 ; load current operator TL value
    neg                             ; convert it to amplitude because channel
    add     $7F                     ; is also amplitude based

    exx                             ; protect BC, DE from being modifier by IMUL
    ld      B, A                    ; load operator TL in D
    ld      A, (channelAmplitude)   ; load channel amplitude in D
    call    IMUL_8                  ; A:E = A * B
    rl      E
    rl      A
    ld      H, A

    ld      A, (ym_register)        ; write the operator TL register to YM
    ld      (IX), A                 ;
    ld      A, H                    ; move scaled TL to A
    neg
    add     $7F
    ld      (IY), A                 ; write the the TL value to YM
    exx

skip_operator:
    ; increment operator target register - - - - - - - - - - - - - - - - - - - -
    ld      A, (ym_register)        ; increment the ym register by 4
    add     4                       ;
    ld      (ym_register), A        ;

    ; shift operator bit mask to the next operator - - - - - - - - - - - - - - -
    sra     C

    djnz    foreach_operators

    ret
    .endlocal

; ------------------------------------------------------------------------------
; change the volume base value for a given FM channel
; input: INS_inputAttenuation
;        INS_curChannel (0-6)
INS_SetFMAttenuation::
    ; compute channel attenuation addr - - - - - - - - - - - - - - - - - - - - -
    ld      HL, INS_fmChannel
    ld      A, (INS_curChannel)
    ld      C, A                    ; save channel in C

    .assert INS_FM_CHANNEL_RAM_SIZE == 8
    sla     A                       ; mul by 8
    sla     A                       ;
    sla     A                       ;
    add     L
    ld      L, A                    ; HL points at the right channel struct

    ; write attenuation and set volume flag - - - - - - - - - - - - - - - - - -
    ld      A, (INS_inputAttenuation)
    ld      (HL), A

    ld      HL, INS_fmSignals
    ld      A, C
    add     L
    ld      L, A
    set     INS_SIGNAL_SET_VOLUME, (HL)

    ret

; ------------------------------------------------------------------------------
; LoadFmInstrument
; input :       INS_instrumentAddr
;               A:channel
; destroys:     A, DE, BC, HL, IX, IY
INSTRUMENT_FM_SIZE             equ 30
    .local
INS_LoadFmInstrument::
    ; offset part-II channel by 1  - - - - - - - - - - - - - - - - - - - - - - -
    cp      3
    jp      C, skip
    inc     A                           ; increment channel by one the channel
                                        ; for part-II
skip:
    ld      (INS_curChannel), A
    bit     2, A                        ; check if it's instrument part-II
    jp      NZ, ld_instr_part2
ld_instr_part1:
    ld      DE, YM1_ADR
    ld      HL, YM1_VAL
    jp      end_ld_instr_part
ld_instr_part2:
    ld      DE, YM2_ADR
    ld      HL, YM2_VAL
end_ld_instr_part:
    ; IX points to instrument data in ROM
    ; IY points to register address on the YM2612
    ld      IX, (INS_instrumentRomAddr)
    ld      IY, DATA_chan0_addr_seq

    rrca                            ; mul by 32 in order to compute
    rrca                            ;  the register to write
    rrca                            ;  on given the input channel
    and     01100000b               ; clears the part-II instrument bit
    ld      B, 0
    ld      C, A                    ; we can use A
    add     IY, BC                  ; make IY to point at the right byte
                                    ;  sequence
    ld      B, INSTRUMENT_FM_SIZE/2

foreach_registers:
    ld      A, (IY)
    ld      (DE), A
    ym_delay    YM_DELAY

    ld      A, (IX)
    ld      (HL), A
    ym_delay    YM_DELAY

    inc     IY
    inc     IX

    ld      A, (IY)
    ld      (DE), A
    ym_delay    YM_DELAY

    ld      A, (IX)
    ld      (HL), A
    ym_delay    YM_DELAY

    inc     IY
    inc     IX

    djnz    foreach_registers

    .endlocal
    ; ** fallthrough     INS_UpdateChannelForInstrument

; ------------------------------------------------------------------------------
; Cache some channel instrument values needed to compute effect and volume.
; This implies to setup which operators need to be updated.
; input: INS_curChannel: channel
;        INS_instrumentRomAddr
INS_FM_ALGO_ROM_OFFSET      equ 28
INS_FM_TL_ROM_OFFSET        equ 4
    .local
INS_SetFMChannelInstrumentCache::
    ld      IX, (INS_instrumentRomAddr)
    ; set channel write position into HL - - - - - - - - - - - - - - - - - - - -
    ld      A, (INS_curChannel)         ; channel values are 0, 1, 2, 4, 5, 6
    cp      3                           ; if channel is greater than 3
    ccf                                 ; lets decrease it by 1 to obtain
    sbc     0                           ; an unfragmented id space

    sla     A                           ; mul A by 8
    sla     A                           ;
    sla     A                           ;
    ld      HL, INS_fmChannel
    add     L
    ld      L, A

    ; write channel values - - - - - - - - - - - - - - - - - - - - - - - - - - -
    inc     L                       ; skip attenuation

    ld      A, (IX+INS_FM_ALGO_ROM_OFFSET)
    and     $07                     ; removes feedback part
    ld      (HL), A                 ; write in FM_ALGO_O
    inc     L

    ld      DE, INS_ALGO_OP_OUT     ; compute connected operator mask addr
    add     E                       ;  in BC
    ld      E, A
    ld      A, (DE)                 ; load the mask in A
    ld      (HL), A                 ; write in FM_ATT_BITS
    inc     L

    ; write TL volumes - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ld      DE, (INS_instrumentRomAddr) ; compute address of the TLs in ROM
    ld      A, INS_FM_TL_ROM_OFFSET
    add     E                           ; perform DE + A
    ld      E, A
    adc     A, D
    sub     E
    ld      D, A

    ld      A, (DE)                     ; operator 0
    ld      (HL), A
    inc     L
    inc     DE

    ld      A, (DE)                     ; operator 1
    ld      (HL), A
    inc     L
    inc     DE

    ld      A, (DE)                     ; operator 2
    ld      (HL), A
    inc     L
    inc     DE

    ld      A, (DE)                     ; operator 3
    ld      (HL), A

    ret
    .endlocal
