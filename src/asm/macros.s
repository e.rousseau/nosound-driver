; ------------------------------------------------------------------------------
; NOSOUND driver for sega mega drive
;
; Copyright (c) 2023 Emmanuel Rousseau
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
; ------------------------------------------------------------------------------
; some definitions

True            equ     $01
False           equ     $00

; ------------------------------------------------------------------------------
; struct definition
struct:     .macro
__struct_offset = 0
    .endm

f_ptr:      .macro &name
&name   equ  {__struct_offset}
__struct_offset = __struct_offset + 2
    .endm

f_u16:      .macro &name
&name   equ  {__struct_offset}
__struct_offset = __struct_offset + 2
    .endm

f_u8:      .macro &name
&name   equ {__struct_offset}
__struct_offset = __struct_offset + 1
    .endm

end_struct:     .macro &struct_name
&struct_name_SIZE  equ {__struct_offset}
    .endm

; ------------------------------------------------------------------------------
; List macros, to be used with list16.s module
; reserve in RAM an array of 16bit words ready to be used with this module
L16_New:  .macro  &elem_count
    ds  &elem_count*2 + 3, $00
    .endm
