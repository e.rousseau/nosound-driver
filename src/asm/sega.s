; ------------------------------------------------------------------------------
; NOSOUND driver for sega mega drive
;
; Copyright (c) 2023 Emmanuel Rousseau
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
; ------------------------------------------------------------------------------
; relates to sega hardware

; ------------------------------------------------------------------------------
; general configurations

SGA_NTSC    equ     0
SGA_PAL     equ     1

; system type
SGA_system          db      SGA_NTSC

; ------------------------------------------------------------------------------
; register I/O addresses

; SN76489 register port address
PSG_REG             equ $7F11

; ------------------------------------------------------------------------------
; bank select

; this variable holds the bank to select and the bank that is currently
; selected. Only the first 9 bits of this variable are used. They control
; which address line, A15 to A23 are used when reading at $8000.
;
; By example,
;   $0000 will map z80:$8000-FFFF = m68k:$00000000-$00007FFF
;   $0001          z80:$8000-FFFF = m68k:$00008000-$0000FFFF
;    ...
;   $01FF          z80:$8000-FFFF = m68k:$00FF8000-$00FFFFFF
;
SGA_selectedBank        dw  $0000

; the bank switch register
SGA_BANK_SELECT_ADDR    equ $6000

; ------------------------------------------------------------------------------
; switch the bank the z80 has access to
; input: SGA_selectedBank
    .local
SGA_BankSwitch::
    ld      HL, (SGA_selectedBank)  ; HL is the selected bank
    ld      B, 8                    ;
foreach_selected_bank_bits:
    ld      A, L                    ; lets rotate the bits of A 8 times
    and     $01
    ld      (SGA_BANK_SELECT_ADDR), A    ; write each bits in $6000 (following Sega's
                                    ;   spec.
    sra     L
    djnz    foreach_selected_bank_bits
    ld      A, H                    ; write 9th bit that was in H to $6000
    and     $01
    ld      (SGA_BANK_SELECT_ADDR), A

    ret
    .endlocal

; ------------------------------------------------------------------------------
; SGA_IsUsingBank returns true in A if the value HL equals the value of
; SGA_selectedBank
; input:
;    HL: bank number to check.
; return Z flags = True when True
    .local
SGA_IsUsingBank::
    ld      BC, (SGA_selectedBank)  ; load in BC the current used back
    ld      A, L                    ; compare lower byte
    cp      C
    ret     NZ
    ld      A, H                    ; compare higher byte
    cp      B

    ret
    .endlocal

