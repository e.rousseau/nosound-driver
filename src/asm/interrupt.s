; ------------------------------------------------------------------------------
; NOSOUND driver for sega mega drive
;
; Copyright (c) 2023 Emmanuel Rousseau
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
; ------------------------------------------------------------------------------
; Interruption Sub Routine

    org     $0038

; ------------------------------------------------------------------------------
; this ISR just set the clock flag to true. When this flag is set, the main loop
; triggers a Sequencer Update as well as the a SFX update.
    .local
INT_Vblanck:
    di
    push    AF

    ld      A, (vdp_fix)
    or      A
    jp      Z, process
    xor     A
    ld      (vdp_fix), A
    jp      end

process:
    ld      A, (NOS_sysClock)
    cp      True
    jp      NZ, no_miss

    ld      A, (NOS_sysMissedClock)
    inc     A
    ld      (NOS_sysMissedClock), A

no_miss:
    ld      A, True
    ld      (NOS_sysClock), A

    ld      A, True
    ld      (vdp_fix), A
end:
    pop     AF
    ei
    ret

vdp_fix     db $00
    .endlocal
; ------------------------------------------------------------------------------
; NMI handler
    org     $0066
    ret

    db  "NOSOUND DRIVER MD, CRANK YOUR CRT UP!!|(C)2023 EMMANUEL ROUSSEAU", $00
