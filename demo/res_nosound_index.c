#include "res_nosound_index.h"
const nos_playable_s nos_sfx_entries[3] = {
    {
        .data = &nosound_chunk_0[0],
        .type = NosPlayableTypeSfx,
        .priority = 0
    },{
        .data = &nosound_chunk_0[125],
        .type = NosPlayableTypeSfx,
        .priority = 1
    },{
        .data = &nosound_chunk_0[257],
        .type = NosPlayableTypeSfx,
        .priority = 0
    }
};
const nos_playable_s nos_mod_entries[2] = {
    {
        .data = &nosound_chunk_0[300],
        .type = NosPlayableTypeMusic,
        .priority = 0
    },{
        .data = &nosound_chunk_0[1100],
        .type = NosPlayableTypeMusic,
        .priority = 0
    }
};